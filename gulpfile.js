
var gulp 		= require('gulp'),
	less 		= require('gulp-less'),
	path 		= require('path'),
	concat 		= require('gulp-concat'),
	sourcemaps 	= require('gulp-sourcemaps'),
	uglify 		= require('gulp-uglify'),
	cleanCSS 	= require('gulp-clean-css'),
	php 		= require('gulp-connect-php'),
	browserSync = require('browser-sync').create();

/*
 |--------------------------------------------------------------------------
 | LESS
 |--------------------------------------------------------------------------
 */


gulp.task('less', () => {

	gulp.src('resources/assets/less/main.less')
		// .pipe(sourcemaps.init())
		.pipe(less())
		// .pipe(sourcemaps.write())
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest('public/assets/css/'))

});


/*
 |--------------------------------------------------------------------------
 | SCRIPTS
 |--------------------------------------------------------------------------
 */

gulp.task('scripts-vendor', () => {

	return 	gulp.src([
				'resources/assets/js/vendor/jquery.min.js',
				'resources/assets/js/vendor/bootstrap.min.js',
				'resources/assets/js/vendor/flickity.js',
				'resources/assets/js/vendor/fastclick.js',
				'resources/assets/js/vendor/modernizr.js',
				'resources/assets/js/vendor/vimeo.js',
				'resources/assets/js/vendor/moment.js',
				'resources/assets/js/vendor/flipclock.js',
			])
			.pipe(sourcemaps.init())
			.pipe(concat('vendor.js'))
			.pipe(sourcemaps.write())
			.pipe(uglify())
			.pipe(gulp.dest('public/assets/js/'));

});

gulp.task('scripts', () => {

	return 	gulp.src('resources/assets/js/main/**/*.js')
			.pipe(sourcemaps.init())
			.pipe(concat('main.js'))
			.pipe(sourcemaps.write())
			.pipe(uglify())
			.pipe(gulp.dest('public/assets/js/'));

});


/*
 |--------------------------------------------------------------------------
 | PHP SERVER
 |--------------------------------------------------------------------------
 */

gulp.task('serve-php', () => {

    php.server({
        port: 5001,
        base: './public',
        open: false
    });

    browserSync.init({
        notify: false,
        proxy: '127.0.0.1:5001',
        port: 5000,
        open: true
    });


});


gulp.task('reload', ['apo:build'], () => {

    browserSync.reload();

});


/*
 |--------------------------------------------------------------------------
 | SERVE
 |--------------------------------------------------------------------------
 */

gulp.task('apo:build', ['less', 'scripts-vendor', 'scripts'], () => {});

gulp.task('apo:serve', ['apo:build', 'serve-php'], () => {});

gulp.task('apo:dev', ['apo:serve'], () => {

	gulp.watch("resources/**/*.{php,js,less}", ['reload']);
	
});





/*
 |--------------------------------------------------------------------------
 | DEFAULT GULP
 |--------------------------------------------------------------------------
 */

gulp.task('default', ['apo:serve']);

