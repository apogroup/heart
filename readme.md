#APO Setup
In the console, from the project directory root:

##Load Laravel Dependencies by running in order

	composer dumpautoload
	composer clearcache
	composer install
	composer update

##Load Node dependenices by running

	npm install

##Create .env file
In the project directory, create a .env file and put the contents of .env.example in the new .env file

##Generate a project key
In the console, from the project directory root run:
	
	php artisan key:generate

##Running from local database (optional)

Create a localhost database named theheartcommunity

Import the theheartcommunity.sql (in the project directory)

Replace the DB_ connection in .env file 

	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=theheartcommunity
	DB_USERNAME=root
	DB_PASSWORD=

##Running from remote database

(Database at 185.24.97.248 / heartstaging.apothecomgroup.com)

Replace the DB_ connection in .env file

	DB_CONNECTION=mysql
	DB_HOST=185.24.97.248
	DB_PORT=3306
	DB_DATABASE=apo_heart_staging
	DB_USERNAME=apo_heart
	DB_PASSWORD=entr3$T0

**Remote MYSQL runs at port 3306 -- if you are being flagged with 'exception 'PDOException' with message 'SQLSTATE[HY000]' or a connection timeout -- 
	
	1. Your IP has not been whitelisted
	2. Your network firewall may be blocking port 3306 or 
	
	Remote MYSQL Whitelisted IPs @ Host:185.24.97.248
	--------------------------------------------
	4.30.41.%
	96.93.39.%


##Serve
Create a PHP Server and launch in browser

	gulp apo:serve

Create a PHP Server and launch in browser

Watch for file changes in /resources {less,js,php} & build, then reload in browser

	gulp apo:dev

Build/Compile LESS & JS

	gulp apo:build

Minify

	TBD


....................................................................




# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
