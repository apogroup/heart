<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


    
    // Handle all undefined routes with 404
    Route::get('404', 'PublicController@notfound');

    // Homepage

    Route::get('/', function () { return redirect('dashboard'); });
    Route::get('/home', function () { return redirect('dashboard'); });

    // Auth (Login, Registration, Password Reset)
    
    Route::auth();
    Route::get('login/{provider}', 'Auth\AuthController@redirectToProvider')->name('login');
    Route::get('login/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

    //Public routes

    Route::get('terms-of-service', 'PublicController@termsofservice')->name('termsofservice');
    Route::get('privacy-policy', 'PublicController@privacypolicy')->name('privacypolicy');

    Route::get('heartclinic', 'PublicController@learningresources')->name('clinic');

    Route::get('heartlibrary', 'PublicController@literature')->name('library');
    Route::post('heartlibrary', 'PublicController@literature')->name('library');

    Route::get('hearttheater', 'PublicController@videos')->name('theater');
    

        //Module routes

            Route::get('module/{id}', 'ContentDeliveryController@lr_module');
            Route::get('video_module/{id}', 'ContentDeliveryController@video_module');

    //User routes

    Route::get('dashboard', 'UserController@dashboard')->name('dashboard');
    Route::get('heartforum', 'UserController@forum')->name('forum');

    //Analytics routes

    Route::post('markcompleted', 'ResourceAnalyticsController@completed')->name('analytics-resource-completed');
    Route::get('markcompleted', function () { return redirect('/'); });
    Route::post('markviewed', 'ResourceAnalyticsController@viewed')->name('analytics-resource-viewed');
    Route::get('markviewed', function () { return redirect('/'); });
    
    // Campaigns
    
    Route::get('/campaigns/{event_category}/{event_action}/{event_label}/{event_referral}', 'PublicController@campaigns')->name('campaigns');

    // Events

    Route::post('eventregister', 'UserController@eventRegister')->name('event-register');
    Route::get('eventregister', function () { return redirect('/'); });


    // Admin

    Route::get('registrants', 'AdminController@registrants')->name('registrants');



