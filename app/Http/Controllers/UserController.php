<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\LearningResource;
use App\Literature;
use App\Video;

use App\ResourceCompletion;
use App\ResourceView;

use App\EventRegistrations;

use Auth;

use App\Http\Controllers\EmailController;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware('auth');
    }

    /**
     * Display dashboard.
     *
     * @return response
     */
    public function dashboard()
    {
        $lrcount            = LearningResource::get()->count();
        $literaturecount    = Literature::get()->count();
        $videocount         = Video::get()->count();

        $user = Auth::User();

        $lrcompleted            = $this->lrcount($user->id);
        $literaturecompleted    = $this->literaturecount($user->id);
        $videocompleted         = $this->videocount($user->id);

        $registered_for_event   = $this->isRegisteredForEvent($user->id);

        $videos = Video::all();
        $learningresources = LearningResource::all();

        return view('dashboard')
                    ->with('videos', $videos)
                    ->with('learningresources', $learningresources)
                    ->with('lrcount', $lrcount)
                    ->with('literaturecount', $literaturecount)
                    ->with('videocount', $videocount)
                    ->with('lrcompleted', $lrcompleted)
                    ->with('literaturecompleted', $literaturecompleted)
                    ->with('videocompleted', $videocompleted)
                    ->with('registeredforevent', $registered_for_event);
    }

    public function forum()
    {
        
        return view('forum');

    }
    


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function lrcount($user_id)
    {
        $count = ResourceView::where(['user_id' => $user_id, 'resource_type' => 'clinic-module'])->count();
        return $count;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function literaturecount($user_id)
    {
        $count = ResourceView::where(['user_id' => $user_id, 'resource_type' => 'library-article'])->count();
        return $count;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function videocount($user_id)
    {
        $count = ResourceView::where(['user_id' => $user_id, 'resource_type' => 'theater-video'])->orWhere(['user_id' => $user_id, 'resource_type' => 'theater-video-chaptered'])->count();
        return $count;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function isRegisteredForEvent($user_id)
    {
        $registrant = EventRegistrations::where(['user_id' => $user_id, 'event' => 'august_24_2016_butler'])->first();
        
        $registered = null;

        if(is_null($registrant)) {
            $registered = false;
        }

        else {
            $registered = true;
        }

        return $registered;
    }


    public function eventRegister(Request $request)
    {

        $userid         = $request->input('user_id');
        $event          = $request->input('event');

        $exist = EventRegistrations::where(['user_id' => $userid, 'event' => $event])->get();

        if($exist->count() < 1) {

            $eventReg             = new EventRegistrations();
            $eventReg->user_id    = $userid;
            $eventReg->event      = $event;
            $eventReg->save();
        
        }

    }

}
