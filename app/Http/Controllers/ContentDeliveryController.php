<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ContentDeliveryController extends Controller {

	/**
     * Initialize controller.
     *
     * @return null
     */
    public function __construct()
    {

    }

	/**
	 * ...
	 *
	 * @return Response
	 */
	public function lr_module($id)
	{

		$module = view('modules.clinic.module_0'.$id.'.module_0'.$id.'');

        return $module;

	}

	/**
	 * ...
	 *
	 * @return Response
	 */
	public function video_module($id)
	{

		$module = view('modules.theater.video_module_0'.$id.'.video_module_0'.$id.'');

        return $module;

	}


}