<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\ResourceCompletion;
use App\ResourceView;

class ResourceAnalyticsController extends Controller {

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

	public function completed(Request $request)
	{

		$userid 		= $request->input('user_id');
		$resourcetype 	= $request->input('resource_type');
		$resourceid 	= $request->input('resource_id');

		$exist = ResourceCompletion::where(['user_id' => $userid, 'resource_id' => $resourceid, 'resource_type' => $resourcetype])->get();

		if($exist->count() < 1) {

			$resourcecompletion 					= new ResourceCompletion();
			$resourcecompletion->user_id 			= $userid;
			$resourcecompletion->resource_type 		= $resourcetype;
			$resourcecompletion->resource_id		= $resourceid;
			$resourcecompletion->save();
		
		}

	}

	public function viewed(Request $request)
	{

		$userid 		= $request->input('user_id');
		$resourcetype 	= $request->input('resource_type');
		$resourceid 	= $request->input('resource_id');

		$exist = ResourceView::where(['user_id' => $userid, 'resource_id' => $resourceid, 'resource_type' => $resourcetype])->get();

		if($exist->count() < 1) {

			$resourceview 					= new ResourceView();
			$resourceview->user_id 			= $userid;
			$resourceview->resource_type 	= $resourcetype;
			$resourceview->resource_id		= $resourceid;
			$resourceview->save();
		
		}

	}

}
