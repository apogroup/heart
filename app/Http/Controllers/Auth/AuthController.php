<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Mail;

use App\EventRegistrations;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'specialty' => 'required|max:255',
            'firstname' => 'required|max:255',
            'lastname'  => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'specialty' => $data['specialty'],
            'name'      => $data['firstname'] . ' ' . $data['lastname'],
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
        ]);

        Mail::send('auth.emails.welcome', $data, function($message) use ($data) {
            $message->from("noreply@theheartcommunity.com", "TheHeartCommunity.com");
            $message->subject("Registration confirmation: heart| the online educational community");
            $message->to($data['email']);
        });

        // $eventReg = new EventRegistrations();
        // $eventReg->user_id    = $user->id;
        // $eventReg->event      = 'august_24_2016_butler';
        // $eventReg->save();

        return $user;

    }
}
