<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware('auth');

    }

    /**
     * Display dashboard.
     *
     * @return response
     */
    public function registrants(User $user)
    {

        $registrants = $user->sortable(['created_at' => 'asc'])->paginate(1000);

        // no best practice -- quick fix but must be change into a proper middleware
        $usr = Auth::User();

        if($usr->email != 'support@theheartcommunity.com') {

            return redirect('/');

        } else {

            return view('admin.registrants')->with('registrants', $registrants);

        }

    }


}
