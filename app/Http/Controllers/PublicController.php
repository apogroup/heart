<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\LearningResource;
use App\Literature;
use App\Video;

use App\ResourceView;

class PublicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function notfound()
    {
        return view('errors.404');
    }


    public function termsofservice()
    {
        return view('legal.termsofservice');
    }

    public function privacypolicy()
    {
        return view('legal.privacypolicy');
    }
    
    public function learningresources()
    {
        $learningresources = LearningResource::orderBy('id')->get();

        return view('clinic')->with('learningresources', $learningresources);
    }

    public function literature(Request $request)
    {

        // $literature = Literature::orderBy('id', 'desc')->get();

        // return view('library')->with('literature', $literature);

        $articles = Literature::all();

        foreach($articles as $article) {

            $views = ResourceView::where('resource_type', '=', 'library-article')->where('resource_id','=',$article['id'])->get()->count();

            $l = Literature::where('id', '=', $article['id'])->firstOrFail();

            $l->views = $views;

            $l->save();

        }

        $query = $request->input('query');

        if(!$query)
        { 
            if ( $request->input('sort') ) { $sort = $request->input('sort'); } else { $sort = 'created_at'; }
            if ( $request->input('order') ) { $order = $request->input('order'); } else { $order = 'desc'; }
            $literature = Literature::sortable([$sort => $order])->paginate(500);

            return view('library')->with('literature', $literature);
        }
        else 
        {

            if ( $request->input('sort') ) { $sort = $request->input('sort'); } else { $sort = 'created_at'; }
            if ( $request->input('order') ) { $order = $request->input('order'); } else { $order = 'desc'; }
            $literature = Literature::search($query, ['title', 'description'])->sortable([$sort => $order])->paginate(500);


            return view('library')->with(['literature' => $literature, 'query' => $query]);

        }

    }

    // public function literatureQuery(Request $request)
    // {
    //     echo $request;
    //     $query = $request->input('query');
    //     $literature = Literature::search($query, ['title', 'description'])->get();

    //     return view('library')->with(['literature' => $literature, 'query' => 'query='.$query.'&']);
    // }

    public function videos()
    {
        $videos = Video::orderBy('id', 'desc')->get();

        return view('theatre')->with('videos', $videos);

    }

    public function campaigns($event_category, $event_action, $event_label, $event_referral)
    {
        return view('campaigns')
                    ->with('event_category', $event_category)
                    ->with('event_action', $event_action)
                    ->with('event_label', $event_label)
                    ->with('event_referral', $event_referral);
    }





}
