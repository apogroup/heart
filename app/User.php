<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
    use Sortable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'specialty'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $sortable = [
        'id', 'name', 'email', 'specialty', 'created_at'
    ];

}
