<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Video extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'videos';

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function count()
	{
		$count = Video::get()->count();
		return $count;
	}


}