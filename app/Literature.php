<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Kyslik\ColumnSortable\Sortable;
use Sofa\Eloquence\Eloquence;

class Literature extends Model {
	
	use Sortable;
	use Eloquence;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'literature';

	protected $sortable = [
        'id', 'title', 'created_at', 'views'
    ];

    protected $searchableColumns = ['title', 'description'];
    
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function count()
	{
		$count = Literature::get()->count();
		return $count;
	}
}