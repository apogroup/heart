<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class ResourceView extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'resource_views';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'resource_type', 'resource_id'];


}