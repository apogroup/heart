<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class LearningResource extends Model {

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'learningresources';


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function count()
	{
		$count = LearningResource::get()->count();
		return $count;
	}


}