-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 24, 2017 at 05:53 PM
-- Server version: 5.5.54-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apo_heart_staging`
--

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `subtitle` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `source` text COLLATE utf8_unicode_ci,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `subtitle`, `description`, `source`, `thumbnail`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'Current Understanding of the Neurohormonal Mechanisms of the Pathophysiology of Heart Failure', 'Presented by Dr. James L. Januzzi, Jr, MD.', 'Review the current understanding of the neurohormonal mechanisms of the pathophysiology of heart failure.', 'https://player.vimeo.com/video/177233301', 'https://i.vimeocdn.com/video/584606945_590x332.jpg', 'video', NULL, '2016-03-03 07:00:00', '2016-03-03 07:00:00'),
(6, 'The Vulnerable Heart: Mechanisms Underlying the Unpredictable Clinical Course of Heart Failure', NULL, 'Learn about the vulnerability of the myocardium in heart failure at the cellular level.', 'https://player.vimeo.com/video/177228048', 'https://i.vimeocdn.com/video/584603581_590x332.jpg', 'video', NULL, '2016-04-27 00:00:00', '2016-04-27 00:00:00'),
(4, 'Neurohormonal Balance and the Pathophysiology of Heart Failure', NULL, 'Watch what happens to the heart when neurohormonal balance is lost in the progression of heart failure.', 'https://player.vimeo.com/video/177232796', 'https://i.vimeocdn.com/video/584605163_590x332.jpg', 'video', NULL, '2016-04-27 01:00:00', '2016-04-27 01:00:00'),
(5, 'The Myth of the Clinically Stable Patient with Heart Failure: Exploring Clinical Trajectories and the Threat of Sudden Cardiac Death', 'Presented by Dr. Javed Butler, MD, MPH, MBA', 'Discussing different clinical trajectories in heart failure and mechanisms underlying sudden cardiac death.', NULL, 'https://i.vimeocdn.com/video/584610873_590x332.jpg', 'module', NULL, '2016-06-16 00:00:00', '2016-06-16 00:00:00'),
(3, 'Targeting Neurohormones in Heart Failure: The Potential Role of Endogenous Compensatory Peptides', 'Presented by Dr. Milton Packer, MD', 'Find the key to unlocking better outcomes in neurohormonal targeting of heart failure.', NULL, 'https://i.vimeocdn.com/video/584609026_590x332.jpg', 'module', NULL, '2016-06-20 00:00:00', '2016-06-20 00:00:00'),
(1, 'The Myth of the Clinically Stable Patient with Heart Failure', 'Presented by Dr. Inder Anand, MD, PhD.', 'A discussion on the inherent clinical instability of patients with heart failure (HF) and implications of their risk for sudden cardiac death (SCD).', NULL, 'https://i.vimeocdn.com/video/584615966_590x332.jpg', 'module', NULL, '2016-06-23 00:00:00', '2016-06-23 00:00:00'),
(7, 'Roundtable Discussion: Neurohormonal Mechanisms That Drive Heart Failure (HF) Progression', 'Presented by Dr. Frank Spinale, MD, and Dr. Peter Liu, MD', 'The importance of rebalancing the neurohormonal mechanisms and pathways that drive heart failure progression.', NULL, 'https://i.vimeocdn.com/video/591780913_600x340.jpg', 'module', NULL, '2016-09-14 00:00:00', '2016-09-14 00:00:00'),
(0, 'The War on Heart Failure', 'Presented by Dr. Eugene Braunwald', 'A discussion around the challenges in HF treatment, advances in care, and potential future treatment options.', NULL, 'https://i.vimeocdn.com/video/608140131_590x332.jpg', 'module', NULL, '2016-06-16 00:00:00', '2016-06-16 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
