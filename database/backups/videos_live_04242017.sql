-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 24, 2017 at 01:56 PM
-- Server version: 5.5.54-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `heart_entresto`
--

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `subtitle` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `source` text COLLATE utf8_unicode_ci,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `subtitle`, `description`, `source`, `thumbnail`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'Current Understanding of the Neurohormonal Mechanisms of the Pathophysiology of Heart Failure', 'Presented by Dr. James L. Januzzi, Jr, MD.', 'Review the current understanding of the neurohormonal mechanisms of the pathophysiology of heart failure.', 'https://player.vimeo.com/video/177233301', 'https://i.vimeocdn.com/video/584606945_590x332.jpg', 'video', NULL, '2016-03-03 07:00:00', '2016-03-03 07:00:00'),
(4, 'Neurohormonal Balance and the Pathophysiology of Heart Failure', '', 'Watch what happens to the heart when neurohormonal balance is lost in the progression of heart failure.', 'https://player.vimeo.com/video/177232796', 'https://i.vimeocdn.com/video/584605163_590x332.jpg', 'video', NULL, '2016-04-27 01:00:00', '2016-04-27 01:00:00'),
(3, 'Targeting Neurohormones in Heart Failure: The Potential Role of Endogenous Compensatory Peptides', 'Presented by Milton Packer, MD.', 'Find the key to unlocking better outcomes in neurohormonal targeting of heart failure.', NULL, 'https://i.vimeocdn.com/video/584609026_590x332.jpg', 'module', NULL, '2016-06-20 00:00:00', '2016-06-20 00:00:00'),
(6, 'The Vulnerable Heart: Mechanisms Underlying the Unpredictable Clinical Course of Heart Failure', '', 'Learn about the vulnerability of the myocardium in heart failure at the cellular level.', 'https://player.vimeo.com/video/177228048', 'https://i.vimeocdn.com/video/584603581_590x332.jpg', 'video', NULL, '2016-09-26 04:00:00', '2016-09-26 04:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
