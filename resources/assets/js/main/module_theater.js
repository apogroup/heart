
////MODULE
	
	var vimeoSRC = [];
		vimeoSRC[7]	= [
						"https://player.vimeo.com/video/182762654",
						"https://player.vimeo.com/video/182762894",
						"https://player.vimeo.com/video/182764742",
						"https://player.vimeo.com/video/182765279",
						"https://player.vimeo.com/video/182766023",
						"https://player.vimeo.com/video/182766401"
					];
		vimeoSRC[5]	= [
						"https://player.vimeo.com/video/177236712",
						"https://player.vimeo.com/video/177236718",
						"https://player.vimeo.com/video/177236724",
						"https://player.vimeo.com/video/177236736",
						"https://player.vimeo.com/video/177236766",
						"https://player.vimeo.com/video/177236765",
						"https://player.vimeo.com/video/177236784",
						"https://player.vimeo.com/video/177236803",
						"https://player.vimeo.com/video/177236813",
						"https://player.vimeo.com/video/177236822",
						"https://player.vimeo.com/video/177236835",
						"https://player.vimeo.com/video/177236845",
						"https://player.vimeo.com/video/177236856",
						"https://player.vimeo.com/video/177236876",
						"https://player.vimeo.com/video/177236885",
						"https://player.vimeo.com/video/177236891"
					];
		vimeoSRC[3]	= [
						"https://player.vimeo.com/video/177234728",
						"https://player.vimeo.com/video/177234751",
						"https://player.vimeo.com/video/177234758"
					];
		vimeoSRC[1] = [
						"https://player.vimeo.com/video/177240897",
						"https://player.vimeo.com/video/177240906",
						"https://player.vimeo.com/video/177240924",
						"https://player.vimeo.com/video/177240936",
						"https://player.vimeo.com/video/177240940",
						"https://player.vimeo.com/video/177240957",
						"https://player.vimeo.com/video/177240966",
						"https://player.vimeo.com/video/177240970",
						"https://player.vimeo.com/video/177240979",
						"https://player.vimeo.com/video/177240986",
						"https://player.vimeo.com/video/177240996",
						"https://player.vimeo.com/video/177241016",
						"https://player.vimeo.com/video/177241033",
						"https://player.vimeo.com/video/177241045",
						"https://player.vimeo.com/video/177241060"
					];
		vimeoSRC[0] = [
						"https://player.vimeo.com/video/195817607",
						"https://player.vimeo.com/video/195817748",
						"https://player.vimeo.com/video/195817843",
						"https://player.vimeo.com/video/195817946",
						"https://player.vimeo.com/video/195817983",
						"https://player.vimeo.com/video/195818090",
						"https://player.vimeo.com/video/195818229",
						"https://player.vimeo.com/video/195818361"
					];



	function theaterInitFlick(id){
		
		// Let's not overload the DOM with multiple instances of this function
		// e.preventDefault();

		// Store our learning resource/ module id in an obj
		// var moduleid = $(this).attr('data-resource');
		var moduleid = id;

		// console.log(moduleid);

		// Init flicity instance
		$flick = $('#module-'+moduleid+'-gallery').flickity({ cellAlign: 'left', contain: true, draggable: false, setGallerySize: false, selectedAttraction: 1, friction: 1 })

			.on('cellSelect', function() {

				setTimeout(function(){

					var flkty = $flick.data('flickity');

					var index = flkty.selectedIndex;

					// Mobile Page Dots Alternative
					var cellNumber = index + 1;
  					$('.gallery-status').html('' + cellNumber + ' <sup>of</sup> ' + flkty.cells.length + '');
  					// Mobile Page Menu
  					var chapterTitle = $('.chapter-'+index+' a').text();
  					$('.mobile-module-menu-title').text(chapterTitle);

					// METRICS
					if(index!=-1){
						var userId = $("#ga-data").attr('user-id');
						var resourceId = moduleid+'.'+index;

						// console.log(userId);
						// console.log(resourceId);

						markResourceViewed(userId,resourceId,'theater-video-chaptered-chapter');

						if($('.chapter-'+index).attr('metric') == 'completion'){

							markResourceCompleted(userId,moduleid,'theater-video-chaptered');

						}
					}
					//

					$(".chapter-btn").removeClass("current-chapter");

					$('.chapter-'+index).addClass('current-chapter');

					var videoModule = $('.chapter-btn').first().parent().attr('video-module');

					var chapter = index;
						chapter = 'chapter-'+chapter;
					chapterScroll(chapter);

					$('.cell-scroll').scrollTop(0);

					//Vimeo

					vimeoPlayer.pause();
					vimeoPlayer = null;

					var loaded = $('.video-mod-'+moduleid+'-container-'+index+' iframe').attr('src');
					if(!loaded) $('.video-mod-'+moduleid+'-container-'+index+' iframe').attr('src', vimeoSRC[moduleid][index]);
					
					initModuleVimeo('mod-'+moduleid+'-chapter-'+index+'', function(response){

						// console.log(response);

						var next = index+1;

						vimeoPlayer = response;

						vimeoPlayer.play();

						vimeoPlayer.on('ended', function() {
					        
							$flick.flickity( 'select', next );

					    });

					});


				},100);

			});

			$('.gallery-status').html('1 <sup>of</sup> ' + $flick.data('flickity').cells.length + '');

			var chapterTitle = $('.chapter-0 a').text();
			$('.mobile-module-menu-title').text(chapterTitle);

	}


	function getTheaterModule(id) {

		if($('.video-module-modal .module-wrapper').length) $('.video-module-modal .module-wrapper').remove();

		$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

	    $.ajax({
	        type: "GET",
	        url: './video_module/'+id+'',
	        success: function(data) {
	            loadTheaterModule(id, data);

	        },
	        error: function(e)
            {  
            	// console.log(e.responseText);  
            } 
	    })

	}

	function loadTheaterModule(id, data){

		$('div.modal-body-'+id+'').html(""+data+"");

		theaterInitFlick(id);

		//Vimeo

		$('.video-mod-'+id+'-container-0 iframe').attr('src', vimeoSRC[id][0]);

		initModuleVimeo('mod-'+id+'-chapter-0', function(response){

			vimeoPlayer = response;

			vimeoPlayer.on('ended', function() {
		        
				$flick.flickity( 'select', 1 );

		    });

		});


		$('.chapter-0').addClass('current-chapter');

		// Listen for our click to open a module modal

		$('.module-modal-close').on('click', function(e){

			// console.log('clearing module');

			// Let's not overload the DOM with multiple instances of this function
			e.preventDefault();

			// Store our learning resource/ module id in an obj
			var moduleid = $(this).attr('data-resource');

			//Destroy flickity instance
			$flick = $('#module-'+moduleid+'-gallery').flickity('destroy');

			$('.chapter-btn').removeClass('current-chapter');

		});

		// On sidebar button click

		$('.module-menu-buttons').on( 'click', '.chapter-btn', function() {

			// console.log('changing chapter');

			// Get the index (chapter number) // add 1 for module intro page adjustment
			var index = $(this).index();

			// Move to this selected chapter in flickity
			$flick.flickity( 'select', index );
			
			// Remove the current-chapter button (only on the specific module gallery menu)
			$(this).siblings().removeClass("current-chapter");

			// Add the current-chapter class to the clicked button
			$(this).addClass("current-chapter");

			if($(window).width() < 415) { $('.modal-sidebar').slideUp(); }
			
		});


		$('.video-module-modal').on('hidden.bs.modal', function () {

			$(".mobile-module-menu-title").text("");

		});


	}