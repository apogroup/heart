
$(document).ready( function() {

	// Launch Modal from URL


	function GetURLParameter(sParam){

	    var sPageURL = window.location.search.substring(1);

	    var sURLVariables = sPageURL.split('&');

	    for (var i = 0; i < sURLVariables.length; i++) {

	        var sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] == sParam) {
	            return sParameterName[1];
	        }

	    }
	};
	
	var modalParam = GetURLParameter('modal');

	setTimeout(function(){

		if(modalParam == 'privacy'){
	 		
	 		$('.open-privacy').trigger('click');	

		} else if(modalParam){

			$('[data-target="#'+modalParam+'"]').first().trigger('click');

		}

	},100);



}); //doc ready













