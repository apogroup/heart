// DOM modification once jQuery is "ready"
$(document).ready( function() {

	checkHCPCookie();

	// Confirmation Buttons
	$( "#confimation-yes-button" ).click(function() {

		setCookie('hcp', true, 30);

		// $( "#pre-confirmation" ).fadeOut( "fast", function() {
		// 	$( "#post-confirmation" ).fadeIn( "fast" );
		// });

	});

}); //end doc ready


// HCP Coookie

	function checkHCPCookie() {
	    var hcp=getCookie("hcp");

	    // console.log('checking for cookie');

	    if(!hcp){

			$('.open-hcp').trigger('click');

		}

	}

	function validateHCP(){
		setCookie('hcp', true, 30);
	}

	function setCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function getCookie(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	    }
	    return "";
	}