$(document).on("click", "div.figure.module-visible", function(){
	var figure = $(this);
	var moduleNumber = figure.attr("module-number");
	var questionNumber = figure.attr("module-question");

	// Hide the figure cell
	$(".figure[module-number="+moduleNumber+"][module-question="+questionNumber+"]").removeClass("module-visible");
	$(".figure[module-number="+moduleNumber+"][module-question="+questionNumber+"]").addClass("module-hidden");
	// Show the question cell
	$(".module-question[module-number="+moduleNumber+"][module-question="+questionNumber+"]").removeClass("module-hidden");
	$(".module-question[module-number="+moduleNumber+"][module-question="+questionNumber+"]").addClass("module-visible");

})

$(document).on("click", "input[type=radio]", function(){

	var moduleQuestion = $(this).parents("div.module-question");
	var moduleNumber = moduleQuestion.attr("module-number");
	var questionNumber = moduleQuestion.attr("module-question");

	// Get the value of the radio button
	var answer = $(this).val();

	// Hide the question cell
	$(".module-question[module-number="+moduleNumber+"][module-question="+questionNumber+"]").removeClass("module-visible");
	$(".module-question[module-number="+moduleNumber+"][module-question="+questionNumber+"]").addClass("module-hidden");
	// Show the answer cell
	$(".module-answer[module-number="+moduleNumber+"][module-question="+questionNumber+"]").removeClass("module-hidden");
	$(".module-answer[module-number="+moduleNumber+"][module-question="+questionNumber+"]").addClass("module-visible");

	// Show the selected answer in the answer cell HTML
	$(".module-selection").html("You chose "+answer+".");

})