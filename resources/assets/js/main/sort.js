// DOM modification once jQuery is "ready"
$(function() {

    var closeIcon   = $('.sort-close'),
        $container  = $('.sort-container'),
        $list       = $container.find('ul'),
        $links      = $container.find('a'),
        $text       = $container.find('span');



    $(closeIcon).on('click', function() {
        
        $(this).toggleClass('active');
        $list.toggle();

    });

    $(closeIcon).hover(function() { 
        
        $container.addClass('hover');

    }, function() { 

        $container.removeClass('hover');

    });


    $links.on('click', function() {
    
        $links.removeClass('active');
        $(this).addClass('active');
        $text.text($(this).text());


        $list.toggle();
        closeIcon.toggleClass('active');

    });

});