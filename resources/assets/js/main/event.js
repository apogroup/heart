var eventTime= 1472079600; // Timestamp - Wed, 24 Aug 2016 23:00:00 GMT/UTC  8/24/2016, 7:00:00 PM EDT
// var startTime = 1470009600; // Timestamp - Mon, 01 Aug 2016 00:00:00 GMT/UTC
var startTime = moment().utc().unix(); // Now
var diffTime = eventTime - startTime;
// var clock = $('.clock').FlipClock(diffTime, {
// 			    clockFace: 'DailyCounter',
// 			    countdown: true
// 			});


function registerForEvent(user_id, event, csrf) {

    console.log('eReg');
    
    // Internal Database Reporting

	$.ajaxSetup({
		headers: {
		// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		'X-CSRF-TOKEN': csrf
		}
	});

	var $data = {};
    $data.user_id   = user_id;
    $data.event 	= event;

    $.ajax({
        type: "POST",
        url: './eventregister',

        data: $data,
        success: function() {
            // console.log('DB_LOGGED: Event Registeration: '+ $data.event +' by User '+ $data.user_id);
        },
        error: function(e)
        {  
        	console.log(e.responseText);  
        } 
    });

    $('.register-event-btn').fadeOut('fast');
    $('.registered-hidden').fadeIn('slow');

}