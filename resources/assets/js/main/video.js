var vimeoPlayer;

// DOM modification once jQuery is "ready"
$(function() {

	//Video events

	$('.video-modal').on('hidden.bs.modal', function () {

		var iframe = $('#'+$(this).attr('id')+' iframe');
		var src = iframe.attr('src');

		iframe.attr('src', '');
		iframe.attr('src', src);

	    destroyVimeo();

  	});

  	$('.video-module-modal').on('hidden.bs.modal', function () {

	    destroyVimeo();

  	});

  


}); //end doc ready

function initVimeo(id) {

	var iframe = $('div[video-id='+id+'] iframe');
    vimeoPlayer = new Vimeo.Player(iframe);

}

function initModuleVimeo(id, callback) {

	var iframe = $('div[video-id='+id+'] iframe');
    var vimeo = new Vimeo.Player(iframe);

    return callback(vimeo);

}

function destroyVimeo() {

	vimeoPlayer.pause();
	vimeoPlayer = null;

}