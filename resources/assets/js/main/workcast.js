window.onload = function() {

	var wcLoading = setInterval(function(){ wcListener(); }, 250);

	function wcListener() {

		if($("#wcint_playercontainer").hasClass("wcint_loading")){} else{

			clearInterval(wcLoading);
			initWCForm();

		}

	}

}

function initWCForm(){

	$("#wcintaq_prompt").detach().appendTo(".workcast-question-form-prompt");
	$("#wcintgl_inlinequestion").detach().appendTo(".workcast-question-form-inline");

	$("#wcintaq_question").attr("rows", "3");
	$("#wcintaq_question").val("Enter your question here");
	$("#wcintaq_question").css("color", "#cccccc");

	$("#wcintaq_ok").html('SEND QUESTION');
	$("#wcintaq_ok").on("click", function(){
		//clears textarea on submit
		setTimeout(function(){
			$("#wcintaq_question").val("Enter your question here");
			$("#wcintaq_question").css("color", "#cccccc");
		}, 500);
	});

	$("#wcintaq_question").focus(function() {
		$("#wcintaq_question").val("");
		$("#wcintaq_question").css("color", "#000000");
	});
}


