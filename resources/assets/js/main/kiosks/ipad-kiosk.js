function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

var HCP=getCookie("hcp");
var idleSeconds = 60;
var isIdle = false;
var idleTimer;
var resetCount;
var restartTimeout;

$(function(){

	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	var isiPadWebApp = window.navigator.standalone;

	// alert("checkingIPAD");

	console.log("Validate iPad Kiosk");

	if(isiPad && isiPadWebApp) {

		// alert("isIpad");

		console.log("iPad Kiosk Mode");

		// Fastclick Initialize

			FastClick.attach(document.body);


		// Redirect Manipulation

			if( window.location.href == 'http://theheartcommunity.com/login' ) window.location.href = 'http://theheartcommunity.com/register';

			if ( $('.sidebar-menubtn-label').first().text() == "Login" ) $('.sidebar-menubtn').first().attr("href", "http://theheartcommunity.com/login?login=true"); 


		// External Link Blocking

			$("a[target='_blank']").attr('target', '').attr('href', '#');

			$('.external-link-disclaimer').html("Please view the article at the link provided below:");

			$('.confirm-no-btn a').attr("href", "#");

			var a=document.getElementsByTagName("a");
			for(var i=0;i<a.length;i++)
			{
			    a[i].onclick=function()
			    {
			        window.location=this.getAttribute("href");
			        return false
			    }
			}

		// Initialize videos on launch

		// $('.video-launch-btn').on('click', function(){

		// 	var vidID = $(this).attr("video-id");

		// 	initalizeVideo(vidID);

		// });


		// Reset Timer

			// Start Reset Timer on Page Load

			if(HCP) resetTimer();

			// Reset the timer on body click

			$(document.body).click(function(){ if(HCP && !isIdle) resetTimer(); });

	        // Reset the timer on confirmation button click

	        $('#confimation-yes-button').on('click', function(){ resetTimer(); });

	        // Reset modal option button 

	        $('.reset-no-btn').on('click', function(){

				isIdle = false;

				clearInterval(resetCount);

				resetTimer();

				$('.timeout-modal').modal('hide', function(e){

					$('.reset-yes-btn input').val("Resetting in 30 seconds");

				});

			});

			$('.reset-yes-btn').on('click', function(){

				restart();

			});


	        // Reset Helper Functions

			function resetTimer(){

				clearTimeout(idleTimer);

				idleTimer = setTimeout( idle, idleSeconds*1000 );
			
			}

			function restartCountDown(){

				clearInterval(resetCount);

				var resetCountSec = 30;
				resetCount = setInterval(function() {

					$('.reset-yes-btn input').val("Resetting in "+resetCountSec--+" seconds");

					console.log(resetCountSec);

					if (resetCountSec == -1) {

				    	restart();

					}
				}, 1000);

			}

			function idle(){

				isIdle= true;

				clearTimeout(idleTimer);

				restartCountDown();

				$('.timeout-modal').modal('show');

			}

			function restart(){

				window.location.href = 'http://theheartcommunity.com/logout';

			}

	} //endif

});