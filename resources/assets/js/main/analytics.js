
function markResourceCompleted(user_id, resource_id, resource_type) {

    // console.log('mRC');
    
    // Internal Database Reporting

	$.ajaxSetup({
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var $data = {};
    $data.user_id       = user_id;
    $data.resource_id   = resource_id;
    $data.resource_type = resource_type;

    $.ajax({
        type: "POST",
        url: './markcompleted',

        data: $data,
        success: function() {
            // console.log('DB_LOGGED: Resource Completed: '+ $data.resource_id +' ('+$data.resource_type+') by User '+ $data.user_id);
        },
        error: function(e)
        {  
        	console.log(e.responseText);  
        } 
    });


    // Google Analytics

    var resource_label = resource_type+'-'+resource_id;

	ga('send', 'event', resource_type, 'completed', resource_label);





}

function markResourceViewed(user_id, resource_id, resource_type) {

    // console.log('mRV');

    // Internal Database Reporting

	$.ajaxSetup({
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var $data           		= {};
        $data.user_id          	= user_id;
        $data.resource_id       = resource_id;
        $data.resource_type     = resource_type;

    $.ajax({
        type: "POST",
        url: './markviewed',
        data: $data,
        success: function() {
            // console.log('DB_LOGGED: Resource Viewed: '+ $data.resource_id +' ('+$data.resource_type+') by User '+ $data.user_id);
        },
        error: function(e)
        {  
        	console.log(e.responseText);  
        } 
    });


    // Google Analytics

    var resource_label = resource_type+'-'+resource_id;
    
	ga('send', 'event', resource_type, 'viewed', resource_label);

}