////MODULE


	function clinicInitFlick(id){
		
		// Let's not overload the DOM with multiple instances of this function
		// e.preventDefault();

		// Store our learning resource/ module id in an obj
		// var moduleid = $(this).attr('data-resource');
		var moduleid = id;

		// console.log(moduleid);

		// Init flicity instance
		$flick = $('#module-'+moduleid+'-gallery').flickity({ cellAlign: 'left', contain: true, draggable: false, setGallerySize: false })

			.on('cellSelect', function() {

				// console.log('cellselected');

				setTimeout(function(){

					var flkty = $flick.data('flickity');

					var index = flkty.selectedIndex-1;

					// Mobile Page Dots Alternative
					var cellNumber = index + 2;
  					$('.gallery-status').html('' + cellNumber + ' <sup>of</sup> ' + flkty.cells.length + '');

					// METRICS
					if(index!=-1){
						var userId = $("#ga-data").attr('user-id');
						var resourceId = moduleid+'.'+index;

						// console.log(userId);
						// console.log(resourceId);

						markResourceViewed(userId,resourceId,'clinic-module-chapter');

						if($('.chapter-'+index).attr('metric') == 'completion'){

							markResourceCompleted(userId,moduleid,'clinic-module');

						}
					}
					//

					$(".chapter-btn").removeClass("current-chapter");

					$('.chapter-'+index).addClass('current-chapter');

					index++;

					var chapter = index-1;
						chapter = 'chapter-'+chapter;
					chapterScroll(chapter);

					$('.cell-scroll').scrollTop(0);



				},100);

			});

		$('.gallery-status').html('1 <sup>of</sup> ' + $flick.data('flickity').cells.length + '');

	}


  	function getClinicModule(id) {

  		if($('.module-modal .module-wrapper').length) $('.module-modal .module-wrapper').remove();

		$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

	    $.ajax({
	        type: "GET",
	        url: './module/'+id+'',
	        success: function(data) {
	            loadClinicModule(id, data);

	        },
	        error: function(e)
            {  
            	// console.log(e.responseText);  
            } 
	    })

	}


	function loadClinicModule(id, data){

		$('div.modal-body-'+id+'').html(""+data+"");

		clinicInitFlick(id);

		// Listen for our click to open a module modal

			$('.module-modal-close').on('click', function(e){

				// console.log('clearing module');

				// Let's not overload the DOM with multiple instances of this function
				e.preventDefault();

				// Store our learning resource/ module id in an obj
				var moduleid = $(this).attr('data-resource');

				//Destroy flickity instance
				$flick = $('#module-'+moduleid+'-gallery').flickity('destroy');

				$('.chapter-btn').removeClass('current-chapter');

			});


			// On sidebar home click

			$('.module-home-btn').on( 'click', function(){

				// console.log('changing to home');

				$flick.flickity( 'select', 0 );		

				$('.chapter-btn').removeClass("current-chapter");

			});


			// On sidebar button click
			$('.module-menu-buttons').on( 'click', '.chapter-btn', function() {

				// console.log('changing chapter');

				// Get the index (chapter number) // add 1 for module intro page adjustment
				var index = $(this).index()+1;

				// Move to this selected chapter in flickity
				$flick.flickity( 'select', index );
				
				// Remove the current-chapter button (only on the specific module gallery menu)
				$(this).siblings().removeClass("current-chapter");

				// Add the current-chapter class to the clicked button
				$(this).addClass("current-chapter");
				
			});
	}
