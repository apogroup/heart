@extends('layouts.default')

@section('content')

	<div class="error">
	    
	    <div class="card card-header centered-card error-card">
	        <div class="card-content">
	            <h1></h1>
	        
	            <h2><i class="icon-heart"></i>heart</h2>
	            <h4>The online educational community for <span>clinicians treating heart failure</span></h4>

	            <p>&nbsp;</p>
	            <p>Sorry, looks like something went wrong. We track these errors automatically, but if the problem persists feel free to contact us. In the meantime, try refreshing.</p>

	        </div> <!-- .card-content -->
	    </div> <!-- .centered-card -->

	</div> <!-- .login -->

@endsection