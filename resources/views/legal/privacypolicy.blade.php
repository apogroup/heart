<div class="legal">

	<div class="card card-header privacy-policy">
		<div class="card-content">
			
			@include('partials.privacypolicy');
			
		</div>
	</div> <!-- .privacy-policy -->

</div>