@extends('layouts.default')

@section('content')

	<div class="login">
		
		<div class="card card-header centered-card login-card">
			<div class="card-video">

				<h2 class="menu-hamburger"><i class="material-icons icon-menu">&#xE5D2;</i></h2>
				<h2 class="heart-welcome">Welcome to <span>heart</span></h2>

				<div class="video-wrapper-169">
					<iframe src="https://player.vimeo.com/video/177232481" width="100%" height="auto" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>

				<h3>WHY JOIN THEHEARTCOMMUNITY.COM?</h3>

				<p>Play this video to hear leading cardiologists discuss TheHeartCommunity.com and why it is the trusted resource and online educational community for clinicians treating heart failure.</p>

			</div>
			<div class="card-content">
				<h1>Login</h1>
			
				<h2><i class="icon-heart"></i>heart</h2>
				<h4>The online educational community for <span>clinicians treating heart failure</span></h4>
			
				<div id="post-confirmation">

					<p class="login-message">Welcome to <strong>heart</strong>! Log in to access your online educational community and resources. Not a member? <a href="{{ url('/register') }}">Join now</a></p> 
					
					<div class="login-form login-field-wrapper">
						{!! Form::open(array('id' => 'login', 'method' => 'POST',  url('/login'), 'class' => 'white-row')) !!}

						{!! Form::token() !!}

							{!! Form::email('email', old('email'), array('required' => 'required', 'placeholder'=>'Email')) !!}

							{!! Form::password('password', array('required' => 'required', 'placeholder' => 'Password')) !!}

								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif

								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif

							<div class="remember-me">
								{!! Form::label('remember', 'Remember me') !!}
								{!! Form::checkbox('remember', null) !!}
							</div>
							
							<div class="submit-btn">
								{!! Form::submit('Sign In', array('class' => 'btn btn-primary')) !!}
							</div>

						{!! Form::close() !!}
					</div>

					<p class="forgot-password"><a href="{{ url('/password/reset') }}">FORGOT PASSWORD?</a></p>

					<p class="bottom-join">Not a member? <a href="{{ url('/register') }}">Join now</a></p>
				
				</div> <!-- .post-confirmation -->

			</div> <!-- .card-content -->
		</div> <!-- .centered-card -->

	</div> <!-- .login -->

@endsection
