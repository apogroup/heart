@extends('layouts.default')

@section('content')

	<div class="register">

		<div class="card card-header centered-card register-card">
			<div class="card-content">

				<h1>Create Your Account</h1>

				<h2 class="menu-hamburger"><i class="material-icons icon-menu">&#xE5D2;</i></h2>
				<h2 class="heart-logo"><i class="icon-heart"></i>heart</h2>
				<h4>The online educational community for <span>clinicians treating heart failure</span></h4>

				{{-- <div class="event-container register-event">
					<div class="headshot-block">
						<img src="{{ asset('assets/css/img/butler_headshot.png') }}" width="110" height="154" alt="butler_headshot" />
					</div>
					<div class="event-container-content">
						<h3>REGISTER NOW TO BE A PART OF:</h3>

						<p>Bringing the New Heart Failure Guidelines to Life: The 2016 ACC/AHA/HFSA Focused Update on New Pharmacological Therapy</p>

						<div class="presented-block">
							<h3>PRESENTED BY:</h3>

							<h2>Javed Butler</h2>

							<h4>MD, MPH, MBA</h4>
						</div>

						<div class="time-block">
							<h3><span>LIVE!</span> 7:00 PM EDT</h3>

							<p>Wednesday, August 24, 2016</p>
						</div>

					</div>
				</div> --}}

				<p class="login-message">Welcome to <strong>heart</strong>, the online educational community for clinicians treating heart failure. To access all of the available resources provided on this website, you must first register.</p>

				<p class="register-message">You must be a US health care professional to join <strong>heart</strong>.</p>

				<div class="login-form login-field-wrapper">
					{!! Form::open(array('id' => 'register', 'method' => 'POST', url('/register'), 'class' => 'white-row')) !!}

					{!! Form::token() !!}

						{!! Form::text('specialty', null, array('required' => 'required', 'placeholder'=>'Specialty')) !!}

							@if ($errors->has('specialty'))
								<span class="help-block">
									<strong>{{ $errors->first('specialty') }}</strong>
								</span>
							@endif

						{!! Form::text('firstname', null, array('required' => 'required', 'placeholder'=>'First Name')) !!}

							@if ($errors->has('fname'))
								<span class="help-block">
									<strong>{{ $errors->first('fname') }}</strong>
								</span>
							@endif

						{!! Form::text('lastname', null, array('required' => 'required', 'placeholder'=>'Last Name')) !!}

							@if ($errors->has('lname'))
								<span class="help-block">
									<strong>{{ $errors->first('lname') }}</strong>
								</span>
							@endif

						{!! Form::text('email', null, array('required' => 'required', 'placeholder'=>'Email')) !!}

							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif

						{!! Form::password('password', array('required' => 'required', 'placeholder'=>'Password')) !!}

						{!! Form::password('password_confirmation', array('required' => 'required', 'placeholder'=>'Confirm Password')) !!}

							@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif

							@if ($errors->has('password_confirmation'))
								<span class="help-block">
									<strong>{{ $errors->first('password_confirmation') }}</strong>
								</span>
							@endif

						<p class="attestation">By clicking Register, you accept the <a data-toggle="modal" data-target="#termsOfServiceModal">Terms of Use</a> and <a data-toggle="modal" data-target="#privacyPolicyModal">Privacy Policy</a>, and attest you are a US health care professional.</p>

						<div class="submit-btn">
							{!! Form::submit('Register', array('class' => 'btn'))!!}
						</div>

					{!! Form::close() !!}
				</div>
		
			</div> <!-- .card-content -->
		</div> <!-- .centered-card -->

	</div> <!-- .register -->

@endsection