@extends('layouts.default')

@section('content')

    <div class="reset">

        <div class="card card-header centered-card reset-card">
            <div class="card-content">

                <h1>Reset Password</h1>

                <h2><i class="icon-heart"></i>heart</h2>
                <h4>The online educational community for <span>clinicians treating heart failure</span></h4>
                
                <div class="login-form">
                    {!! Form::open(array('id' => 'reset', 'method' => 'POST', 'url' => url('/password/reset'))) !!}

                    {!! Form::token() !!}

                        {!! Form::hidden('token', $token) !!}

                        {!! Form::text('email', $email, array('required' => 'required', 'placeholder'=>'Email')) !!}

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                        {!! Form::password('password', array('required' => 'required', 'placeholder'=>'Enter New Password')) !!}

                        {!! Form::password('password_confirmation', array('required' => 'required', 'placeholder'=>'Confirm New Password')) !!}

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif

                        <div class="submit-btn">
                            {!! Form::submit('Reset Password', array('class' => 'btn'))!!}
                        </div>

                    {!! Form::close() !!}
                </div>
        
            </div> <!-- .card-content -->
        </div> <!-- .centered-card -->

    </div> <!-- .reset -->

@endsection
