@extends('layouts.default')

@section('content')

    <div class="password">

        <div class="card card-header centered-card forgot-card">
            <div class="card-content">

                <h1>Forgot Your Password?</h1>

                <h2><i class="icon-heart"></i>heart</h2>
                <h4>The online educational community for <span>clinicians treating heart failure</span></h4>
        
                <div class="login-form">
                    {!! Form::open(array('id' => 'email_reset', 'method' => 'POST', 'url' => url('/password/email'))) !!}

                        {!! Form::token() !!}

                        @if (Session::has('status'))

                            <div class="alert alert-info">
                                {!! Session::pull('status') !!}
                            </div>

                        @else
                        
                            <p class="forgot-message">Please enter your email address below and click Reset Password. We will send a message to your email address with a link to reset your password.</p>
                            
                            <div class="login-field-wrapper">
                                {!! Form::text('email', null, array('required' => 'required', 'placeholder'=>'Email')) !!}
                            </div>
                            
                            <div class="submit-btn">
                                <button type="submit" class="btn">Reset Password</button>
                            </div>

                        @endif

                    {!!Form::close()!!}
                </div>
        
            </div> <!-- .card-content -->
        </div> <!-- .centered-card -->

    </div> <!-- .password -->

@endsection
