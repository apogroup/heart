<p style="font-family: Arial,Helvetica,Sans-Serif;font-size:12pt;color:#474747;font-weight:bold;">Welcome and thank you for signing up for heart: the online educational community for clinicians treating heart failure!</p>

<p style="font-family: Arial,Helvetica,Sans-Serif;font-size:12pt;color:#474747;font-weight:bold;">Please save this email for future reference. As a reminder, your email address is your username. If you forget your password, please reset it using the <a href="http://www.theheartcommunity.com/password/reset" style="color:#c71d23;">FORGOT PASSWORD?</a> link on the login screen.</p>
 
<p style="font-family: Arial,Helvetica,Sans-Serif;font-size:12pt;color:#474747;font-weight:bold;">If you have any questions, please contact us via one of the following channels:</p>

<table>
	<tr>
		<td width=20 valign="top"><p style="font-family: Arial,Helvetica,Sans-Serif;font-size:12pt;color:#474747;">1.</p></td>
		<td><p style="font-family: Arial,Helvetica,Sans-Serif;font-size:12pt;color:#474747;">Call Novartis Medical Information with questions about our products or services. Call us Monday–Friday from 8:30 AM to 5:00 PM ET at 1-855-ASK-4MIC (1-855-275-4642).</p></td>
	</tr>
	<tr>
		<td width=20 valign="top"><p style="font-family: Arial,Helvetica,Sans-Serif;font-size:12pt;color:#474747;">2.</p></td>
		<td><p style="font-family: Arial,Helvetica,Sans-Serif;font-size:12pt;color:#474747;">For technical support queries related to the heart website, email us at <a href="mailto:support@theheartcommunity.com" style="color:#c71d23;">support@TheHeartCommunity.com</a>. Please include your username (if applicable) and preferred mode of contact. We will do our best to respond to your message within 2 business days.</p></td>
	</tr>
</table>
