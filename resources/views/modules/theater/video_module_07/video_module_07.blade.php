<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Roundtable Discussion: Neurohormonal Mechanisms That Drive Heart Failure (HF) Progression</h3>
			
			<ul class="module-menu-buttons" video-module="7">

				<li class="chapter-btn chapter-0"><a>1. Introduction with Frank Spinale, MD</a></li>
				<li class="chapter-btn chapter-1"><a>2. Current Challenges in HF</a></li>
				<li class="chapter-btn chapter-2"><a>3. Neurohormonal Pathways in the Development of HF</a></li>
				<li class="chapter-btn chapter-3"><a>4. The Other Half of the Story: Endogenous Compensatory Peptides</a></li>
				<li class="chapter-btn chapter-4"><a>5. The Role of Neurohormonal Imbalance in the Progression of HF</a></li>
				<li class="chapter-btn chapter-5" metric="completion"><a>6. Summary</a></li>

				{{-- <li class="chapter-btn chapter-7" onclick="markResourceCompleted({{ Auth::user()->id }},'1','learningresource');"><a>References</a></li>  --}}
				
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>CHAPTER</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-7-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-7-container-0" video-id="mod-7-chapter-0">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-7-container-1" video-id="mod-7-chapter-1">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-7-container-2" video-id="mod-7-chapter-2">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-7-container-3" video-id="mod-7-chapter-3">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-7-container-4" video-id="mod-7-chapter-4">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">


						<div class="container video-mod-7-container-5" video-id="mod-7-chapter-5">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>


							
		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->