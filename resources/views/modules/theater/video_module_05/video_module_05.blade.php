<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">The Myth of the Clinically Stable Patient with Heart Failure: Exploring Clinical Trajectories and the Threat of Sudden Cardiac Death</h3>
			
			<ul class="module-menu-buttons" video-module="5">

				<li class="chapter-btn chapter-0"><a>1. Introduction with Javed Butler, MD, MPH, MBA</a></li>
				<li class="chapter-btn chapter-1"><a>2. Patient Case</a></li>
				<li class="chapter-btn chapter-2"><a>3. HF Mortality Remains High Despite Treatment Advances</a></li>
				<li class="chapter-btn chapter-3"><a>4. Residual Risk for Symptomatic HF Patients Based on Previous Clinical Trials</a></li>
				<li class="chapter-btn chapter-4"><a>5. Section Summary: Need For Aggressive Management</a></li>
				<li class="chapter-btn chapter-5"><a>6. HF Mortality and Risk of SCD Remain High</a></li>
				<li class="chapter-btn chapter-6"><a>7. The Unpredictable Clinical Course of HF</a></li>
				<li class="chapter-btn chapter-7"><a>8. The Pathophysiology of HF</a></li>
				<li class="chapter-btn chapter-8"><a>9. Risk Factors for SCD in HF</a></li>
				<li class="chapter-btn chapter-9"><a>10. Summary</a></li>
				<li class="chapter-btn chapter-10"><a>11. Question 1: Possible Reasoning Behind Increased HF Mortality?</a></li>
				<li class="chapter-btn chapter-11"><a>12. Question 2: Determinants of HF Clinical Trajectories?</a></li>
				<li class="chapter-btn chapter-12"><a>13. Question 3: Reasons for SCD Rates by NYHA Class?</a></li>
				<li class="chapter-btn chapter-13"><a>14. Question 4: Role of Microvasculature in HF?</a></li>
				<li class="chapter-btn chapter-14"><a>15. Question 5: Why Modest Improvement in Survival in HF Trials?</a></li>
				<li class="chapter-btn chapter-15" metric="completion"><a>16. Question 6: Prevalence of Myocardial Recovery?</a></li>

				{{-- <li class="chapter-btn chapter-5" onclick="markResourceCompleted({{ Auth::user()->id }},'1','learningresource');"><a>References</a></li>  --}}
				
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>CHAPTER</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-5-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-5-container-0" video-id="mod-5-chapter-0">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Introduction with Javed Butler, MD, MPH, MBA</h1> -->

						<div class="container video-mod-5-container-1" video-id="mod-5-chapter-1">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Patient Case</h1> -->

						<div class="container video-mod-5-container-2" video-id="mod-5-chapter-2">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>HF Mortality Remains High Despite Treatment Advances</h1> -->

						<div class="container video-mod-5-container-3" video-id="mod-5-chapter-3">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Residual Risk for Symptomatic HF Patients Based on Previous Clinical Trials</h1> -->

						<div class="container video-mod-5-container-4" video-id="mod-5-chapter-4">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Section Summary: Need For Aggressive Management</h1> -->

						<div class="container video-mod-5-container-5" video-id="mod-5-chapter-5">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>HF Mortality and Risk of SCD Remain High</h1> -->

						<div class="container video-mod-5-container-6" video-id="mod-5-chapter-6">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>The Unpredictable Clinical Course of HF</h1> -->

						<div class="container video-mod-5-container-7" video-id="mod-5-chapter-7">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>The Pathophysiology of HF</h1> -->

						<div class="container video-mod-5-container-8" video-id="mod-5-chapter-8">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Risk Factors for SCD in HF</h1> -->

						<div class="container video-mod-5-container-9" video-id="mod-5-chapter-9">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Summary</h1> -->

						<div class="container video-mod-5-container-10" video-id="mod-5-chapter-10">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Question 1: Possible Reasoning Behind Increased HF Mortality?</h1> -->

						<div class="container video-mod-5-container-11" video-id="mod-5-chapter-11">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Question 2: Determinants of HF Clinical Trajectories?</h1> -->

						<div class="container video-mod-5-container-12" video-id="mod-5-chapter-12">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Question 3: Reasons for SCD Rates by NYHA Class?</h1> -->

						<div class="container video-mod-5-container-13" video-id="mod-5-chapter-13">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Question 4: Role of Microvasculature in HF?</h1> -->

						<div class="container video-mod-5-container-14" video-id="mod-5-chapter-14">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Question 5: Why Modest Improvement in Survival in HF Trials?</h1> -->

						<div class="container video-mod-5-container-15" video-id="mod-5-chapter-15">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>

							
		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->