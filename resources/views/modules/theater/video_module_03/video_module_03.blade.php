<div class="module-wrapper">
				
	<div class="modal-sidebar module-3-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Targeting Neurohormones in Heart Failure: The Potential Role of Endogenous Compensatory Peptides</h3>
			
			<ul class="module-menu-buttons" video-module="3">

				<li class="chapter-btn chapter-0"><a>1. Current Use of ACE Inhibitors and ARBs in HF</a></li>
				<li class="chapter-btn chapter-1"><a>2. The Problem With Current Inhibitors of the Renin-Angiotensin System</a></li>
				<li class="chapter-btn chapter-2" metric="completion"><a>3. Biological Antagonism with ECPs in HF</a></li>

				{{-- <li class="chapter-btn chapter-6" onclick="markResourceCompleted({{ Auth::user()->id }},'1','learningresource');"><a>References</a></li> --}}
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>CHAPTER</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-3-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-3-container-0" video-id="mod-3-chapter-0">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>Current Use of ACE Inhibitors and ARBs in HF</h1> -->

						<div class="container video-mod-3-container-1" video-id="mod-3-chapter-1">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<!-- <h1>The Problem With Current Inhibitors of the Renin-Angiotensin System</h1> -->

						<div class="container video-mod-3-container-2" video-id="mod-3-chapter-2">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>
							
		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->