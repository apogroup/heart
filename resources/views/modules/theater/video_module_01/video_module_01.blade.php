<div class="module-wrapper">

	<div class="modal-sidebar module-1-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">The Myth of the Clinically Stable Patient with Heart Failure</h3>
			
			<ul class="module-menu-buttons" video-module="1">

				<li class="chapter-btn chapter-0"><a>1. Introduction with Inder Anand, PhD</a></li>
				<li class="chapter-btn chapter-1"><a>2. Patient Case</a></li>
				<li class="chapter-btn chapter-2"><a>3. HF Mortality Remains High Despite Treatment Advances</a></li>
				<li class="chapter-btn chapter-3"><a>4. HF Mortality and Risk of SCD Remains High Despite Maximal Care With Recommended Therapies and ICDs</a></li>
				<li class="chapter-btn chapter-4"><a>5. The Unpredictable Clinical Course of HF</a></li>
				<li class="chapter-btn chapter-5"><a>6. Risk Factors for SCD in HF</a></li>
				<li class="chapter-btn chapter-6" metric="completion"><a>7. Summary</a></li>
<!-- 				<li class="chapter-btn chapter-7"><a>8. Question: Why have improvements in survival been modest in patients with HFrEF throughout the years?</a></li>
				<li class="chapter-btn chapter-8"><a>9. Question: Why does SCD occur in patients with HF?</a></li>
				<li class="chapter-btn chapter-9"><a>10. Question: Why are rates of SCD lower in Stage III and IV HF compared with Stage II?</a></li>
				<li class="chapter-btn chapter-10"><a>11. Question: What are the common risk factors for SCD in your practice?</a></li>
				<li class="chapter-btn chapter-11"><a>12. Question: What has been your clinical experience with SCD in patients with heart failure?</a></li>
				<li class="chapter-btn chapter-12"><a>13. Question: What is the clinical utility of measuring myocardial scar to predict SCD?</a></li>
				<li class="chapter-btn chapter-13"><a>14. Question: When is an ICD indicated?</a></li>
				<li class="chapter-btn chapter-14" metric="completion"><a>15. Question: Does an ACE and ARB combination help in HF?</a></li> -->

				{{-- <li class="chapter-btn chapter-6" onclick="markResourceCompleted({{ Auth::user()->id }},'1','learningresource');"><a>References</a></li> --}}
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>CHAPTER</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-1-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-0" video-id="mod-1-chapter-0">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-1" video-id="mod-1-chapter-1">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-2" video-id="mod-1-chapter-2">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-3" video-id="mod-1-chapter-3">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-4" video-id="mod-1-chapter-4">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-5" video-id="mod-1-chapter-5">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-6" video-id="mod-1-chapter-6">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->

					</div>
				</div>
			</div>

<!-- 			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-7" video-id="mod-1-chapter-7">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> --> <!-- .container -->

<!-- 					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-8" video-id="mod-1-chapter-8">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> --> <!-- .container -->

<!-- 					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-9" video-id="mod-1-chapter-9">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>  --><!-- .container -->

<!-- 					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-10" video-id="mod-1-chapter-10">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>  --><!-- .container -->

<!-- 					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-11" video-id="mod-1-chapter-11">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>  --><!-- .container -->

<!-- 					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-12" video-id="mod-1-chapter-12">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>  --><!-- .container -->

<!-- 					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-13" video-id="mod-1-chapter-13">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>  --><!-- .container -->

<!-- 					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-1-container-14" video-id="mod-1-chapter-14">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>  --><!-- .container -->

<!-- 					</div>
				</div>
			</div> -->
							
		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->