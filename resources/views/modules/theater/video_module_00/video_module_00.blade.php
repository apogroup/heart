<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">The War on Heart Failure</h3>
			
			<ul class="module-menu-buttons" video-module="0">

				<li class="chapter-btn chapter-0"><a>1. Heart Failure Disease State Overview and Challenges</a></li>
				<li class="chapter-btn chapter-1"><a>2. Advances in heart failure treatment options</a></li>
				<li class="chapter-btn chapter-2"><a>3. Possible Future Treatment Options: miRNAs</a></li>
				<li class="chapter-btn chapter-3"><a>4. Possible Future Treatment Options: Contractile Process</a></li>
				<li class="chapter-btn chapter-4"><a>5. Possible Future Treatment Options: Gene Therapy</a></li>
				<li class="chapter-btn chapter-5"><a>6. Possible Future Treatment Options: Cell Therapy</a></li>
				<li class="chapter-btn chapter-6"><a>7. Possible Future Treatment Options: Anti-Fibrotic</a></li>
				<li class="chapter-btn chapter-7" metric="completion"><a>8. Conclusions</a></li>

			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>CHAPTER</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-0-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-0" video-id="mod-0-chapter-0">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div> <!-- .container -->
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-1" video-id="mod-0-chapter-1">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-2" video-id="mod-0-chapter-2">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-3" video-id="mod-0-chapter-3">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-4" video-id="mod-0-chapter-4">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-5" video-id="mod-0-chapter-5">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-6" video-id="mod-0-chapter-6">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>

					</div>
				</div>
			</div>
							
			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<div class="container video-mod-0-container-7" video-id="mod-0-chapter-7">

							<iframe src="" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

						</div>

					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->