<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 4: Adrenomedullin: More Than Just a Potential Marker in Heart Failure</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1. Adrenomedullin Synthesis and Localization</a></li>
				<li class="chapter-btn chapter-2"><a>2. Adrenomedullin Vasodilatory and Hypotensive effects</a></li>
				<li class="chapter-btn chapter-3"><a>3. Adrenomedullin as a Therapeutic Target</a></li>
				<li class="chapter-btn chapter-4"><a>4. Adrenomedullin as a Biomarker</a></li>
				<li class="chapter-btn chapter-5" metric="completion"><a>5. Summary</a></li>
				<li class="chapter-btn chapter-6"><a>References</a></li>
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-4-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>Module 4: Adrenomedullin: More Than Just a Potential Marker in Heart Failure</h1>

						<h2>Module Description</h2>

						<p>Adrenomedullin is an endogenous compensatory peptide that plays an important role in cardiorenal diseases, such as heart failure, through its vasodilatory and natriuretic properties to maintain physiological cardiovascular and renal homeostasis. Explore the potential of adrenomedullin, not only as a biomarker, but also a potential therapeutic target in heart failure.</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Characterize the compensatory role of adrenomedullin in counterbalancing excessive peripheral vasoconstriction in heart failure (HF)</p></li>
							<li><p>Review data indicating the superiority of adrenomedullin as a biomarker for short-term risk of HF-related mortality compared with brain natriuretic peptide</p></li>
						</ul>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p>A growing body of evidence suggests that the hemodynamically active, vasodilatory, endogenous compensatory peptide adrenomedullin may play a compensatory role in heart failure (HF) by counterbalancing the deleterious effects of excessive peripheral vasoconstriction <strong>[Figure 1]</strong>.<sup>1</sup> Adrenomedullin possesses hypotensive activities that comprise vasodilatory, natriuretic, diuretic, antifibrotic, and possible inotropic effects.<sup>2,3</sup> In HF, circulating levels of adrenomedullin are increased in proportion to the severity of disease. Indeed, the ability of a prohormone of adrenomedullin to predict HF-related death has been confirmed in a clinical trial.<sup>1</sup> Furthermore, adrenomedullin may also have a unique ability as a biomarker to identify patients at short-term risk of HF-related mortality and may represent a means to potentially identify patients in need of more aggressive therapy.<sup>3</sup></p>

						<div class="figure">
							<h4>Figure 1. Adrenomedullin counterbalances the deleterious effects of vasoconstriction in HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-1.png') }}" width="594" height="293" alt="figure-1">
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>1. Adrenomedullin Synthesis and Localization</h2>

						<p>Adrenomedullin is a hypotensive endogenous compensatory peptide originally isolated from human pheochromocytoma tissue, a neuroendocrine tumor of the adrenal medulla.<sup>4</sup> Adrenomedullin is initially expressed as a 185-amino acid precursor (preproADM) that is processed first to a 164-amino acid proADM, then to its biologically active form of 52 amino acids <strong>[Figure 2]</strong>.<sup>2</sup></p> 

						<div class="figure">
							<h4>Figure 2. Adrenomedullin structure and formation</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-2.png') }}" width="594" height="492" alt="figure-2">

							<h5>Jougasaki M, Burnett JC. Adrenomedullin: potential in physiology and pathophysiology. <em>Life Sci</em>. 2000;66(10):855-872. </h5>
						</div>

						<p>Adrenomedullin expression has been observed in multiple regions of the body, including cardiovascular, endocrine, pulmonary, cerebrovascular, gastrointestinal, and renal tissues.<sup>2,3</sup> Adrenomedullin can operate as either an autocrine or paracrine effector, in addition to functioning as a circulating hormone.<sup>3</sup></p> 

						<p>Receptors for adrenomedullin are found in several tissues and in endothelial cells and smooth muscle vascular tissue.<sup>1</sup> Adrenomedullin binds to several G <nobr>protein–coupled</nobr> receptors, including the calcitonin receptor-like receptor (CRLR), and one that is specific to the adrenomedullin peptide itself.<sup>1</sup></p> 

						<p>The ability of adrenomedullin to bind to CRLR depends on which accessory proteins are complexed with the receptor, specifically receptor activity–modifying proteins (RAMPs).<sup>5,6</sup> CRLR functions as an adrenomedullin receptor when complexed with either RAMP2 or RAMP3, but when complexed with RAMP1, CRLR instead serves as a calcitonin gene-related peptide (CGRP) receptor instead <strong>[Figure 3]</strong>.<sup>6,7</sup></p>

						<div class="figure">
							<h4>Figure 3. RAMP/receptor paradigm for adrenomedullin and CGRP signaling</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-3.png') }}" width="594" height="416" alt="figure-3">

							<h5>ADM, adrenomedullin; CGRP, calcitonin gene-related peptide; RAMP, receptor activity–modifying protein.</h5>

							<h5>Gibbons C, Dackor R, Dunworth W, et al. Receptor activity-modifying proteins: RAMPing up adrenomedullin signaling. <em>Mol Endocrinol</em>. 2007;21(4):783-796.</h5>
						</div>

						<div class="figure inline-figure-right" style="width: 408px;">
							<h4>Figure 4. Adrenomedullin exerts multiple physiological and cellular effects</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-4.png') }}" width="370" height="561" alt="figure-4">

							<h5>AC, adenylate cyclase; Ca, calcium; cAMP, cyclic adenosine monophosphate; cGMP, cyclic guanosine monophosphate; ERK, extracellular signal-regulated kinases; MAPK, mitogen-activated protein kinase; NO, nitric oxide; PI3K, phosphoinositide 3-kinase; PKA, protein kinase A.</h5>

							<h5>Gibbons C, Dackor R, Dunworth W, et al. Receptor activity-modifying proteins: RAMPing up adrenomedullin signaling. <em>Mol Endocrinol</em>. 2007;21(4):783-796.</h5>
						</div>

						<p>Although best known as a potent vasodilator with key effects on regulating blood pressure, adrenomedullin exerts multiple physiological and cellular effects on various processes and functions as varied as bronchodilation, metabolism, immunity, and stress response <strong>[Figure 4]</strong>.<sup>6</sup> It has been well documented that adrenomedullin can produce vasodilation of many vascular beds. The vasodilatory effect of adrenomedullin is due, at least in part, to cyclic adenosine monophosphate and to the nitric oxide and cyclic guanosine monophosphate pathways.<sup>6</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>2. Adrenomedullin Vasodilatory and Hypotensive Effects</h2>

						<p>Adrenomedullin is a vasodilatory agent that is increased in patients with HF. Elevated levels of adrenomedullin are observed in the plasma of patients with HF proportionate to the intensity of cardiac and hemodynamic injury.<sup>1</sup> Adrenomedullin exerts acute effects on vascular beds <strong>[Figure 5]</strong>.<sup>2</sup> In addition to vasodilation, animal studies have demonstrated that the activities of adrenomedullin include natriuretic effects mediated by a decrease in distal tubular sodium reabsorption and diuretic impacts on the rate of glomerular filtration.<sup>2</sup></p>

						<div class="figure">
							<h4>Figure 5. Cardiorenal actions of adrenomedullin</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-5.png') }}" width="594" height="470" alt="figure-5">

							<h5>ACTH, adrenocorticotropic hormone; ANP, atrial natriuretic peptide; AVP, arginine vasopressin; ET-I, endothelin-1; VSMC, vascular smooth muscle cell.</h5>

							<h5>Jougasaki M, Burnett JC. Adrenomedullin: potential in physiology and pathophysiology. <em>Life Sci</em>. 2000;66(10):855-872. </h5>
						</div>

						<p>There have been some mixed findings with regard to inotropic effects. Studies in rabbits with isolated myocytes have revealed a negative inotropic activity for adrenomedullin,<sup>8</sup> while another in patients with myocardial infarction indicated that adrenomedullin increased myocardial contractility and decreased myocardial relaxation, all without increasing oxygen consumption.<sup>9</sup> The reason for the discrepancies among studies with respect to inotropic potential is yet to be determined.<sup>10</sup> Adrenomedullin also has some inhibitory action on the secretion of aldosterone in rats, thereby potentially opposing an important component of the renin-angiotensin-aldosterone system <strong>[Figure 6]</strong>.<sup>11</sup></p> 

						<div class="figure">
							<h4>Figure 6. Adrenomedullin inhibited the increase in aldosterone secretion that occurs in rats fed a low sodium diet</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-6.png') }}" width="594" height="408" alt="figure-6">

							<h5>NS, not specified.</h5>

							<h5><sup>a</sup>P&lt;0.01 compared with Normal.</h5>

							<h5>Yamaguchi T, Baba K, Doi Y, Yano K, Kitamura K, Eto T. Inhibition of aldosterone production by adrenomedullin, a hypotensive peptide, in the rat. <em>Hypertension</em>. 1996;28(2):308–314.</h5>
						</div>

						<div class="figure inline-figure-right" style="width: 424px;">
							<h4>Figure 7. Adrenomedullin administration reduced myocardial fibrosis in rats</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-7.png') }}" width="386" height="355" alt="figure-7">

							<h5>Okumura H, Nagaya N, Kangawa K. Adrenomedullin infusion during ischemia/reperfusion attenuates left ventricular remodeling and myocardial fibrosis in rats. <em>Hypertens Res</em>. 2003;26 Suppl:S99–S104.</h5>
						</div>

						<p>Adrenomedullin has been associated with other cardioprotective effects. Adrenomedullin, by both direct administration and gene delivery, has been associated with attenuation of myocardial infarction after ischemic injury in rats.<sup>12,13</sup> There are some data to indicate that adrenomedullin exerts this protection from ischemic injury via antiapoptotic mechanisms through Akt activation.<sup>12,13</sup> Adrenomedullin has also been indicated to protect against the oxidative stress that can occur in ischemia/reperfusion injuries by suppressing the increase of reactive oxidative species.<sup>14</sup> Additionally, antifibrotic activities were detected in hypertensive rats treated with adrenomedullin <strong>[Figure 7]</strong>,<sup>13</sup> and adrenomedullin has been shown to inhibit collagen synthesis in cardiac fibroblasts.<sup>10</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>3. Adrenomedullin as a Therapeutic Target</h2>

						<div class="figure inline-figure-right" style="width: 296px;">
							<h4>Figure 8. IV adrenomedullin reduces MAP in humans</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-8.png') }}" width="258" height="258" alt="figure-8">

							<h5>MAP, mean arterial pressure.</h5>

							<h5>Lainchbury JG, Nicholls MG, Espiner EA, Yandle TG, Lewis LK, Richards AM. Bioactivity and interactions of adrenomedullin and brain natriuretic peptide in patients with heart failure. <em>Hypertension</em>. 1999;34(1):70-75.</h5>
						</div>

						<p>The potential therapeutic benefits of adrenomedullin have largely been investigated in preclinical settings, although there are some clinical studies in HF. In a study of eight patients with HF, infusions of pathophysiological levels of adrenomedullin were compared against infusions of placebo in the same patients. Adrenomedullin induced dose-dependent reductions in arterial blood pressure, with mean pressure declining by 25% <strong>[Figure 8]</strong>, although cardiac output did not change. Heart rate increased in the adrenomedullin-treated patients, while plasma aldosterone remained lower in these patients than in time-matched measurements with placebo.<sup>15</sup></p>

						<p>In a placebo-controlled clinical study to investigate the hemodynamic, renal, and hormonal effects of short-term intravenous infusion of adrenomedullin in seven patients with congestive HF, adrenomedullin was associated with the following: significant decrease in mean arterial pressure, increased cardiac index, significant decrease in mean pulmonary arterial pressure, significant increase in urine volume and sodium excretion, and significant reductions in plasma aldosterone compared to baseline <strong>[Figure 9]</strong>.<sup>16</sup></p>

						<div class="figure">
							<h4>Figure 9. Effects of IV adrenomedullin infusion</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-9.png') }}" width="594" height="526" alt="figure-9">

							<h5>Nagaya N, Satoh T, Nishikimi T, et al. Hemodynamic, renal, and hormonal effects of adrenomedullin infusion in patients with congestive heart failure. <em>Circulation</em>. 2000;101(5):498–503.</h5>
						</div>
						 
						<p>The gene delivery <strong>[Figure 10]</strong> of adrenomedullin via adenovirus to hypertensive rats resulted in prolonged reduction of blood pressure, enhanced renal function, and reduced interstitial fibrosis within the heart.<sup>17</sup> In a similar study of adenovirus gene delivery to rats, adrenomedullin treatment reduced the ratio of infarct size to ischemic area and reduced instances of sustained ventricular fibrillation compared with controls in an ischemia/reperfusion (I/R) injury model. Protection against cardiac remodeling, myocardial infarction, arrhythmia, and apoptosis in I/R injury was observed, thus suggesting that adrenomedullin exhibited cardioprotective effects.<sup>13</sup></p>

						<div class="figure">
							<h4>Figure 10. Gene therapy using an adenovirus vector</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-10.png') }}" width="594" height="477" alt="figure-10">
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>4. Adrenomedullin as a Biomarker</h2>

						<p>High levels of circulating and ventricular adrenomedullin have been described in chronic HF, with concentration levels that parallel increased severity of disease, thus creating potential for the use of adrenomedullin as a biomarker for HF.<sup>18-20</sup> Adrenomedullin is not stable in vitro; for this reason, immunoassays have been developed to detect the more stable mid-region of the adrenomedullin prohormone (MR-proADM), which correspond to levels of adrenomedullin in a one to one ratio.<sup>3</sup> Increased levels of MR-proADM in patients with HF are associated with increased risk of mortality and morbidity <strong>[Figure 11]</strong>, and these increases are independent of the behavior of natriuretic peptides.<sup>19</sup></p>

						<div class="figure">
							<h4>Figure 11. Higher quartile levels of MR-proADM were associated with higher median rates of mortality compared to lower quartile levels</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-11.png') }}" width="594" height="551" alt="figure-11">

							<h5>AUC, area under curve; CT-proAVP, C-terminal pro-arginine vasopressin.</h5>

							<h5>Peacock WF. Novel biomarkers in acute heart failure: MR-pro-adrenomedullin. <em>Clin Chem Lab Med</em>. 2014;52(10):1433-1435.</h5>
						</div>

						<div class="figure inline-figure-right" style="width: 375px;">
							<h4>Figure 12. Superiority of MR-proADM over BNP and troponin biomarker assays</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-12.png') }}" width="337" height="228" alt="figure-12">

							<h5>Maisel A, Mueller C, Nowak RM, et al. Midregion prohormone adrenomedullin and prognosis in patients presenting with acute dyspnea: results from the BACH (Biomarkers in Acute Heart Failure) trial. <em>J Am Coll Cardiol</em>. 2011;58(10):1057–1067.</h5>
						</div>

						<p>The prognostic potential of the MR-proADM immunoassays to predict <nobr>HF-related </nobr>death was demonstrated in the Biomarkers in Acute Heart Failure (BACH) trial. <nobr>MR-proADM</nobr> outperforms all established biomarkers in identifying patients at the highest risk of death at <nobr>90-days</nobr> <strong>[Figure 12]</strong>. Importantly, in addition to the assay’s ability to identify patients at highest risk of death within 90 days, MR-proADM was demonstrated to be superior to assays for both brain natriuretic peptide (BNP) and N-terminal pro–BNP in predicting mortality within 30 days in this trial of 568 patients with acute HF.<sup>20</sup></p>

						<p>The utility of a biomarker to predict outcomes in heart failure is  typically assessed based on correlation with 30- and 90-day mortality end points. However, clinical decisions made in the emergency department setting could benefit greatly from identifying patients at high short-term mortality risk, in particular, in order to deliver appropriate treatment. As MR-proADM may be one of the best biomarkers to identify patients at short-term risk of HF-related mortality <strong>[Figure 13]</strong>, it could potentially detect patients in need of more aggressive therapy.<sup>3</sup></p>

						<div class="figure">
							<h4>Figure 13. Comparative values of biomarker performance for mortality prediction in acute HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_04/img/figure-13.png') }}" width="594" height="551" alt="figure-13">

							<h5>Peacock WF. Novel biomarkers in acute heart failure: MR-pro-adrenomedullin. <em>Clin Chem Lab Med</em>. 2014;52(10):1433-1435.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>5. Summary</h2>

						<p>Adrenomedullin possesses hypotensive activities comprising vasodilatory, natriuretic, diuretic, antifibrotic, and possible inotropic effects. This hemodynamically active, vasodilatory, endogenous compensatory peptide exerts many effects on the cardiovascular system, particularly in times of crisis, such as HF. Furthermore, adrenomedullin has been associated with cardioprotective effects, such as inhibition of hypertrophy following cardiac damage and prevention of ischemic injury and hypoxia-induced cell death. Preclinical and clinical studies examining adrenomedullin delivery either as intravenous infusion or gene delivery have reinforced its cardioprotective roles. In a major clinical trial, the peptide adrenomedullin has demonstrated superior prognostic value in predicting even short-term mortality risk in HF compared with BNP. Adrenomedullin may have a unique ability as a biomarker to identify patients at short-term risk of HF-related mortality and could potentially detect patients in need of more aggressive therapy.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Hasenfuss G, Mann DL. Pathophysiology of heart failure. Chap 22 in: Mann DL, Zipes DP, Libby P, et al. eds, <em>Braunwald’s Heart Disease: A Textbook of Cardiovascular Medicine</em>, 10th ed. Elsevier Saunders; 2015.</li>
							<li>Jougasaki M, Burnett JC. Adrenomedullin: potential in physiology and pathophysiology. <em>Life Sci</em>. 2000;66(10):855-872.</li> 
							<li>Peacock WF. Novel biomarkers in acute heart failure: MR-pro-adrenomedullin. <em>Clin Chem Lab Med</em>. 2014;52(10):1433-1435.</li> 
							<li>Kitamura K, Kangawa K, Kawamoto M, et al. Adrenomedullin: a novel hypotensive peptide isolated from human pheochromocytoma. <em>Biochem Biophys Res Commun</em>. 1993;192:553-560.</li>
							<li>Aiyar N, Disa J, Pullen M, Nambi P. Receptor activity modifying proteins interaction with human and porcine calcitonin receptor-like receptor (CRLR) in HEK-293 cells. <em>Mol Cell Biochem</em>. 2001;224(1-2):123-133.</li>
							<li>Gibbons C, Dackor R, Dunworth W, et al. Receptor activity-modifying proteins: RAMPing up adrenomedullin signaling. <em>Mol Endocrinol</em>. 2007;21(4):783-796.</li>
							<li>McLatchie LM, Fraser NJ, Main MJ, et al. RAMPs regulate the transport and ligand specificity of the calcitonin-receptor-like receptor. <em>Nature</em>. 1998;393(6683):333-339.</li>
							<li>Fontes-Souza AP, Pires AL, Carneiro CS, Brás-Silva C, Leite-Moreira AF. Effects of adrenomedullin on systolic and diastolic myocardial function. <em>Peptides</em>. 2009;30(4):796-802.</li> 
							<Li>Nagaya N, Goto Y, Satoh T, Sumida H, Kojima S, Miyatake K, Kangawa K. Intravenous adrenomedullin in myocardial function and energy metabolism in patients after myocardial infarction. <em>J Cardiovasc Pharmacol</em>. 2002;39(5):754-760.</Li>
							<li>Nishikimi T, Yoshihara F, Horinaka S, et al. Chronic administration of adrenomedullin attenuates transition from left ventricular hypertrophy to heart failure in rats. <em>Hypertension</em>. 2003;42(5):1034-1041.</li> 
							<li>Yamaguchi T, Baba K, Doi Y, Yano K, Kitamura K, Eto T. Inhibition of aldosterone production by adrenomedullin, a hypotensive peptide, in the rat. <em>Hypertension</em>. 1996;28(2):308-314.</li>
							<li>Okumura H, Nagaya N, Kangawa K. Adrenomedullin infusion during ischemia/reperfusion attenuates left ventricular remodeling and myocardial fibrosis in rats. <em>Hypertens Res</em>. 2003;26 Suppl:S99-S104.</li>
							<li>Kato K, Yin H, Agata J, Yoshida H, Chao L, Chao J. Adrenomedullin gene delivery attenuates myocardial infarction and apoptosis after ischemia and reperfusion. <em>Am J Physiol Heart Circ Physiol</em>. 2003;285(4):H1506-H1514.</li>
							<Li>Kim SM, Kim JY, Lee S, Park JH. Adrenomedullin protects against hypoxia/reoxygenation-induced cell death by suppression of reactive oxygen species via thiol redox systems. <em>FEBS Lett</em>. 2010;584(1):213-218.</Li> 
							<li>Lainchbury JG, Nicholls MG, Espiner EA, Yandle TG, Lewis LK, Richards AM. Bioactivity and interactions of adrenomedullin and brain natriuretic peptide in patients with heart failure. <em>Hypertension</em>. 1999;34(1):70-75.</li>
							<li>Nagaya N, Satoh T, Nishikimi T, et al. Hemodynamic, renal, and hormonal effects of adrenomedullin infusion in patients with congestive heart failure. <em>Circulation</em>. 2000;101(5):498-503.</li>
							<li>Dobrzynski E, Wang C, Chao J, Chao L. Adrenomedullin gene delivery attenuates hypertension, cardiac remodeling, and renal injury in deoxycorticosterone acetate-salt hypertensive rats. <em>Hypertension</em>. 2000;36(6):995-1001.</li>
							<li>Jougasaki M, Wei CM, McKinley LJ, Burnett JC. Elevation of circulating and ventricular adrenomedullin in human congestive heart failure. <em>Circulation</em>. 1995;92(3):286–289.</li>
							<li>Potocki M, Ziller R, Mueller C. Mid-regional pro-adrenomedullin in acute heart failure: a better biomarker or just another biomarker? <em>Curr Heart Fail Rep</em>. 2012;9(3):244-251.</li>
							<li>Maisel A, Mueller C, Nowak RM, et al. Midregion prohormone adrenomedullin and prognosis in patients presenting with acute dyspnea: results from the BACH (Biomarkers in Acute Heart Failure) trial. <em>J Am Coll Cardiol</em>. 2011;58(10):1057-1067.</li>
						</ol>

					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->