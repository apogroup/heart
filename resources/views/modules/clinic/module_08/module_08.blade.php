<div class="module-wrapper">

	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 8: The Impact of Heart Failure on Our Patients’ Everyday Lives</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1.	HF Treatment Goals: Stabilizing or Improving Patients&rsquo; HRQOL</a></li>
				<li class="chapter-btn chapter-2"><a>2.	Methods to Measure Risk Factors and HRQOL in Practice</a></li>
				<li class="chapter-btn chapter-3"><a>3.	Association of HRQOL With Patient Outcomes</a></li>
				<li class="chapter-btn chapter-4"><a>4.	Worsening HRQOL and HF Hospitalizations</a></li>
				<li class="chapter-btn chapter-5"><a>5.	Communicating With Patients and Establishing Goals</a></li>
				<li class="chapter-btn chapter-6" metric="completion"><a>6. Summary</a></li>
				<li class="chapter-btn chapter-7"><a>References</a></li>
				<li class="chapter-btn chapter-8"><a>Keywords</a></li>
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-8-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>Module 8: The Impact of Heart Failure on Our Patients’ Everyday Lives</h1>

						<h2>Module Description</h2>

						<p>Learn how heart failure (HF) impacts patients&rsquo; health-related quality of life (HRQOL) and how physicians can track and utilize HRQOL to guide patient management decisions.</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Understand the burden of HF on patients&rsquo; HRQOL and describe tools to measure and track HRQOL </p></li>
							<li><p>Illustrate the relationship between HRQOL and long-term outcomes in patients with HF</p></li>
							<li><p>Advise on strategies to improve patient communication, treatment goal setting, and expectations</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p style="width: 605px;">Patients with HF may experience troubling symptoms, including breathlessness, orthopnea, paroxysmal nocturnal dyspnea, reduced exercise tolerance, fatigue, and ankle swelling.<sup>1</sup> The burden that symptoms have on patients with HF is on par with the symptom burden of cancer.<sup>2</sup> Two of the most commonly used scales in HF, New York Heart Association (NYHA) functional class and American College of Cardiology Foundation/American Heart Association guideline stages, classify the stage and severity of HF largely based on symptoms.<sup>3</sup> Despite the burden of symptoms and known association between worsened symptoms and advanced disease, HRQOL may not be considered as important as other variables when making treatment decisions. However, evidence suggests that some measures of HRQOL may be as reliable as NYHA functional class for managing patients with HF.<sup>4</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						
						<h2>1. HF Treatment Goals: Stabilizing or Improving Patients&rsquo; HRQOL</h2>

						<p>The primary goals in the management of patients with HF are to prevent disease progression and improve patients&rsquo; health status. Or stated more simply, treatment goals are to help patients live longer and feel better <strong>(Figure 1)</strong>.<sup>1,3</sup> There are multiple measures that contribute to achieving these goals. For example, preventing disease progression involves controlling and managing arrhythmia and preventing morbidity and mortality.<sup>1,3</sup> Some components involved in managing patients&rsquo; health status include controlling and improving symptoms, improving HRQOL, and decreasing physical limitations.<sup>1,3</sup> A change in any of these measures should signal a need to uptitrate existing drugs or to introduce new treatments. Additionally, treatment guidelines list comorbidities, worsening structural heart disease, and worsened symptoms as partial measures that can indicate treatment choices and more aggressive regimens.<sup>3</sup></p>

						<div class="figure">
							<h4>Figure 1: Treatment Goals in HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-1.png') }}" width="527" height="252" alt="figure-1">

							<h5>LVAD, left ventricular assist device.</h5>
							<h5>1. Yancy CW et al. <em>Circulation</em>. 2013;128:1810-1852. 2. McMurray JJV et al. <em>Eur Heart J</em>. 2012;33:1787-1847.</h5>
						</div>

						<p>However, worsened symptoms may not be perceived to cause the same urgency for new treatment approaches as some other measures. Furthermore, a challenge with utilizing symptoms as a measure to suggest patient treatment is that patients may not report or may misunderstand the urgency of symptoms.<sup>5,6</sup> Patients may not recognize escalating symptoms or think there is a need to seek help for worsened symptoms.<sup>5</sup> Patients may wait many days to seek medical care after the onset of HF symptoms, and this can result in unnecessary hospital admissions.<sup>6</sup> To combat this limitation, there are risk assessment tools and questionnaires that can be utilized to quantify patient risk and track HRQOL to aid in symptom monitoring and treatment decisions.</p>
					
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>2. Methods to Measure Risk Factors and<br>HRQOL in Practice</h2>

						<p>Risk assessment tools such as the Seattle Heart Failure Model (SHFM) and the Meta-Analysis Global Group in Chronic Heart Failure (MAGGIC) risk model attempt to categorize patients by risk for disease progression.<sup>7,8</sup> Both of these risk assessment tools incorporate multiple variables that are known to correlate with risk for morbidity and mortality in HF, including age, sex, body mass index, smoking status, NYHA class, etc., into their model.<sup>7,8</sup> These risk assessment tools were designed to identify patients who are at risk for worsening HF or death, such that higher scores, indicating greater risk, correlate with a higher likelihood of death or HF events.<sup>7,9</sup> However, it is important to note that even the &ldquo;lowest-risk&rdquo; patients are still at risk for events. This is illustrated by the allotment of patients from a large HF study by MAGGIC risk score. In this analysis, the lowest-risk patients were still at risk for events.<sup>9,10</sup> The increased rate of events as risk scores increase highlights the utility of these risk measures.</p>

<!-- 						<div class="module-question module-visible" module-number="8" module-question="2">
							<h4>Please answer the following question to reveal <strong>Figure 2</strong>.</h4>

							<h4><strong>Question:</strong> What proportion of patients in the lowest MAGGIC risk score group (scores 4-15) do you think experienced all-cause mortality in the PARADIGM-HF trial after a median of 27 months of follow-up?</h4>

							<div class="module-choices">
								<div class="module-choice">
									<input type="radio" name="module-08-2" value="0-2.5%">
									<label for="0-2.5%">0-2.5%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-2" value="2.5-5.0%">
									<label for="2.5-5.0%">2.5-5.0%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-2" value="5.0-7.5%">
									<label for="5.0-7.5%">5.0-7.5%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-2" value="7.5-10.0%">
									<label for="7.5-10.0%">7.5-10.0%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-2" value="10-12.5%">
									<label for="10-12.5%">10-12.5%</label>
								</div>
							</div>

						</div>

						<div class="module-answer module-hidden" module-number="8" module-question="2">
							<h4>Thank you for responding. 11% of patients in the lowest MAGGIC risk score group experienced all-cause death. Please see <strong>Figure 2</strong> for full details.</h4>

							<h4>Figure 2. Clinical Outcomes According to MAGGIC Risk Score Category in PARADIGM-HF after 27-Month Follow-up<sup>1,2</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-2.png') }}" width="540" height="362" alt="figure-2">

							<h5>1. Simpson J et al. <em>J Am Coll Cardiol</em>. 2015;66:2059-2071. 2. McMurray JJV et al. <em>N Engl J Med</em>. 2014;371:993-1004.</h5>
						</div> -->

						<p>Another useful tool in clinical practice is a health questionnaire, which aims to evaluate patients&rsquo; HRQOL. There are several available questionnaires, including the Kansas City Cardiomyopathy Questionnaire (KCCQ) and the Minnesota Living with Heart Failure Questionnaire (MLHFQ).<sup>11</sup> These tools present questions to patients about their health and well-being to understand how HF is affecting their HRQOL. Questionnaires correlate well with NYHA functional class.<sup>4,12</sup></p>


					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>3. Association of HRQOL With Patient Outcomes</h2>

						<p>Patients&rsquo; quality of life can be ascertained using patient-reported outcome measures such as the KCCQ and MLHFQ.<sup>11</sup> These HRQOL assessment tools utilize different scoring systems and questions, but they all allow understanding of how HF is affecting patients&rsquo; HRQOL and well-being. These measures also correlate with patient outcomes.</p>

						<div class="module-question module-visible" module-number="8" module-question="3">
							
							<h4>Please answer the following question to reveal <strong>Figure 2</strong>.</h4>

							<h4><strong>Question:</strong> What do you think the approximate difference in 1-year HF hospitalization rates was for patients with the worst HRQOL based on KCCQ summary score <br>(score&lt;25) vs best HRQOL based on KCCQ (&ge;75)?</h4>

							<div class="module-choices">
								<div class="module-choice">
									<input type="radio" name="module-08-3" value="10%">
									<label for="10%">10%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-3" value="15%">
									<label for="15%">15%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-3" value="20%">
									<label for="20%">20%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-3" value="25%">
									<label for="25%">25%</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-3" value="30%">
									<label for="30%">30%</label>
								</div>
							</div>

						</div>

						<div class="module-answer module-hidden" module-number="8" module-question="3">

							<h4>Thank you for responding. The difference in 1-year HF hospitalization rates was approximately 30% for the lowest vs highest HRQOL KCCQ summary scores. Please see <strong>Figure 2</strong> for full details.</h4>

							<h4>Figure 2: One-Year HF Hospitalization Rates Based on KCCQ Summary Score (N=505)</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-2.png') }}" width="540" height="362" alt="figure-2">

							<h5>Heidenreich PA et al. <em>J Am Coll Cardiol</em>. 2006;47:752-756.</h5>

						</div>

						<p>The KCCQ is a 23-item, self-administered, sensitive, specific, and responsive measure for patients with chronic heart failure. The KCCQ is scored from 0 to 100, where a higher score indicates better HRQOL.<sup>13</sup> Studies may report KCCQ clinical summary score or overall summary score, which are based on a composite of some of the key elements of the test. KCCQ summary scores can correlate with patient outcomes, including HF hospitalization. A study of 505 outpatients with HF with LVEF &lt;40% from 13 North American centers who completed 1 year of follow-up showed that patients&rsquo; HRQOL based on KCCQ summary score significantly correlated with 1-year hospitalization rates  when patients were grouped into quartiles by KCCQ score <strong>(Figure 2)</strong>.<sup>14</sup> Additionally, this study also showed that patients grouped into quartiles by KCCQ summary score demonstrated significant correlation with all-cause mortality through 1 year <strong>(Figure 3A)</strong>.<sup>14</sup></p>

						<p>Correlations between HRQOL and outcomes were also demonstrated for the MLHFQ, a self-administered, disease-specific questionnaire for patients with HF that includes 21 items. The test is scored from 0 to 105, where high scores indicate more symptoms.<sup>15</sup> A study of 114 Japanese patients followed for a median of 2.1 years who made scheduled visits to a cardiovascular (CV) outpatient clinic demonstrated that MLHFQ score significantly correlated with event-free survival for up to 900 days <strong>(Figure 3B)</strong>. Patients were grouped into 2 categories by MLHFQ score (MLHFQ score &lt;26 and &ge;26).<sup>16</sup> Therefore, HRQOL questionnaires not only provide information on a patients&rsquo; well-being, but also questionnaire scores significantly correlate with patient outcomes.</p>

						<div class="figure">
							<h4>Figure 3. Correlation Between HRQOL Based on KCCQ Summary Score and All-cause Mortality<sup>1</sup> (A) and MLHFQ Score and Event-free Survival<sup>2</sup> (B)</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-3.png') }}" width="353" height="581" alt="figure-3">

							<h5>1. Heidenreich PA et al. <em>J Am Coll Cardiol</em>. 2006;47:752-756. 2. Kato N et al. <em>Circ J</em>. 2011;75:1661-1669.</h5>

						</div>
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>4. Worsening HRQOL and HF Hospitalizations</h2>

						<p>Worse HRQOL at baseline, assessed by questionnaires, correlated with patient outcomes, and worsening HRQOL over time was also associated with patient outcomes. The KCCQ was used in the Eplerenone&rsquo;s Neurohormonal Efficacy and Survival Study (EPHESUS), a multicenter, randomized clinical trial of 6632 patients with acute myocardial infarction, LVEF &le;40%, and postinfarction HF or diabetes who were treated with eplerenone or placebo.<sup>17</sup> Changes in KCCQ scores at 1 and 3 months significantly correlated with all-cause mortality and CV mortality or rehospitalization <strong>(Figure 4)</strong>.<sup>17</sup> Patients with improvement in symptoms based on a &ge;10-point increase in KCCQ scores were less likely to experience CV death or rehospitalization than patients who had KCCQ scores remaining within 10 points of baseline, and patients with worsened HRQOL based on a &le;10-point decrease in KCCQ scores were more likely to have these events.<sup>17</sup> Similar results were observed using the MLHFQ in an analysis of 433 patients with advanced HF enrolled in the Evaluation Study of Congestive Heart Failure and Pulmonary Artery Catheterization Effectiveness (ESCAPE) trial.<sup>18</sup> In this study, &ge;5-point improvements in MLHFQ score were predictive of lower rates of rehospitalization or death. The odds ratio for poorer event-free survival was 3.3 in patients whose MLHFQ scores worsened compared with those whose scores improved (<em>P=</em>0.009).<sup>18</sup> Therefore, changes in HRQOL may help predict risk for hospitalization and mortality.</p>

						<div class="figure">
							<h4>Figure 4. KCCQ-OS Changes Correlate with All-cause Mortality (A) and CV Mortality or Rehospitalization (B)</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-4a.png') }}" width="452" height="420" alt="figure-4a">

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-4b.png') }}" width="460" height="420" alt="figure-4b">

							<h5>KCCQ-OS, KCCQ overall summary score.</h5>
							<h5>Kosiborod M et al. <em>Circulation</em>. 2007;115:1975-1981.</h5>
						</div>

						<p>Assessing risk for worsening of HF is important because this worsening, and HF hospitalization (HFH) in particular, causes an elevated risk for morbidity and mortality. The Digitalis Investigation Group trial included 7888 patients from 302 centers with chronic HF, with most patients (86%) having LVEF &le;45%.<sup>19</sup> An analysis of 1057 pairs of propensity-matched patients with and without HFH during the first 2 years of the trial demonstrated a significantly elevated risk of CV mortality for patients with HFH compared with patients with no HFH <strong>(Figure 5)</strong>.<sup>19</sup></p>

						<div class="module-question module-visible" module-number="8" module-question="4">

							<h4>Please answer the following question to reveal <strong>Figure 5.</strong></h4>
						
							<h4><strong>Question: </strong>What do you think the approximate increased risk is for CV mortality based on hazard ratio (HR) for patients with an HFH compared with those with no HFH?</h4>

							<div class="module-choices">
								<div class="module-choice">
									<input type="radio" name="module-08-4" value="1.5-fold increased risk">
									<label for="1.5-fold increased risk">1.5-fold increased risk</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-4" value="2-fold increased risk">
									<label for="2-fold increased risk">2-fold increased risk</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-4" value="2.5-fold increased risk">
									<label for="2.5-fold increased risk">2.5-fold increased risk</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-4" value="3-fold increased risk">
									<label for="3-fold increased risk">3-fold increased risk</label>
								</div>
								<div class="module-choice">
									<input type="radio" name="module-08-4" value="3.5-fold increased risk">
									<label for="3.5-fold increased risk">3.5-fold increased risk</label>
								</div>
							</div>

						</div>

						<div class="module-answer module-hidden" module-number="8" module-question="4">
						
							<h4>Thank you for responding. The HR was 2.88 based on these groupings, suggesting a nearly 3-fold increased risk of CV mortality for patients with a HFH compared with patients with no HFH. Please see <strong>Figure 5</strong> for full details.</h4>

							<h4>Figure 5. CV Mortality Based on Prior HFH</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-5.png') }}" width="450" height="381" alt="figure-5">

							<h5>Ahmed A et al. <em>J Card Fail</em>. 2008;14:211-218.</h5>							

						</div>

						<p>Following an HFH, HRQOL measures may also be an important method of tracking patients&rsquo; progress. A post hoc analysis of a large HF study showed that lower HRQOL based on KCCQ scores were significantly associated with more recent hospitalization  (<strong>Table 1</strong>).<sup>20</sup> Patients&rsquo; HRQOL scores based on questionnaires may be a useful measure to track patients&rsquo; improvement following a hospitalization.</p>

						<div class="figure">

							<h4>Table 1. Mean Changes in KCCQ Scores from Baseline to 8 Months by <br>Time Since HF Hospitalization</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/table-1_NEW.png') }}" width="551" height="284" alt="table-1">							

							<h5>Lewis E et al. Presented at: Heart Failure Society of America 20th Annual Scientific Meeting; September 17-20, 2016; Kissimmee, FL.</h5>

						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>5. Communicating With Patients and <br>Establishing Goals</h2>

						<p>There are some ways that HRQOL questionnaires can be more readily utilized in clinical practice. Physicians should aim to make the HRQOL tests actionable, efficient, interpretable, obligatory, and user-friendly.<sup>21</sup> By making the test obligatory, the physician can track a patient&rsquo;s HRQOL over time, and by making it user-friendly, the patient can easily and quickly complete the test. To achieve an actionable, efficient, and interpretable result, physicians can develop a scale and tracking system to translate the meaning of the scores (<strong>Figure 6</strong>).<sup>21</sup> A way to quickly apply the meaning of HRQOL measures into more meaningful results such as NYHA class or risk for 1-year death or hospitalization may aid in interpreting the results and helping to make decisions based on the test.</p>

						<div class="figure">

							<h4>Figure 6. Sample Scale to Improve Utility of KCCQ or Other Questionnaires</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-6.png') }}" width="514" height="218" alt="figure-6">

							<h5>Data based on an editorial.</h5>

							<h5>Spertus J. <em>Circ Cardiovasc Qual Outcomes</em>. 2014;7:2-4.</h5>

						</div>

						<p>In addition to HRQOL questionnaires or risk assessment tools, physicians must establish an open dialogue with patients because it is important to recognize that patients may misinterpret symptoms as being due to advanced age rather than worsening HF and may feel unable to ask their physicians questions.<sup>22</sup> By establishing open communication, these barriers may be overcome; however, it is important to understand the patient’s goals, expectations, and priorities. Different patients may be more concerned with different aspects of treatment or disease course (<strong>Figure 7</strong>).<sup>23</sup> By determining what is important to patients and setting realistic goals, physicians may be able to individualize treatment courses and patient expectations.<sup>23</sup></p>

						<div class="figure">
							<h4 style="font-size: 15.7px;">Figure 7. Most Important Factors of Treatment for Individual Patients With HF Can&nbsp;Vary</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_08/img/figure-7.png') }}" width="510" height="498" alt="figure-7">

							<h5>Allen LA et al. <em>Circulation</em>. 2012;125:1928-1952.</h5>
						</div>



					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>6. Summary</h2>

						<p>HF is associated with significant disease burden based on the risk for morbidity and mortality. Also, HF can severely impact patients&rsquo; HRQOL. Monitoring of HRQOL is important to improve patients&rsquo; well-being, because HRQOL is significantly associated with long-term outcomes. Higher scores in risk assessment models such as MAGGIC and SHFM or worsening HRQOL based on questionnaires are both associated with elevated risk for morbidity and mortality. Implementing questionnaires and risk models into routine clinical practice may help to identify and proactively address worsening HF. Additionally, establishing an open dialogue, understanding what is important to patients, and setting treatment goals with patients is of paramount importance to improve patients&rsquo; understanding of their disease and to set appropriate expectations.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>McMurray JJV, Adamopoulos S, Anker SD, et al. <em>Eur Heart J</em>. 2012;33:1787-1847.</li>
							<li>Xu J, Nolan MT, Heinze K, et al. <em>Appl Nurs Res</em>. 2015;28:311-315.</li>
							<li>Yancy CW, Jessup M, Bozkurt B, et al. <em>Circulation</em>. 2013;128:1810-1852.</li>
							<li>Spertus JA, Jones PG, Kim J, Globe D. <em>Qual Life Res</em>. 2008;17:291-298.</li>
							<li>Horowitz CR, Rein SB, Leventhal H. <em>Soc Sci Med</em>. 2015;58:631-643.</li>
							<li>Gravely-Witte S, Jurgens CY, Tamim H, Grace SL. <em>Eur J Heart Fail</em>. 2010;12:1122-1129.</li>
							<li>Levy WC, Mozaffarian D, Linker DT, et al. <em>Circulation</em>. 2006;113:1424-1433.</li>
							<li>Pocock SJ, Ariti CA, McMurray JJV, et al; on behalf of the Meta-Analysis Global Group in Chronic Heart Failure. <em>Eur Heart J</em>. 2013;34:1404-1413.</li>
							<li>Simpson J, Jhund PS, Cardoso JS, et al. <em>J Am Acad Cardiol</em>. 2015;66:2059-2071.</li>
							<li>McMurray JJV, Packer M, Desai AS, et al. <em>N Engl J Med</em>. 2014;371:993-1004.</li>
							<li>Garin O, Herdman M, Vilagut G, et al. <em>Heart Fail Rev</em>. 2014;19:359-367.</li>
							<li>Riegel B, Moser DK, Glaser D, et al. <em>Nurs Res</em>. 2002;51:209-218.</li>
							<li>Green CP, Porter CB, Bresnahan DR, Spertus JA. <em>J Am Acad Cardiol</em>. 2000;35:1245-1255.</li>
							<li>Heidenreich PA, Spertus JA, Jones PG, et al; for the Cardiovascular Outcomes Research Consortium. <em>J Am Acad Cardiol</em>. 2006;47:752-756.</li>
							<li>Bilbao A, Escobar A, García-Perez L, Navarro G, Quirós R. <em>Health Qual Life Outcomes</em>. 2016;14:23.</li>
							<li>Kato N, Kinugawa K, Seki S, et al. <em>Circ J</em>. 2011;75:1661-1669.</li>
							<li>Kosiborod M, Soto GE, Jones PG, et al. <em>Circulation</em>. 2007;115:1975-1981.</li>
							<li>Moser DK, Yamokoski L, Sun JL, et al. <em>J Card Fail</em>. 2009;15:763-769.</li>
							<li>Ahmed A, Allman RM, Fonarow GC, et al. <em>J Card Fail</em>. 2008;14:211-218.</li>
							<li>Lewis EF, Claggett B, McMurray JJV, et al. Heart Failure Society of America 20th Annual Scientific Meeting; September 17-20, 2016; Kissimmee, FL.</li>
							<li>Spertus J. <em>Circ Cardiovasc Qual Outcomes</em>. 2014;7:2-4.</li>
							<li>Rogers AE, Addington-Hall JM, Abery AJ, et al. <em>BMJ</em>. 2000;321:605-607.</li>
							<li>Allen LA, Stevenson LW, Grady KL, et al. <em>Circulation</em>. 2012;125:1928-1952.</li>
						</ol>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						<h2>Keywords</h2>

						<ol>
							<li>Heart failure (HF)</li>
							<li>Well-being</li>
							<li>Health-related quality of life (HRQOL)</li>
							<li>Kansas City Cardiomyopathy Questionnaire (KCCQ)</li>
							<li>Minnesota Living with Heart Failure Questionnaire (MLHF)</li>
							<li>Heart failure hospitalization</li>
							<li>Seattle Heart Failure Model (SHFM)</li>
							<li>Meta-Analysis Global Group in Chronic Heart Failure (MAGGIC)</li>
						</ol>
					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->