<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 5: The Other Side of the Renin-Angiotensin-Aldosterone System: Discovering Angiotensin (1–7)</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1. Synthesis and Localization of Ang-(1-7)</a></li>
				<li class="chapter-btn chapter-2"><a>2. The Role of Ang-(1-7) in the Protective Arm of the RAAS</a></li>
				<li class="chapter-btn chapter-3"><a>3. Stimulating the Protective Arm of the RAAS as a Therapeutic Strategy in HF</a></li>
				<li class="chapter-btn chapter-4" metric="completion"><a>4. Summary</a></li>
				<li class="chapter-btn chapter-5"><a>References</a></li>
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-5-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>The Other Side of the Renin-Angiotensin-Aldosterone System: Discovering Angiotensin (1–7)</h1>

						<h2>Module Description</h2>

						<p>Explore the &ldquo;protective&rdquo; side of the renin-angiotensin-aldosterone system and the emerging role of the novel endogenous compensatory peptide angiotensin (1-7) in treating heart failure</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Explore the key compensatory neurohormonal mechanisms underlying the pathophysiology of heart failure</p></li>
							<li><p>Describe the components of the protective arm of the renin-angiotensin-aldosterone system</p></li>
							<li><p>Characterize the effects of angiotensin (1-7) and its potential as a therapeutic target in heart failure</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p>In patients with chronic heart failure (HF), the decrease in cardiac output activates several compensatory neurohormonal mechanisms to help increase cardiac contractility and maintain circulatory homeostasis.<sup>1</sup> The sympathetic nervous system is activated early in the course of HF, whereas later stages of the disease are characterized by activation of the renin-angiotensin-aldosterone system (RAAS).<sup>1</sup> The RAAS plays an important role in regulating blood pressure, blood volume, and electrolytes, impacting  the heart, vasculature, and kidneys.<sup>2</sup> The main &ldquo;excitatory&rdquo; axis of the RAAS involves angiotensin I (Ang I), angiotensin-converting enzyme (ACE), and angiotensin II (Ang II), as well as the Ang II receptors AT1R and AT2R <strong>[Figure 1, left side]</strong>.<sup>3</sup> Ang I is converted by ACE into Ang II, a potent vasoconstrictor.<sup>2</sup> The actions of Ang II are mediated primarily via its binding to AT1R or AT2R.<sup>1</sup> Ang II binding to AT1R mediates vasoconstriction, cell proliferation, aldosterone secretion, and catecholamine release, whereas binding to AT2R counteracts AT1R-mediated effects.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 1: Updated Overview of the RAAS System</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-1.png') }}" width="594" height="418" alt="figure-1">
							<h5>ACE, angiotensin-converting enzyme; AMP, aminopeptidase;
							Ang, angiotensin; AT<sub>1</sub>, angiotensin type 1 receptor; AT<sub>2</sub>, angiotensin type 2 receptor; D-Amp, dipeptidyl aminopeptidase I–III; IRAP,
							insulin-regulated aminopeptidase; Mas, Mas receptor; MrgD, Mas-related G-protein–coupled receptor D; PCP, prolyl carboxypeptidase; and PEP, prolyl endopeptidase.</h5>
							<h5>Santos RA. Angiotensin-(1-7). <em>Hypertension.</em> 2014;63:1138-1147.</h5>
						</div>

						<p>While the main axis of the RAAS and the effects of Ang II are well characterized, there is growing evidence that other components of this neurohormonal system, including the heptapeptide Ang-(1-7), the ACE homolog ACE2, and the Mas receptor, form a secondary axis with effects opposite to that of the ACE/Ang II/AT1R arm <strong>[Figure 1, right side]</strong>.<sup>4</sup> This &ldquo;protective&rdquo; arm of the RAAS may serve as an important therapeutic target in HF as well as other cardiovascular diseases <strong>[Figure 2]</strong>.</p>
						
						<div class="figure">
							<h4>Figure 2: Opposing Effects of the RAAS System</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-2.png') }}" width="594" height="365" alt="figure-2">

							<h5>Santos RA. Angiotensin-(1-7). <em>Hypertension.</em> 2014;63:1138-1147.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						
						<h2>1. Synthesis and Localization of Ang-(1-7) </h2>

						<p>Ang-(1-7) is an endogenous compensatory peptide that counterbalances the effects of Ang II and may be generated in several ways <strong>[Figures 3 and 4]</strong>.<sup>2,4,5</sup> Removal of the phenylalanine at the carboxy-terminal of Ang II, a process catalyzed by the recently discovered ACE homolog ACE2, yields the heptapeptide Ang-(1-7).<sup>2</sup> Alternatively, Ang I may first be hydrolyzed by ACE2 to form Ang-(1-9), which in turn is converted by ACE to Ang-(1-7).<sup>2</sup> A third synthetic pathway is the direct conversion of Ang I to Ang-(1-7), which is catalyzed by the neutral endopeptidase neprilysin and other enzymes, including prolyl endopeptidase and thimet oligopeptidase.<sup>2</sup></p>

						<div class="figure">
							<h4>Figure 3: Ang-(1-7) is a 7 Amino Acid Peptide</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-3.png') }}" width="594" height="334" alt="figure-3">

							<h5>Passos-Silva DG, Verano-Braga T, Santos RAS. Angiotensin-(1-7): beyond the cardio-renal actions. <em>Clin Sci (Lond).</em> 2013;124:443–456.</h5>
						</div>

						<div class="figure">
							<h4>Figure 4: Pathways of Ang-(1-7) Formation</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-4.png') }}" width="594" height="415" alt="figure-4">

							<h5>Passos-Silva DG, Verano-Braga T, Santos RAS. Angiotensin-(1-7): beyond the cardio-renal actions. <em>Clin Sci (Lond).</em> 2013;124:443–456.</h5>
						</div>

						<p>Ang-(1-7) is present in the myocardium of various species, including the human heart and rat cardiac myocytes.<sup>6,7</sup> In humans, the formation of Ang-(1-7) is dependent on the availability of Ang II as a substrate, suggesting that the primary method of Ang-(1-7) synthesis may be the ACE2-mediated conversion of Ang II.<sup>6</sup> In rat experiments, the expression of Ang-(1-7) was found to be associated with changes in cardiac contractility.<sup>7</sup> After coronary artery ligation, Ang-(1-7) expression is increased in tissue surrounding the infarct area.<sup>7</sup> Ang-(1-7) is also formed in the human vascular endothelium.<sup>3</sup></p>

						<p>Ang-(1-7) exerts its effects through the G protein–coupled Mas receptor <strong>[Figure 5]</strong>.<sup>8</sup> The Mas receptor is distinct from the Ang II receptors, AT1R and AT2R, in that it does not mediate conventional G protein-coupled signaling pathways.<sup>9</sup> Rather, activation of the Mas receptor by Ang-(1-7) leads to prostaglandin and nitric oxide generation, as well as stimulation of the phosphoinositide 3-kinase (PI3K)/Akt and mitogen-activated protein kinase (MEK) 1/2 extracellular-signal-regulated kinase (ERK) 1/2 pathways.<sup>9</sup></p>

						<div class="figure">
							<h4>Figure 5: Interaction of Ang-(1-7) with the Mas G Protein-Coupled Receptor and Subsequent Actions</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-5.png') }}" width="594" height="334" alt="figure-5">

							<h5>Passos-Silva DG, Verano-Braga T, Santos RAS. Angiotensin-(1-7): beyond the cardio-renal actions. <em>Clin Sci (Lond).</em> 2013;124:443–456.</h5>
							<h5>Santos RAS, Simoes e Silva AC, Maric C, et al. Angiotensin-(1-7) is an endogenous ligand for the G protein-coupled receptor Mas. <em>Proc Natl Acad Sci U S A</em>. 2003;100:8258–8263.</h5>
							<h5>Gironacci MM. Angiotensin-(1-7): beyond its central effects on blood pressure. <em>Ther Adv Cardiovasc Dis.</em> 2015;9:209–216.</h5>
						</div>
					
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>2. The Role of Ang-(1-7) in the Protective Arm of the RAAS</h2>

						<p>In the early stages of HF, the RAAS is activated as a compensatory mechanism to help maintain cardiac output. In the short term, this activation modulates left ventricular (LV) function within a physiologic range and helps maintain circulatory homeostasis.<sup>1</sup> However, in the long term, the sustained expression of Ang II results in LV remodeling and fibrosis of the heart, kidneys, and other organs, in addition to increased activation of the sympathetic nervous system <strong>[Figure 6]</strong>.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 6: Role of Prolonged Ang II Activity in Progression of HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-6.png') }}" width="594" height="454" alt="figure-6">

							<h5>MMP, matrix metalloproteinase; VSMCs, vascular smooth muscle cells.</h5>
							<h5>Mehta PK, Griendling KK. Angiotensin II cell signaling: physiological and pathological effects in the cardiovascular system.  <em>Am J Physiol Cell Physiol.</em> 2007;292:C82-C97.</h5>
						</div>

						<p>As previously described, Ang II itself can undergo ACE2-catalyzed conversion to generate Ang-(1-7),<sup>2</sup> which has multiple protective cardiovascular actions that can counteract the deleterious effects of Ang II on endothelial function <strong>[Figure 7]</strong>.<sup>3</sup></p>

						<div class="figure">
							<h4>Figure 7: Cardiovascular Actions of Ang-(1-7)</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-7.png') }}" width="594" height="591" alt="figure-7">

							<h5>Santos RA. Angiotensin-(1-7). <em>Hypertension.</em> 2014;63:1138–1147.</h5>
						</div>

						<p>Ang-(1-7) has been shown to induce vasodilation of coronary, cerebral, renal, and other blood vessels in a dose-dependent manner.<sup>10</sup> This vasodilatory effect appears to be endothelium-dependent and mediated by nitric oxide as well as prostaglandin release.<sup>10</sup> </p>

						<p>The effects of Ang-(1-7) in the kidney are complex, with some animal studies suggesting a diuretic/natriuretic effect and others showing an antidiuretic/antinatriuretic effect.<sup>11</sup></p>

						<p>The cardioprotective effects of Ang-(1-7) have been demonstrated in several studies. In isolated rat hearts, Ang-(1-7) reduced the incidence and duration of ischemia/reperfusion-induced arrhythmias.<sup>12</sup> Lentivirus-mediated overexpression of Ang-(1-7) in rat hearts attenuated myocardial infarction-induced ventricular hypertrophy and LV wall thinning, preserved fractional shortening, and attenuated the increase in LV end-diastolic pressure, but did not alter cardiac function in rats without myocardial infarction.<sup>13</sup> In transgenic rats engineered for increased production of Ang-(1-7), isoproterenol-induced LV hypertrophy was significantly attenuated compared with control rats.<sup>14</sup> Ang-(1-7) demonstrated antifibrotic and antitrophic effects in adult rat cardiac fibroblasts, inhibiting collagen synthesis, induction of growth factors, and the profibrotic effects of Ang II.<sup>15</sup></p>

						<p>Ang-(1-7) has been found to inhibit vascular smooth muscle cell growth following vascular injury as well as the growth of neonatal cardiac myocytes in rats.<sup>16,17</sup> Ang-(1-7) also has demonstrated antiangiogenic effects in a mouse sponge model of angiogenesis.<sup>18</sup></p>

						<p>In summary, Ang-(1-7), as part of the ACE2/Ang-(1-7)/Mas axis, is able to oppose the often deleterious pressor, proliferative, profibrotic, and prothrombotic actions mediated by Ang II <strong>[Figure 8]</strong>.<sup>11</sup></p>

						<div class="figure">
							<h4>Figure 8: Ang-(1-7) Opposes the Deleterious Effects of Ang II</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-8.png') }}" width="594" height="561" alt="figure-8">

							<h5>Santos RAS, Ferreira AJ, Verano-Braga T, Bader M. Angiotensin-converting enzyme 2, angiotensin-(1-7) and Mas: new players of the renin-angiotensin system. <em>J Endocrinol.</em> 2013;216:R1–R17.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>3. Stimulating the Protective Arm of the RAAS as a Therapeutic Strategy in HF</h2>
													

						<p>Inhibiting the excitatory arm of the RAAS, with the use of ACE inhibitors and angiotensin receptor blockers (ARBs), is a widely used and proven strategy in the treatment of cardiovascular diseases. However, activating the protective arm of the RAAS may represent an effective alternative or complementary approach to the use of these agents.<sup>2</sup> Potential strategies to activate the protective arm of the RAAS include direct administration of Ang-(1-7), use of selective agonists of the Mas receptor, and increasing endogenous production of Ang-(1-7).<sup>3</sup></p>

						<p>Low doses of Ang-(1-7) have been found to improve cardiac output in rats. Infusion of femtomolar doses of Ang-(1-7) increased cardiac index by 30% and decreased total peripheral resistance <strong>[Figure 9]</strong>, while higher doses had the opposite effect.<sup>19</sup> In a rat model of HF, infusion of Ang-(1-7) for 8 weeks after coronary artery ligation preserved cardiac function, coronary perfusion, and aortic endothelial function <strong>[Figure 10]</strong>.<sup>20</sup> Ang-(1-7) also appears to have a vasodilating effect. Infusion of Ang-(1-7) in 8 healthy volunteers potentiated the vasodilating effect of bradykinin in a dose-dependent manner.<sup>21</sup> </p>

						<div class="figure">
								<h4>Figure 9: Ang-(1-7) Increased Cardiac Index by 30% in Rats </h4>

								<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-9.png') }}" width="594" height="417" alt="figure-9">

								<h5>*<em>P</em>&lt;0.05 vs. basal values.</h5>
								<h5>Sampaio WO, Nascimento AAS, Santos RAS. Systemic and regional hemodynamic effects of angiotensin-(1-7) in rats. <em>Am J Physiol Heart Circ Physiol.</em> 2003;284:H1985–H1994.</h5>
						</div>

							<div class="figure">
								<h4>Figure 10: Ang-(1-7) Attenuated Post-Myocardial Infarction Damage in a Rat Model of HF</h4>

								<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-10.png') }}" width="594" height="307" alt="figure-10">

								<h5>*<em>P</em>&lt;0.05 vs. sham.</h5>
								<h5>Loot AE, Roks AJM, Henning RH, et al. Angiotensin-(1-7) attenuates the development of heart failure after myocardial infarction in rats. <em>Circulation.</em> 2002;105:1548–1550.</h5>
						</div>

						<p>Further evidence of the therapeutic potential of Ang-(1-7) comes from studies of selective Mas receptor agonists such as AVE-0991. AVE-0991 improved cardiac function in normal rats and had cardioprotective effects in a rat model of heart failure, decreasing infarct area and preventing infarction-induced vasoconstriction.<sup>22</sup> The Mas receptor agonist CGEN-856S demonstrated cardioprotective effects in 2 rat models of cardiac failure, preventing isoproterenol-induced myocardial hypertrophy and fibrosis, improving cardiac function, and reducing myocardial injury after infarction <strong>[Figure 11]</strong>.<sup>23</sup> </p>

						<div class="figure">
								<h4>Figure 11: Mas Receptor Agonists Demonstrated Cardioprotective Effects by Reducing Infarct Size After MI in Rat Models of Cardiac Failure</h4>

								<img src="{{ asset('assets/css/img/modules/clinic/module_05/img/figure-11.png') }}" width="594" height="347" alt="figure-11">

								<h5>Quantification of the infarct area of animals treated with a Mas agonist (CGEN-856S) or ACE inhibitor (captopril). Values are expressed as mean ± SEM; n=7–8 animals.</h5>
								<h5>*<em>P</em>&lt;0.05 vs. sham; <sup>&dagger;</sup><em>P</em>&lt;0.05 vs. MI + vehicle.</h5>
								<h5>Savergnini SQ, Ianzer D, Carvalho MBL, et al. The novel Mas agonist, CGEN-856S, attenuates isoproterenol-induced cardiac remodeling and myocardial infarction injury in rats. <em>PLoS One.</em> 2013;8:e57757).</h5>
						</div>

						<p>The protective arm of the RAAS may also be activated by increasing endogenous production of Ang-(1-7). As discussed in Section 1, neprilysin catalyzes the direct conversion of Ang I to Ang-(1-7) and thus is an important activator of Ang-(1-7).<sup>2</sup> In a study of wild-type and ACE2 knockout mice, neprilysin-dependent conversion of Ang I to Ang-(1-7) appeared to be the main pathway of Ang-(1-7) formation in murine kidneys.<sup>24</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>4. Summary</h2>

						<p>For the last 2-3 decades, blockade of the ACE/Ang II/ AT1R &ldquo;excitatory&rdquo; axis of the RAAS has been the cornerstone of treatment for cardiovascular diseases. The focus of this approach has been inhibiting the activity of the potent vasoconstrictor Ang II with the use of ACE inhibitors or ARBs. However, in recent years it has become clear that the human body has its own counterregulatory mechanism to control Ang II activity, namely the ACE2/Ang-(1-7)/Mas axis. Ang-(1-7), derived primarily from the ACE2-catalyzed conversion of Ang II, has vasodilatory, antifibrotic, and other cardioprotective effects mediated via the Mas receptor that counteract the effects of Ang II. In animal models of HF, Ang-(1-7) has been shown to preserve cardiac function and reduce cardiac hypertrophy. Activation of this protective arm of the RAAS is a novel approach to interrupting RAAS activation and has great potential as a therapeutic strategy in HF and other cardiovascular diseases.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Hasenfuss G, Mann DL. Pathophysiology of heart failure. In: Mann DL, Zipes DP, Libby P, et al, eds. <em>Braunwald’s Heart Disease: A Textbook of Cardiovascular Medicine.</em> 10th ed. Philadelphia, PA: Elsevier Saunders; 2015:454–472.e3.</li>
							<li>Keidar S, Kaplan M, Gamliel-Lazarovich A. ACE2 of the heart: From angiotensin I to angiotensin (1-7). <em>Cardiovasc Res.</em> 2007;73:463–469. </li>
							<li>Santos RA. Angiotensin-(1-7). <em>Hypertension.</em> 2014;63:1138–1147.</li>
							<li>Ferreira AJ, Santos RAS. Cardiovascular actions of angiotensin-(1-7). <em>Braz J Med Biol Res.</em> 2005;38:499–507.</li>
							<li>Passos-Silva DG, Verano-Braga T, Santos RAS. Angiotensin-(1-7): beyond the cardio-renal actions. <em>Clin Sci (Lond).</em> 2013;124:443–456.</li>
							<li>Zisman LS, Meixell GE, Bristow MR, et al. Angiotensin-(1-7) formation in the intact human heart: in vivo dependence on angiotensin II as substrate. <em>Circulation.</em> 2003;108:1679–1681.</li>
							<li>Averill DB, Ishiyama Y, Chappell MC, et al. Cardiac angiotensin-(1-7) in ischemic cardiomyopathy. <em>Circulation.</em> 2003;108:2141–2146.</li>
							<li>Santos RAS, Simoes e Silva AC, Maric C, et al. Angiotensin-(1-7) is an endogenous ligand for the G protein-coupled receptor Mas. <em>Proc Natl Acad Sci U S A.</em> 2003;100:8258–8263.</li>
							<li>Gironacci MM. Angiotensin-(1-7): beyond its central effects on blood pressure. <em>Ther Adv Cardiovasc Dis.</em> 2015;9:209–216.</li>
							<li>Zhi J-M, Chen R-F, Wang J, et al. Comparative studies of vasodilating effects of angiotensin-(1-7) on the different vessels. <em>Acta Physiol Sinica.</em> 2004;56:730–734.</li>
							<li>Santos RAS, Ferreira AJ, Verano-Braga T, et al. Angiotensin-converting enzyme 2, angiotensin-(1-7) and Mas: new players of the renin-angiotensin system. <em>J Endocrinol.</em> 2013;216:R1–R17.</li>
							<li>Ferreira AJ, Santos RAS, Almeida AP. Angiotensin-(1-7): cardioprotective effect in myocardial ischemia/reperfusion. <em>Hypertension.</em> 2001;38(3 pt 2):665–668.</li>
							<li>Qi Y, Shenoy V, Wong F. Lentivirus-mediated overexpression of angiotensin-(1-7) attenuated ischaemia-induced cardiac pathophysiology. <em>Exp Physiol.</em> 2011;96:863–874.</li>
							<li>Santos RAS, Ferreira AJ, Nadu AP, et al. Expression of an angiotensin-(1-7)-producing fusion protein produces cardioprotective effects in rats. <em>Physiol Genomics.</em> 2004;17:292–299.</li>
							<li>Iwata M, Cowling RT, Gurantz D, et al. Angiotensin-(1-7) binds to specific receptors on cardiac fibroblasts to initiate antifibrotic and antitrophic effects. <em>Am J Physiol Heart Circ Physiol. </em>2005;289:H2356–H2363.</li>
							<li>Tallant EA, Clark MA. Molecular mechanisms of inhibition of vascular growth by angiotensin-(1-7). <em>Hypertension.</em> 2003;42:574–579.</li>
							<li>Tallant EA, Ferrario CM, Gallagher PE. Angiotensin-(1-7) inhibits growth of cardiac myocytes through activation of the mas receptor. <em>Am J Physiol Heart Circ Physiol.</em> 2005;289:H1560–H1566.</li>
							<li>Machado RDP, Santos RAS, Andrade SP. Mechanisms of angiotensin-(1-7)–induced inhibition of angiogenesis. <em>Am J Physiol Regul Integr Comp Physiol.</em> 2001;280:R994–sR1000.</li>
							<li>Sampaio WO, Nascimento AAS, Santos RAS. Systemic and regional hemodynamic effects of angiotensin-(1-7) in rats. <em>Am J Physiol Heart Circ Physiol.</em> 2003;284:H1985–H1994.</li>
							<li>Loot AE, Roks AJM, Henning RH, et al. Angiotensin-(1-7) attenuates the development of heart failure after myocardial infarction in rats. <em>Circulation.</em> 2002;105:1548–1550.</li>
							<li>Ueda S, Masumori-Maemoto S, Wada A, et al. Angiotensin (1-7) potentiates bradykinin-induced vasodilatation in man. <em>J Hypertens.</em> 2001;19:2001–2009.</li>
							<li>Ferreira AJ, Jacoby BA, Araújo CAA, et al. The nonpeptide angiotensin-(1-7) receptor Mas agonist AVE-0991 attenuates heart failure induced by myocardial infarction. <em>Am J Physiol Heart Circ Physiol.</em> 2007;292:H1113–H1119.</li>
							<li>Savergnini SQ, Ianzer D, Carvalho MBL, et al. The novel Mas agonist, CGEN-856S, attenuates isoproterenol-induced cardiac remodeling and myocardial infarction injury in rats. <em>PLoS One.</em> 2013;8:e57757.</li>
							<li>Domenig O, Manzel A, Grobe N, et al. The role of neprilysin in angiotensin 1-7 formation in the kidney. <em>J Hypertens.</em> 2013;33(esuppl 1): e114. Abstract 8D.05.</li>
						</ol>

					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->