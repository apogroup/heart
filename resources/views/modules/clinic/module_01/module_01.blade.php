<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 1: The “Frailty” of the Heart Failure (HF) Patient</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1. Evaluating Rates of SCD in Patients With HF</a></li>
				<li class="chapter-btn chapter-2"><a>2. The Impact of Standard-of-Care Therapy on SCD</a></li>
				<li class="chapter-btn chapter-3"><a>3. The Unpredictable Clinical Course of HF</a></li>
				<li class="chapter-btn chapter-4"><a>4. Risk Factors for SCD</a></li>
				<li class="chapter-btn chapter-5" metric="completion"><a>5. Summary</a></li>
				<li class="chapter-btn chapter-6"><a>References</a></li>

			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>

		<div class="gallery" id="module-1-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>Module 1: The “Frailty” of the Heart Failure (HF) Patient</h1>

						<h2>Module Description</h2>

						<p>Take a deeper look into the unpredictable clinical course of HF and the underlying mechanisms of sudden cardiac death in these patients</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Evaluate the rates of mortality and SCD in patients with HF and understand the impact of standard-of-care therapy</p></li>
							<li><p>Explore the unpredictable clinical course of HF and the potential mechanisms underlying SCD</p></li>
							<li><p>Review risk factors to identify patients with HF most at risk for SCD</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p>It is impossible to predict the clinical course for an individual patient with heart failure (HF) <strong>[Figure 1]</strong>.<sup>1</sup> Even patients exhibiting mild symptoms are at high risk for the next HF–related event, which could be death. HF-related mortality is on the rise and has significantly increased since 2009 <strong>[Figure 2]</strong>.<sup>2</sup> When compared with other diseases such as stroke and the most common cancers,<sup>a</sup> HF is associated with a comparable 5-year survival rate among diagnosed patients <strong>[Figure 3]</strong>.<sup>3b</sup> Indeed, approximately 50% of patients diagnosed with HF will die within 5 years.<sup>4</sup> The total population of patients with HF continues to grow. Projections show that the prevalence of HF in the United States will increase by 46% from 2012 to 2030 <strong>[Figure 4]</strong>. This will result in more than 8 million Americans aged 18 years or older with HF.<sup>5</sup></p>
						
						<div class="figure">
							<h4>Figure 1: Predicting the Clinical Course for an Individual Patient With HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-1.png') }}" width="594" height="402" alt="figure-1">
						</div>
						
						<div class="figure">
							<h4>Figure 2: HF-Related Deaths<sup>a</sup> in the United States From 2000-2014</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-2.png') }}" width="594" height="390" alt="figure-2">

							<h5><sup>a</sup>HF-related deaths are those with HF (International Classification of Diseases 10 code) reported anywhere on the death certificate (ie, as an underlying or contributing cause of death).</h5>

							<h5>Ni H, Xu JQ. NCHS Data Brief No. 231. Hyattsville, MD: National Center for Health Statistics; 2015.</h5>
						</div>

						<div class="figure">
							<h4>Figure 3: 5-Year Survival of Patients With HF Compared With That of Stroke and Most Common Cancers</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-3.png') }}" width="594" height="475" alt="figure-3">

							<h5><sup>a</sup>Lung, breast, prostate, colorectal. Data were obtained from large, adult, population-based and hospital-based studies from developed countries that reported observed age- and sex-adjusted or observed relative 5-year survival rates. Studies included were published between 2003 and 2009.</h5>

							<h5>Askoxylakis V et al. <em>BMC Cancer</em>. 2010;10:105.</h5>
						</div>
							
						<div class="figure">
							<h4>Figure 4: Projected Prevalence of HF in the United States<sup>a</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-4.png') }}" width="594" height="413" alt="figure-4">
							
							<h5><sup>a</sup>NHANES data (2009 – 2012).</h5>

							<h5>Heidenreich PA et al. <em>Circ Heart Fail</em>. 2013;6(3):606-619.</h5>
						</div>
						
						<ul class="footnotes">
							<li>
								<p><sup>a</sup>Lung, breast, prostate, colorectal.</p>
							</li>
							<li>
								<p><sup>b</sup>Data were obtained from large, adult, population-based and hospital-based studies from developed countries that reported observed age- and sex-adjusted survival rates or observed relative 5-year survival rates. Studies included were published between 2003 and 2009.</p>
							</li>
						</ul>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">
						
						<h2>1. Evaluating Rates of SCD in Patients with HF</h2>

						<div class="inline-figure-left">
							<h4>Figure 5</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-5.png') }}" width="326" height="141" alt="figure-5">
						</div>

						<p>From the late 1980s through 2001, there have been only small improvements in 5-year survival rates for patients with HF <strong>[Figures 5 and 6]</strong>.<sup>6</sup> Meanwhile, hospital discharge rates for HF have steadily increased during that same time.<sup>4</sup> Multiple hospitalizations are common for patients with HF, occurring nearly once per year over the course of their lifetime after HF diagnosis. In a prospective community study of 1077 patients with HF in Olmsted County, MN, 83% of patients were hospitalized at least once, and over half of patients were hospitalized at least 3 times.<sup>7</sup></p>
						
						<div class="figure">
							<h4>Figure 6: Kaplan-Meier Survival Curves for Patients With HFrEF (LVEF &lt;50%)</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-6.png') }}" width="594" height="431" alt="figure-6">

							<h5>HFrEF, HF with reduced ejection fraction; LVEF, left ventricular ejection fraction.</h5>

							<h5>Owan TE et al. <em>N Engl J Med</em> 2006;355(3):251-259.</h5>
						</div>

						<p>Mortality and hospital discharge rates in patients with HFrEF remain high despite the advent of new therapies. The key milestone trials in HFrEF over the past 2 decades included the SOLVD-T, SOLVD-P, MERIT-HF, BEST, COPERNICUS, RALES, and EMPHASIS-HF trials. These trials established the use of key classes of HF therapies: angiotensin-converting enzyme inhibitors (ACEIs), beta-blockers, and aldosterone blockers <strong>[Table 1]</strong>. <sup>8-15</sup></p>

						<div class="figure">
							<h4>Table 1: Morality and Hospital Discharge Rates in Patients With HFrEF Remain High Despite the Advent of New Therapies</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/table-1.png') }}" width="594" height="520" alt="table-1">

							<h5>CV, cardiovascular; RRR, relative risk reduction.</h5>

							<h5><sup>a</sup>Data from a subgroup analysis of MERIT-HF in patients with a history of hypertension.</h5>

							<h5>1. SOLVD Investigators. <em>N Engl J Med</em>. 1991;325(5):293-302. 2. SOLVD Investigators. <em>N Engl J Med</em>. 1992;327(10):685-691. 3. Pitt et al. <em>N Engl J Med</em>. 1999;341(10):709-717. 4. Herlitz et al. <em>J Cardiac Failure</em>. 2002;8(1):8-14. 5. MERIT-HF Study Group. <em>Lancet</em>. 1999;353(9169):2001-2007. 6. Beta-Blocker Evaluation of Survival Trial Investigators. <em>N Engl J Med</em>. 2001;344(22):1659-1667. 7. Packer et al.<em> N Engl J Med</em>. 2001;344(22):1651-1658. 8. Zannad et al. <em>N Engl J Med</em>. 2011;364(1):11-21.</h5>
						</div>

						<p>The mode of death for patients with HF can vary according to different stages or classes of the disease. Interestingly, patients with milder symptoms of HF are at higher risk for SCD compared with patients with more severe disease, who are more likely to die from congestive HF. In the MERIT-HF study, which ushered in the era of beta-blockers, SCD was the most common mode of death in New York Heart Association (NYHA) class II and III patients <strong>[Figure 7]</strong>.<sup>12,16</sup> Although mortality rate increases with higher NYHA class, the incidence of SCD is proportionally higher in patients with less severe HF (NYHA class II or III) <strong>[Figure 8]</strong>.<sup>12,17</sup></p>

						<div class="figure">
							<h4>Figure 7: Severity of HF and Mode of Death in the MERIT-HF Study (1999)<sup>1,2</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-7.png') }}" width="594" height="376" alt="figure-7">

							<h5><sup>a</sup>Other CV death includes all CV deaths not ascribed to pump failure or sudden death.</h5>

							<h5>1. Lane RE et al. <em>Heart</em>. 2005;91(5):674-680.</h5>

							<h5>2. Merit-HF Study Group. <em>Lancet</em>. 1999;353(9169):2001-2007.</h5>
						</div>

						<div class="figure">
							<h4>Figure 8: Total Annual Mortality and Total Deaths Classified as Sudden Death, %<sup>a</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-8.png') }}" width="594" height="460" alt="figure-8">

							<h5><sup>a</sup>The range of values are mortality estimates from published data.</h5>
							<h5>Uretsky BF et al. <em>JACC</em>. 1997;30(7):1589-1597.</h5>
						</div>
					
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>2. The Impact of Standard-of-Care Therapy on SCD</h2>
					
						<p>SCD has been assessed in contemporary studies that represent current standard-of-care management approaches in HF. Eplerenone in Mild Patients Hospitalization and Survival Study in Heart Failure (EMPHASIS-HF) was a randomized, double-blind trial in patients (n = 2737) with NYHA class II HF and an ejection fraction of no more than 35% receiving eplerenone or placebo.<sup>15</sup> All patients received recommended pharmacologic therapy for systolic HF (referred to as HFrEF). The percentage of patients in EMPHASIS-HF who received diuretics, ACE inhibitors, ARBs, or beta-blockers was 85%, 78%, 19%, and 87%, respectively. Yet despite administration of maximal care with standard-of-care therapy across the patient population, death from CV causes or hospitalization for HF (primary outcome) occurred in 18.3% in the eplerenone group and 25.9% in the placebo group over a 21-month follow-up <strong>[Figure 9]</strong>. SCD remained a leading cause of CV death at 5%.<sup>15</sup></p>

						<div class="figure">
							<h4>Figure 9: CV Death Rates Remained High in EMPHASIS-HF Despite Maximal Care, Including Eplerenone</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-9.png') }}" width="594" height="439" alt="figure-09">
							<h5>Zannad F et al. <em>N Engl J Med</em>. 2011;364(1):11-21.</h5>
						</div>

						<p>In a 2014 randomized, double-blind study, 8442 patients with NYHA class II, III, and IV HF and an ejection fraction of no more than 40% received maximal care with standard-of-care therapy. Most patients were receiving recommended pharmacologic therapy for HF. The percentage of enrolled patients who received diuretics, mineralocorticoid antagonists, digitalis, beta-blockers, or an implantable cardioverter defibrillator (ICD) was 80%, 60%, 30%, 93%, and 15%, respectively. CV death remained high despite maximal care, which included ICDs; SCD was a leading cause of death <strong>[Figure 10]</strong>.<sup>19</sup></p>

						<div class="figure">
							<h4>Figure 10: CV Death Remained High in PARADIGM-HF Despite Maximal Care</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-10.png') }}" width="594" height="370" alt="figure-10">
							<h5><sup>a</sup>Other CV death includes all CV deaths not ascribed to pump failure or sudden death.</h5> 
							<h5>Desai AS et al. <em>Eur Heart J</em>. 2015;36(30):1990-1997.</h5>
						</div>

						<p>According to the  American College of Cardiology Foundation (ACCF) and American Heart Association (AHA) guideline class IIa level recommendations for HF, ICDs are considered a reasonable approach to reducing risk of death in patients with asymptomatic ischemic cardiomyopathy who are at least 40 days post-MI, have an LVEF &le;30%, and are on guideline-directed medical therapy.<sup>20</sup> The 2005 SCD-HeFT trial shed further light on the benefit of ICD for prevention of sudden death in patients with HF. SCD-HeFT was a randomized, double-blinded study designed to enroll 2521 patients with NYHA class II or II HF receiving standard-of-care therapy. Of note, beta-blocker therapy may not have been maximally optimized in this trial, as baseline therapy usage of beta-blockers was 69%. Although ICD implantation reduced overall mortality by 23% In patients with NYHA class II or III HF, the risk of mortality remains high for these patients <strong>[Figure 11]</strong>. Mortality rates were similar for ischemic or nonischemic causes of CHF. However, patients with NYHA class III HF had no apparent reduction in the risk of death with ICD implantation compared with placebo <strong>[Table 2]</strong>.<sup>21</sup></p>

						<div class="figure">
							<h4>Figure 11: ICD Implantation Reduced All-Cause Mortality by 23% In Patients With NYHA Class II or III HF</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-11.png') }}" width="594" height="459" alt="figure-11">
							<h5>Bardy GH et al. <em>N Engl J Med</em>. 2005;352(3):225-237.</h5>
						</div>

						<div class="figure">
							<h4>Table 2: Risk of Death With ICD Implantation By NYHA HF Class</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/table-2.png') }}" width="594" height="92" alt="table-2">
							<h5>CI, conﬁdence interval; HR, hazard ratio.</h5>
							<h5>Bardy GH et al. <em>N Engl J Med</em>. 2005;352(3):225-237.</h5>
						</div>

						<p>In addition to NYHA class differences in ICD benefit, there may also be gender differences as well. Sex differences in ICD benefit were evaluated across a large cohort of patients with HF across 5 trials or registries. Although the proportion of sudden death was 30% lower for women compared with men, the proportion of pump failure death was 54% higher <strong>[Figure 12]</strong>.<sup>22</sup></p>

						<div class="figure">
							<h4>Figure 12: Kaplan-Meier Observed Annual Mortality and Percentage of Deaths Attributable to Pump Failure</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-12.png') }}" width="594" height="530" alt="figure-12">
							<h5>SHFM, Seattle Heart Failure Model.</h5>
							<h5>Rho RW, et al. <em>Circulation</em>. 2012;126:2402-2407.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>3. The Unpredictable Clinical Course of HF</h2>
					
						<p>It is impossible to predict the clinical course for an individual patient with HF <strong>[Figure 13]</strong>. The prognosis of HF is complicated by the unique contrast between unexpected sudden death and lingering death with congestive symptoms, such as could be the result of progressive pump failure or worsening HF.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 13: The Clinical Course of HF is Relatively Unpredictable for Individual Patients</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-13.png') }}" width="594" height="475" alt="figure-13">
							<h5>Allen LA et al. <em>Circulation</em>. 2012;125(15):1928-1952.</h5>
						</div>

						<p>Further complicating the matter, worsening of HF can occur asymptomatically in patients with diagnosed CHF. In an observational study of treated and symptomatically stable CHF patients (n = 74), 42% of asymptomatic patients actually experienced worsening of HF over a mean follow-up period of 21 months <strong>[Figure 14]</strong>. Asymptomatic worsening was defined as an event indicative of fluid retention diagnosed by a physician despite the absence of symptoms.<sup>23a,b</sup></p> 

						<div class="inline-figure-right">
							<h4>Figure 14:</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-14.png') }}" width="218" height="269" alt="figure-14">
							<h5>Kataoka H. <em>Congest Heart Fail</em>. 2012;18(1):37-42.</h5>
						</div>

						<p>The very definition of SCD remains controversial and subject to interpretation. Typically, deaths labeled as SCD are not witnessed. Thus, in the absence of cardiac monitoring at the time of death, it is purely speculative to assume the underlying cause of SCD as arrhythmic in nature.<sup>16</sup> SCD is often thought to be initiated by arrhythmic events; however, it can also be caused by nonarrhythmic events. The mechanism for SCD in nonischemic cardiomyopathies is less understood. In HF, small infarctions may accumulate over time and tip the balance to SCD or fatal HF <strong>[Figure 15]</strong>.<sup>17,24</sup> A recent meta-analysis of 15 studies with a total of 2727 patients with nonischemic cardiomyopathies and myocardial scarc may, in part, support this hypothesis. In this meta-analysis, the presence of myocardial scar was associated with &gt;3-fold higher risk for major cardiac events compared with patients without scar and a 5-fold increased rate of ventricular tachyarrhythmia or SCD <strong>[Figure 16]</strong>.</p>

						<div class="figure">
							<h4>Figure 15: In HF, Small Infarctions May Accumulate Over Time and Tip the Balance to SCD or Fatal HF</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-15.png') }}" width="594" height="416" alt="figure-15">
							<h5>Uretsky BF et al. <em>J Am Coll Cardiol</em>. 1997;30(7):1589-1597.</h5>
						</div>

						<div class="figure">
							<h4>Figure 16: Presence of Myocardial Scar Was Associated With Increased Risk for Major Cardiac Events</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-16.png') }}" width="594" height="471" alt="figure-16">
							<h5>Kim EK et el. <em>Korean J Radiol</em>. 2015;16(4):683-695.</h5>
						</div>

						<ul class="footnotes">
							<li>
								<p><sup>a</sup>Patients were NYHA class II (78%) and III (22%) and received diuretics (99%), ACEI/ARB (61%), beta-blockers (45%), and CCB (41%).</p>
							</li>
							<li>
								<p><sup>b</sup>Asymptomatic worsening events included pulmonary rales, S3 gallop, or pleural effusion on ultrasonography.</p>
							</li>
							<li>
								<p><sup>c</sup>Myocardial scar and fibrosis were assessed by delayed-enhancement cardiac magnetic resonance imaging.</p>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>4. Risk Factors for SCD</h2>

						<div class="inline-figure-right">
							<h4>Figure 17: Who Is At Risk?</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-17.png') }}" width="326" height="170" alt="figure-17">
						</div>

						<p>There are no definitive, widely accepted markers to identify HF patients at greatest risk of SCD <strong>[Figure 17]</strong>.<sup>25</sup> Rather, there are many parameters that have been associated with the prognosis for the general HF population, such as age, gender, and diabetes.<sup>25</sup> Predictors of all-cause mortality and SCD within 5 years of hospitalization in HFrEF patients have been calculated from a medical chart review with data from patients with a primary diagnosis of HF during a 5-year period (1995–2000) in Minneapolis-St. Paul, including 1416 patients with reduced ejection fraction <strong>[Figure 18]</strong>.<sup>26</sup> There is overlap and alignment across these risk factor categories for all-cause mortality and SCD.</p>

						<div class="figure">
							<h4>Figure 18: Predictors of SCD Within 5 Years of Hospitalization in HFrEF Patients</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/figure-18.png') }}" width="594" height="281" alt="figure-17">
							<h5>*Risk factors calculated from a medical chart review with data from patients with a primary diagnosis of HF during a 5-year period (1995–2000) in Minneapolis-St. Paul, including 1416 patients with reduced ejection fraction. Multivariable Cox proportional hazards regression model with backward stepwise selection was used to calculate predictive factors.</h5>
						</div>

						<p>The Seattle Proportional Risk Model has been applied to help identify clinical factors associated with SCD. The odds ratio of sudden (vs nonsudden) death was assessed from an analysis of 9885 patients with HF without ICDs, of whom 2552 died during an average follow-up of 2.3 years. This analysis has provided a higher level of granularity around predictive factors <strong>[Table 3]</strong>.<sup>27</sup></p>

						<div class="figure">
							<h4>Table 3: SCD Predication: Seattle Proportional Risk Model</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_01/img/table-3.png') }}" width="594" height="517" alt="table-3">
							<h5>BMI, body mass index; NS, not signiﬁcant; NT-proBNP, N-terminal prohormone of brain natriuretic peptide.</h5>
							<h5><sup>a</sup>Odds ratio of sudden (vs nonsudden) death assessed from an analysis of 9885 patients with HF without ICDs, of whom 2552 died during an average follow-up of 2.3 years. An odds ratio of >1 represents an increased risk of sudden death, while an odds ratio of &lt;1 represents an increased risk of nonsudden death.</h5>
							<h5><sup>b</sup>Assessed by a separate retrospective multivariate analysis of 7237 patients.</h5>
							<h5><sup>c</sup>Assessed by a separate retrospective multivariate analysis of 4028 patients.</h5>
							<h5>Shadman R et al. <em>Heart Rhythm</em>. 2015;12(10):2069-2077.</h5>
						</div>
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>5. Summary</h2>

						<p>In summary, it is impossible to determine which of your patients with HF are at greatest or lowest risk of SCD, underscoring the need to ensure that all patients are optimally managed. Despite the advent of new therapies over the past decades, the mortality burden in patients with HFrEF remains high and the 5-year survival rate is comparable to that of many types of cancer. Interestingly, your patients with milder HF may be at greater risk of SCD compared with your more severe patients, as the incidence of SCD is proportionally higher in patients with less severe HF (ie, NYHA class II and III). Unfortunately, even asymptomatic, clinically stable patients receiving maximally tolerated recommended doses for HF are at risk of SCD. Several patient-specific factors associated with risk of sudden death have been identified. However, there are no definitive, widely accepted markers to identify HF patients at greatest risk of SCD.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Allen LA, Stevenson LW, Grady KL, et al. Decision making in advanced heart failure: a scientific statement from the American Heart Association. <em>Circulation</em>. 2012;125(15):1928-1952.</li>
							<li>Ni H, Xu JQ. Recent trends in heart failure-related mortality: United States, 2000–2014. NCHS data brief, no 231. Hyattsville, MD: National Center for Health Statistics; 2015.</li>
							<li>Askoxylakis V, Thieke C, Pleger ST, et al. Long-term survival of cancer patients compared to heart failure and stroke: a systematic review. <em>BMC Cancer</em>. 2010;10:105.</li>
							<li>Mozaffarian D, Benjamin EJ, Go AS, et al. Heart disease and stroke statistics--2015 update: a report from the American Heart Association. <em>Circulation</em>. 2015;131(4):e29-e322.</li>
							<li>Heidenreich PA, Albert NM, Allen LA, et al. Forecasting the impact of heart failure in the United States: a policy statement from the American Heart Association. <em>Circ Heart Fail</em>. 2013;6(3):606-619.</li>
							<li>Owan TE, Hodge DO, Herges RM, et al. Trends in prevalence and outcome of heart failure with preserved ejection fraction. <em>N Engl J Med</em>. 2006;355(3):251-259.</li>
							<li>Dunlay SM, Redfield MM, Weston SA, Therneau TM, Hall Long K, Shah ND, Roger VL. Hospitalizations after heart failure diagnosis: a community perspective. <em>J Am Coll Cardiol</em>. 2009;54:1695–1702.</li>
							<li>SOLVD Investigators. Effect of enalapril on survival in patients with reduced left ventricular ejection fractions and congestive heart failure. The SOLVD Investigators. <em>N Engl J Med</em>. 1991;325(5):293-302</li>
							<li>SOLVD Investigators. Effect of enalapril on mortality and the development of heart failure in asymptomatic patients with reduced left ventricular ejection fractions. The SOLVD Investigators. <em>N Engl J Med</em>. 1992;327(10):685-691.</li> 
							<li>Pitt B, Zannad F, Remme WJ, et al. The effect of spironolactone on morbidity and mortality in patients with severe heart failure. Randomized Aldactone Evaluation Study Investigators. <em>N Engl J Med</em>. 1999;341(10):709-717.</li>
							<li>Herlitz J, Wikstrand J, Denny M, et al. <em>J Cardiac Failure</em>. 2002;8(1):8-14.</li>
							<li>MERIT-HF Study Group. Effects of metoprolol CR/XL on mortality and hospitalizations in patients with heart failure and history of hypertension. <em>Lancet</em>. 1999;353(9169):2001-2007.</li> 
							<li>Beta-Blocker Evaluation of Survival Trial Investigators. A trial of the beta-blocker bucindolol in patients with advanced chronic heart failure. <em>N Engl J Med</em>. 2001;344(22):1659-1667.</li>
							<li>Packer M, Coats AJ, Fowler MB, et al. Effect of carvedilol on survival in severe chronic heart failure. <em>N Engl J Med</em>. 2001;344(22):1651-1658.</li>
							<li>Zannad F, McMurray JJ, Krum H, et al. Eplerenone in patients with systolic heart failure and mild symptoms. <em>N Engl J Med</em>. 2011;364(1):11-21.</li>
							<li>Lane RE, Cowie MR, Chow AW. Prediction and prevention of sudden cardiac death in heart failure. <em>Heart</em>. 2005;91(5):674-680.</li>
							<li>Uretsky BF, Sheahan RG. Primary prevention of sudden cardiac death in heart failure: will the solution be shocking? <em>J Am Coll Cardiol</em>. 1997;30(7):1589-1597.</li>
							<li>McMurray JJ, Packer M, Desai AS, et al. Baseline characteristics and treatment of patients in prospective comparison of ARNI with ACEI to determine impact on global mortality and morbidity in heart failure trial (PARADIGM-HF). <em>Eur J Heart Fail</em>. 2014;16(7):817-825.</li>
							<li>Desai AS, McMurray JJ, Packer M, et al. Effect of the angiotensin-receptor-neprilysin inhibitor LCZ696 compared with enalapril on mode of death in heart failure patients. <em>Eur Heart J</em>. 2015;36(30):1990-1997.</li>
							<li>Yancy CW, Jessup M, Bozkurt B, et al. 2013 ACCF/AHA guideline for the management of heart failure: executive summary: a report of the American College of Cardiology Foundation/American Heart Association Task Force on practice guidelines. <em>Circulation</em>. 2013;128(16):e240-e327.</li>
							<li>Bardy GH, Lee KL, Mark DB, et al. Amiodarone or an implantable cardioverter-defibrillator for congestive heart failure. <em>N Engl J Med</em>. 2005;352(3):225-237.</li>
							<li>Rho RW, Patton KK, Poole JE, et al. important differences in mode of death between men and women with heart failure who would qualify for a primary prevention implantable cardioverter-defibrillator. <em>Circulation</em>. 2012;126:2402-2407.</li>
							<li>Kataoka H. Detection of preclinical body fluid retention in established heart failure patients during follow-up by a digital weight scale incorporating a bioelectrical impedance analyzer. <em>Congest Heart Fail</em>. 2012;18(1):37-42.</li>
							<li>Kim EK, Chattranukulchai P, Klem I. cardiac magnetic resonance scar imaging for sudden cardiac death risk stratification in patients with non-ischemic cardiomyopathy. <em>Korean J Radiol</em>. 2015;16(4):683-695.</li>
							<li>Mosterd A, Hoes W. Clinical epidemiology of heart failure. <em>Heart</em>. 2007;93(9):1137-1146.</li> 
							<li>Adabag S, Smith LG, Anand IS, et al. Sudden cardiac death in heart failure patients with preserved ejection fraction. <em>J Cardiac Fail</em>. 2012;18(10):749-754.</li>
							<li>Shadman R, Poole JE, Dardas TF, et al. A novel method to predict the proportional risk of sudden cardiac death in heart failure: Derivation of the Seattle Proportional Risk Model. <em>Heart Rhythm</em>. 2015;12(10):2069-2077.</li>
						</ol>

					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->