<div class="module-wrapper">

	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 7:  A Case-Based Exploration of the 2016 ACC/AHA/HFSA Heart Failure Guideline Update</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1.	Review of and Update to Classification of Recommendations and Level of Evidence</a></li>
				<li class="chapter-btn chapter-2"><a>2.	Recommendations for RAAS Inhibition</a></li>
				<li class="chapter-btn chapter-3"><a>3.	Recommendations for Ivabradine</a></li>
				<li class="chapter-btn chapter-4"><a>4.	Patient case: Jon</a></li>
				<li class="chapter-btn chapter-5"><a>5.	Patient case: Marcus</a></li>
				<li class="chapter-btn chapter-6" ><a>6. Patient case: Shannon</a></li>
				<li class="chapter-btn chapter-7"><a>7.	The Impact of Poor Guideline Adherence on Patient Outcomes</a></li>
				<li class="chapter-btn chapter-8" metric="completion"><a>8. Summary</a></li>
				<li class="chapter-btn chapter-9"><a>References</a></li>
				<li class="chapter-btn chapter-10"><a>Keywords</a></li>
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-7-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>Module 7: A Case-Based Exploration of the 2016 ACC/AHA/HFSA Heart Failure Guideline Update</h1>

						<h2>Module Description</h2>

						<p>This module will review the 2016 update to the ACC/AHA/HFSA heart failure treatment guideline and discuss patient case studies that may illustrate the potential impact of this update.</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Educate healthcare providers on the recent update to the ACC/AHA/HFSA heart failure (HF) treatment guideline</p></li>
							<li><p>Provide illustrative patient case examples that demonstrate how the new HF guidelines can be implemented in clinical practice</p></li>
							<li><p>Discuss real-world rates of guideline adherence and outline the impact of not following guideline-based treatment recommendations on patient outcomes</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p>Management guidelines are periodically updated to reflect current knowledge and newly available treatment options, and to provide healthcare providers with up-to-date direction for medical care. To this end, in 2016 the American College of Cardiology (ACC), the American Heart Association (AHA), and the Heart Failure Society of America (HFSA) developed and published the <em>2016 ACC/AHA/HFSA Focused Update on New Pharmacological Therapy for Heart Failure</em>, an update to the previously published 2013 ACC/AHA Guideline for the Management of Heart Failure.<sup>1,2</sup> These guidelines, developed concurrently with the <em>2016 European Society of Cardiology (ESC) Guideline on the Diagnosis and Treatment of Acute and Chronic Heart Failure</em>, were designed to incorporate knowledge gained from new clinical data and recently available therapeutics into clinical practice.<sup>1,3</sup> Two new therapies were included in the updated guidelines: the angiotensin receptor-neprilysin inhibitor (ARNI) sacubitril/valsartan and the sinoatrial node modulator ivabradine. These recommendations incorporate the most recently available data to provide healthcare providers with an expert approach to managing patients with HF.<sup>1</sup>  </p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						
						<h2>1. Review of and Update to Classification of Recommendations and Level of Evidence</h2>

						<p>The ACC/AHA task force developed an evidence-based approach to analyzing data and establishing recommendations. Each recommendation is accompanied by a Class of Recommendation (COR) and a Level of Evidence (LOE). COR and LOE are independently derived according to established criteria and may help the reader to better understand the strength and level of support for each recommendation <strong>(Figure 1)</strong>.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 1: Applying Class of Recommendation and Level of Evidence to Clinical Strategies, Interventions, Treatments, or Diagnostic Testing in Patient Care* (August 2015 ACCF/AHA/HFSA Update)<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-1.png') }}" width="500" height="422" alt="figure-1">

							<h5>COR and LOE are determined independently (any COR may be paired with any LOE).</h5>

							<h5>A recommendation with LOE Level C does not imply that the recommendation is weak. Many important clinical questions addressed in guidelines do not lend themselves to clinical trials. Although randomized clinical trials are unavailable, there may be a very clear clinical consensus that a particular test or therapy is useful or effective.</h5>

							<h5>EO, expert opinion; LD, limited data; NR, nonrandomized; R, randomized.</h5>

							<h5><sup>*</sup>The outcome or result of the intervention should be specified (an improved clinical outcome or increased diagnostic accuracy or incremental prognostic information).</h5>

							<h5><sup>&dagger;</sup>For comparative-effectiveness recommendations (COR I and IIa; LOE A and B only), studies that support the use of comparator verbs should involve direct comparisons of the treatments or strategies being evaluated.</h5>

							<h5><sup>&Dagger;</sup>The method of assessing quality is evolving, including the application of standardized, widely used, and preferably validated evidence-grading tools, as well as systematic reviews and the incorporation of an Evidence Review Committee.</h5>

							<h5>1. Yancy CW et al. <em>Circulation.</em> 2016;134:e282-e293.</h5>
						</div>

						<p>The COR indicates the strength of the recommendation. This parameter estimates the scale and certainty of benefit associated with a clinical intervention in proportion to the risk. Class recommendations range from Class I: Strong Benefit (the intervention has a high benefit-to-risk ratio) to Class III: Harm (the intervention is associated with greater risk than benefit).<sup>1</sup></p>

						<p>The LOE is an estimate of the likelihood of the treatment effect. The LOE rates the quality of scientific evidence supporting the intervention on the basis of the type, quantity, and consistency of data. The guideline writing committee reviewed and ranked each recommendation with the weight of evidence ranked as LOE A, B, or C.<sup>1</sup> To provide increased granularity, the updated guideline subcategorizes LOE Class B and Class C <strong>(Figure 1)</strong>.<sup>1</sup></p>
					
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>2. Recommendations for RAAS Inhibition</h2>

						<p>The updated guideline recommends inhibition of the renin-angiotensin-aldosterone-system (RAAS) in conjunction with &beta;-blockers to reduce morbidity and mortality in patients with stage C HF with reduced ejection fraction (HFrEF) <strong>(Figure 2)</strong>.<sup>1</sup> Three types of therapy are available that target the RAAS: angiotensin-converting enzyme (ACE) inhibitors, angiotensin-receptor blockers (ARBs), and ARNIs <strong>(Figure 3)</strong>.<sup>1</sup> </p>

						<div class="figure">
							<h4>Figure 2. Recommendations for RAAS Inhibition With An ACE Inhibitor, ARB, or ARNI<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-2.png') }}" width="452" height="128" alt="figure-2">

							<h5>1. Yancy CW et al. <em>Circulation.</em> 2016;134:e282-e293.</h5>
						</div>

						<div class="figure">
							<h4>Figure 3. Recommendations by Individual RAAS Inhibitor Class<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-3.png') }}" width="452" height="174" alt="figure-3">

							<h5>1. Yancy CW et al. <em>Circulation.</em> 2016;134:e282-e293.</h5>
						</div>

						<p>Randomized controlled trials (RCTs) have established the benefits of ACE inhibition in patients with mild, moderate, or severe symptoms of HF.<sup>1,4-9</sup> ACE inhibitors should be initially given at low doses and titrated upward to doses shown to reduce the risk of cardiovascular (CV) events in clinical trials.<sup>1</sup> ACE inhibitors may cause angioedema and should be given with caution to patients with low systemic blood pressure (BP), renal insufficiency, or elevated serum potassium.<sup>1</sup></p>  

						<p>Large RCTs conducted in patients with HFrEF have demonstrated that ARBs reduce mortality and HF hospitalizations.<sup>1,10-13</sup> Long-term treatment with ARBs produces hemodynamic, neurohormonal, and clinical effects consistent with RAAS interference.<sup>1</sup> ARBs should be given with caution to patients with low systemic BP, renal insufficiency, or elevated serum potassium.<sup>1</sup>  ARBs should also be started at low doses and titrated upward to reduce risk of cardiovascular events.<sup>1</sup> ARBs are recommended in patients with intolerance to ACE inhibitors due to cough or angioedema, but caution is advised because some patients have also developed angioedema with ARBs. <strong>(Figure 3)</strong>.<sup>1</sup></p>

						<p>The ARNI sacubitril/valsartan has been associated with significant decreases in hospitalizations and mortality compared with the ACE inhibitor enalapril.<sup>14</sup> Patients with mild-to-moderate HF and mildly elevated B-type natriuretic peptide (BNP) or N-terminal pro-BNP levels who were hospitalized in the previous 12 months and were able to tolerate an ARNI and ACE inhibitor were included in this study.<sup>14</sup> Based on these data, the guideline recommends that an ARNI may be a substitute for ACE inhibitors or ARBs in patients with symptomatic HFrEF and New York Heart Association (NYHA) class II or III HF <strong>(Figure 3)</strong>.<sup>1</sup></p>

						<p>Use of an ARNI is associated with hypotension and a low-frequency incidence of angioedema. Concomitant use of ACE inhibitors and an ARNI may lead to angioedema and should be avoided.<sup>1</sup> Angioedema is believed to occur when these 2 types of therapies are combined, because both an ACE and neprilysin can break down bradykinin, which can directly or indirectly cause angioedema.<sup>1</sup> To minimize the risk of angioedema, an ARNI should not be administered within 36 hours of switching from an ACE inhibitor or given to patients with a history of angioedema <strong>(Figure 4)</strong>.<sup>1</sup>  Additional clinical experience will provide further information on the optimal titration and tolerability of ARNIs.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 4. Safety Warnings with ARNIs<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-4.png') }}" width="452" height="87" alt="figure-4">

							<h5>1. Yancy CW et al. <em>Circulation</em>. 2016;134:e282-e293.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>3. Recommendations for Ivabradine</h2>

						<p>Ivabradine specifically inhibits the If current in the sinoatrial node, causing a reduction in heart rate (HR) without altering other aspects of CV function, including BP, myocardial contractility, intracardiac conduction, or ventricular repolarization.<sup>15-17</sup></p>

						<p>One RCT, conducted in patients with NYHA class II-IV HF (primarily class II and III) and a resting HR &ge;70 beats per minute (bpm), has demonstrated that ivabradine reduced the composite endpoint of CV deaths or HF hospitalizations. This benefit was driven by a reduction in HF hospitalization.<sup>18</sup>  Based on these results, the ACC/AHA/HFSA guideline recommends ivabradine to reduce hospitalizations in patients with symptomatic, stable chronic HF who are currently receiving guideline-directed medical therapy (GDEM), including a &beta;-blocker at the maximum tolerated dose <strong>(Figure 5)</strong>.<sup>1</sup> Ivabradine should only be provided to patients who are in sinus rhythm with a heart rate &ge;70 bpm at rest.<sup>1</sup> Given the established mortality benefits of &beta;-blocker therapy, it is important to initiate and uptitrate &beta;-blockers to target doses before assessing the resting HR for consideration of ivabradine treatment.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 5. Recommendations for Ivabradine<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-5.png') }}" width="452" height="105" alt="figure-5">

							<h5>1. Yancy CW et al. <em>Circulation.</em> 2016;134:e282-e293.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>4. Patient case: Jon</h2>

						<p>Jon is a 74-year-old white male with ischemic cardiomyopathy, progressive HF, and NYHA functional class III symptoms on GDEM who has been referred for further management <strong>(Figure 6)</strong>. Jon has a history of anterior wall myocardial infraction (AWMI), followed by coronary artery bypass graft (CABG) surgery. After surgery, his ejection fraction (EF) improved and he experienced reverse remodeling. However, over time his condition deteriorated and he experienced a second myocardial infarction (MI) that was complicated by acute mitral regurgitation. Jon is presently being managed by an internist with acetylsalicylic acid (ASA) 81 mg daily, enalapril 2.5 mg twice daily (BID), carvedilol 12.5 BID, furosemide 40 mg BID in conjunction with metolazone as needed (PRN), and atorvastatin 10 mg once daily (QD). Despite GDEM, he is experiencing dyspnea with mild excursion, occasional paroxysmal nocturnal dyspnea (PND), and edema. He appears ill, has some jugular venous distention, has a loud second heart sound and a soft systolic murmur, and has trace peripheral edema. To improve the management of Jon&rsquo;s HF, his internist has decided to refer him to a cardiologist.</p>

						<p>Please click on <strong>Figure 6</strong> to enter into an interactive mode and answer a question about Jon&rsquo;s treatment.</p>

						<div class="figure module-visible" module-number="7" module-question="4">
							<h4>Figure 6. Jon, An NYHA Class III Patient Referred for Treatment</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-6a.png') }}" width="522" height="546" alt="figure-6a">

							<h5>ECG, electrocardiogram; JVP, jugular venous pressure; LAD, left anterior descending; MR, mitral regurgitation; SR, sinus rhythm.</h5>
							<h5></h5>
						</div>
						
						<div class="module-question module-hidden" module-number="7" module-question="4">
							<h4>How would you treat this patient?</h4>

							<h5>Which of the following do you believe is the most appropriate treatment option for this patient (please note that no answer is right or wrong)?</h5>

								<div class="module-choices">
									<div class="module-choice">
										<input type="radio" name="module-07-4" value="Sacubitril/valsartan">
										<label for="Sacubitril/valsartan">Sacubitril/valsartan</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-4" value="Eplerenone">
										<label for="Eplerenone">Eplerenone</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-4" value="Ivabradine">
										<label for="Ivabradine">Ivabradine</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-4" value="Digoxin">
										<label for="Digoxin">Digoxin</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-4" value="Iron">
										<label for="Iron">Iron</label>
									</div>
								</div>

						</div>

						<div class="module-answer module-hidden" module-number="7" module-question="4">
							<h4 class="module-selection"></h4> 
							<h4>In a meeting at the 2016 HFSA conference, this same patient case was presented and this question was posed to an audience of approximately 200 attendees with the following results:</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-6b.png') }}" width="545" height="238" alt="figure-6b">

						</div>


					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>5. Patient case: Marcus</h2>

						<p>Marcus is a 42-year-old African American male with newly diagnosed HF, who is currently not taking any therapy for HF <strong>(Figure 7)</strong>. Several weeks ago, he began treatment for suspected bronchitis with an inhaler, steroids, and a Z-Pak.  Despite treatment, his symptoms persisted and he developed pedal edema. An ECG showed that he had left ventricular hypertrophy (LVH) with primary T-wave changes, and a chest x-ray showed cardiomegaly and diffuse vascular markings. Marcus was referred to a cardiologist, and presented with dyspnea on exertion, a cough, persistent palpitations, jugular venous pressure, 1+ edema, a fourth heart sound, and a soft systolic murmur. An ECG revealed mild dilation and dysfunction of the left and right ventricles and an LVEF of 25%-30%. After receiving these results, his cardiologist diagnosed him with HF.</p>

						<p>Please click on <strong>Figure 7</strong> to enter into an interactive mode and answer a question about how you would manage Marcus’s HF. </p>

						<div class="figure module-visible" module-number="7" module-question="5">
							<h4>Figure 7. Marcus, A Patient with Newly Diagnosed HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-7a.png') }}" width="535" height="560" alt="figure-7a">

							<h5>NSVT, nonsustained ventricular tachycardia; TR, tricuspid regurgitation.</h5>

						</div>


						<div class="module-question module-hidden" module-number="7" module-question="5">
							<h4>How would you treat this patient?</h4>

							<h5>Which of the following do you believe is the most appropriate treatment option for this patient (please note that no answer is right or wrong)?</h5>

								<div class="module-choices">
									<div class="module-choice">
										<input type="radio" name="module-07-5" value="Beta blocker">
										<label for="Beta blocker">Beta blocker</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-5" value="Isosorbide dinitrate plus hydralazine">
										<label for="Isosorbide dinitrate plus hydralazine">Isosorbide dinitrate plus hydralazine</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-5" value="RAAS inhibitor (ACEI, ARB, ARNI)">
										<label for="RAAS inhibitor (ACEI, ARB, ARNI)">RAAS inhibitor (ACEI, ARB, ARNI)</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-5" value="Mineralocorticoid receptor antagonist">
										<label for="Mineralocorticoid receptor antagonist">Mineralocorticoid receptor antagonist</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-5" value="Ivabradine">
										<label for="Ivabradine">Ivabradine</label>
									</div>
								</div>
							
						</div>

						<div class="module-answer module-hidden" module-number="7" module-question="5">
							<h4 class="module-selection">You chose answer X.</h4> 
							<h4>In a meeting the 2016 HFSA conference, this same patient case was presented and this question was posed to an audience of approximately 200 attendees with the following results:</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-7b.png') }}" width="544" height="237" alt="figure-7b">

							<h5> MRA, mineralocorticoid receptor antagonist.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						
						<h2>6. Patient case: Shannon</h2>

						<p>Shannon is a 67-year-old white female experiencing mild fatigue with exertion <strong>(Figure 8)</strong>. She has had mildly symptomatic chronic HF for over 3 years with NYHA class II symptoms. While her HF has mostly been well-controlled, she experienced an MI 3 years ago and was hospitalized for an acute exacerbation of HF 10 months ago. Shannon is currently being managed with enalapril 10 mg BID, carvedilol 25 mg BID, spironolactone 25 mg QD, and furosemide 80 mg BID and has tolerable symptoms. </p>

						<p>Please see full case details and enter into an interactive mode to answer a question about Shannon’s treatment course by clicking on <strong>Figure 8</strong>.</p>

						<div class="figure module-visible" module-number="7" module-question="6">
							<h4>Figure 8. Shannon, A Patient with Persistent Symptoms</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-8a.png') }}" width="535" height="560" alt="figure-8a">

						</div>

						<div class="module-question module-hidden" module-number="7" module-question="6">
							
							<h4>How would you treat this patient?</h4>

							<h5>Which of the following do you believe is the most appropriate treatment option for this patient (please note that no answer is right or wrong)?</h5>
							
								<div class="module-choices">
									<div class="module-choice">
										<input type="radio" name="module-07-6" value="Sacubitril/valsartan">
										<label for="Sacubitril/valsartan">Sacubitril/valsartan</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-6" value="Eplerenone">
										<label for="Eplerenone">Eplerenone</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-6" value="Ivabradine">
										<label for="Ivabradine">Ivabradine</label>
									</div>
									<div class="module-choice">
										<input type="radio" name="module-07-6" value="Digoxin">
										<label for="Digoxin">Digoxin</label>
									</div>
								</div>
						</div>
				
						<div class="module-answer module-hidden" module-number="7" module-question="6">
						
							<h4 class="module-selection">You chose answer X.</h4>
							<h4>Thank you for your response. Here are the HF guideline recommendations for these treatment options<sup>1,2</sup>:</h4>
							
							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-8b.png') }}" width="452" height="311" alt="figure-8b">

							<h5>1. Yancy CW et al. <em>Circulation</em>. 2016;134:e282-e293. 2. Yancy CW et al. <em>Circulation</em>. 2013;128:1810-1852.</h5>

						</div>						

					</div> 
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						<h2>7. The Impact of Poor Guideline Adherence on Patient Outcomes</h2>

						<p>Despite the well-established benefits of recommended therapies, healthcare providers do not always follow guideline recommendations in their clinical practice. Current treatment management was evaluated in patients with HFrEF treated in the outpatient setting in an analysis using data from the Registry to Improve the Use of Evidence-Based Heart Failure Therapies in the Outpatient Setting (IMPROVE HF) study.<sup>19</sup> Based on this analysis of medical records from 15,381 patients treated at 167 outpatient cardiology practices based on baseline data between 2005 and 2007, a median of 27% of patients received all HF therapies for which they were eligible, according to the 2005 ACC/AHA guideline.<sup>19</sup></p>

						<p>Poor guideline adherence by healthcare providers may impact patient outcomes. The consequences of poor adherence were assessed in the SUrvey of Guideline Adherence for Treatment of Systolic Heart Failure in Real World (SUGAR) study in Korea.<sup>20</sup> Performance measures were modified using definitions from the 2011 ACC/AHA guideline, clinical performance measures for adults with chronic HF, the Joint Commission on Accreditation of Healthcare Organizations, the OPTIMIZE- HF study, the MAHLER survey, and the ESC’s performance measure for aldosterone antagonists.<sup>20</sup> Guideline adherence was assessed using the guideline adherence indicator (GAI), a performance measure designed to quantify adherence to guidelines for 3 pharmacologic classes (ACE inhibitors or ARBs, &beta;-blockers, and aldosterone inhibitors). Patients were divided into 2 groups; good guideline adherence (2/2 or 2/3 guidelines followed) and poor guideline adherence (GAI 0/3 or 1/3 guidelines followed).<sup>20</sup>  As displayed in <strong>Figure 9</strong>, the results of this study demonstrated that patients with good adherence to the guideline at that time had favorable mortality and rehospitalization rates compared with patients with poor guideline adherence.<sup>20</sup></p>

						<div class="figure">
							<h4>Figure 9. Unadjusted Event-Free Survival Curves for Overall Mortality and Rehospitalization in Patients With HF With Good and Poor Guideline Adherence at Discharge<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_07/img/figure-9.png') }}" width="577" height="223" alt="figure-9">

							<h5>Good, good guideline adherence; Poor, poor guideline adherence.</h5>
							<h5>1. Yoo BS et al. <em>PLoS One.</em> 2014;9:e86596.</h5>

						</div>
					</div>
				</div>
			</div>


			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>8. Summary</h2>

						<p>In summary, the ACC/AHA/HFSA writing committee conducted an extensive review of the available clinical data to establish evidence-based treatment guidelines.<sup>1</sup>  Adhering to the treatment course recommended by the 2011 guideline has been associated with reduced hospitalization rates and improved overall survival following hospital discharges.<sup>20</sup> Despite these benefits, healthcare providers often do not adhere to guideline recommendations in their clinical practice.<sup>19</sup> By consulting with the 2016 ACC/AHA/HFSA guideline, physicians can ensure that their HF management approach is consistent with available clinical data.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Yancy CW, Jessup M, Bozkurt B, et al. 2016 ACC/AHA/HFSA focused update on new pharmacological therapy for heart failure: an update of the 2013 ACCF/AHA guideline for the management of heart failure. <em>Circulation</em>. 2016;134:e282-e293.</li>
							<li>Yancy CW, Jessup M, Bozkurt B, et al. 2013 ACCF/AHA guideline for the management of heart failure: executive summary: a report of the American College of Cardiology Foundation/American Heart Association Task Force on practice guidelines. <em>Circulation</em>. 2013;128:1810-1852.</li>
							<li>Ponikowski P, Voors AA, Anker SD, et al. 2016 ESC Guidelines for the diagnosis and treatment of acute and chronic heart failure. <em>Eur J Heart Fail</em>. 2016;18:891-975.</li>
							<li>The CONSENSUS Trial Study Group. Effects of enalapril on mortality in severe congestive heart failure. Results of the Cooperative North Scandinavian Enalapril Survival Study (CONSENSUS). <em>N Engl J Med</em>. 1987;316:1429-1435.</li>
							<li>The SOLVD Investigators. Effect of enalapril on survival in patients with reduced left ventricular ejection fractions and congestive heart failure. <em>N Engl J Med</em>. 1991;325:293-302.</li>
							<li>Packer M, Poole-Wilson PA, Armstrong PW, et al; on behalf of the ATLAS Study Group. Comparative effects of low and high doses of the angiotensin-converting enzyme inhibitor, lisinopril, on morbidity and mortality in chronic heart failure. <em>Circulation</em>. 1999;100:2312-2318.</li>
							<li>Pfeffer MA, Braunwald E, Moy&eacute; LA, et al; on behalf of the SAVE Investigators. Effect of captopril on mortality and morbidity in patients with left ventricular dysfunction after myocardial infarction: results of the Survival and Ventricular Enlargement trial. <em>N Engl J Med</em>. 1992;327:669-677.</li>
							<li>The Acute Infarction Ramipril Efficacy (AIRE) Study Investigators. Effect of ramipril on mortality and morbidity of survivors of acute myocardial infarction with clinical evidence of heart failure. <em>Lancet</em>. 1993;342:821-828.</li>
							<li>K&oslash;ber L, Torp-Pedersen C, Carlsen JE, et al; for the Trandolapril Cardiac Evaluation (TRACE) study group. A clinical trial of the angiotensin-converting-enzyme inhibitor trandolapril in patients with left ventricular dysfunction after myocardial infarction. <em>N Engl J Med</em>. 1995;333:1670-1676.</li>
							<li>Cohn JN, Tognoni G; for the Valsartan Heart Failure Trial Investigators. A randomized trial of the angiotensin receptor blocker valsartan in chronic heart failure. <em>N Engl J Med</em>. 2001;345:1667-1675.</li>
							<li>Pfeffer MA, McMurray JJV, Velazquez EJ, et al; for the Valsartan in Acute Myocardial Infarction Trial Investigators. Valsartan, captopril, or both in myocardial infarction complicated by heart failure, left ventricular dysfunction, or both. <em>N Engl J Med</em>. 2003;349:1893-1906.</li>
							<li>Konstam MA, Neaton JD, Dickstein K, et al; for the HEAAL Investigators. Effects of high-dose versus low-dose losartan on clinical outcomes in patients with heart failure (HEAAL study): a randomised, double-blind trial. <em>Lancet</em>. 2009;374:1840-1848.</li>
							<li>Pfeffer MA, Swedberg K, Granger CB, et al; for the CHARM Investigators and Committees. Effects of candesartan on mortality and morbidity in patients with chronic heart failure: the CHARM-Overall programme. <em>Lancet</em>. 2003;362:759-766.</li>
							<li>McMurray JJV, Packer M, Desai AS, et al; for the PARADIGM-HF Investigators and Committees. Angiotensin–neprilysin inhibition versus enalapril in heart failure. <em>N Engl J Med</em>. 2014;371:993-1004.</li>
							<li>Fox K, Ford I, Steg PG, Tendera M, Ferrari R; on behalf of the BEAUTIFUL Investigators. Ivabradine for patients with stable coronary artery disease and left-ventricular systolic dysfunction (BEAUTIFUL): a randomised, double-blind, placebo-controlled trial. <em>Lancet</em>. 2008;372:807-816.</li>
							<li>Joannides R, Moore N, Iacob M, et al. Comparative effects of ivabradine, a selective heart rate-lowering agent, and propranolol on systemic and cardiac haemodynamics at rest and during exercise. <em>Br J Clin Pharmacol</em>. 2006;61:127-137.</li>
							<li>Manz M, Reuter M, Lauck G, Omran H, Jung W. A single intravenous dose of ivabradine, a novel If inhibitor, lowers heart rate but does not depress left ventricular function in patients with left ventricular dysfunction. <em>Cardiology</em>. 2003;100:149-155.</li>
							<li>Swedberg K, Komajda M, B&ouml;hm M, et al; on behalf of the SHIFT Investigators. Ivabradine and outcomes in chronic heart failure (SHIFT): a randomised placebo-controlled study. <em>Lancet</em>. 2010;376:875-885.</li>
							<li>Fonarow GC, Yancy CW, Albert NM, et al. Heart failure care in the outpatient cardiology practice setting: findings from IMPROVE HF. <em>Circ Heart Fail</em>. 2008;1:98-106.</li>
							<li>Yoo B-S, Oh J, Hong B-K, et al; on behalf of the SUGAR Study. SUrvey of Guideline Adherence for Treatment of Systolic Heart Failure in Real World (SUGAR): a multi-center, retrospective, observational study. <em>PLoS One</em>. 2014;9:e86596.</li>
						</ol>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						<h2>Keywords</h2>

						<ol>
							<li>ACC/AHA/HFSA Guideline</li>
							<li>ACEI</li>
							<li>ARB</li>
							<li>ARNI</li>
							<li>Ivabradine</li>
							<li>Guideline adherence</li>
						</ol>
					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->