<div class="module-wrapper">

	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 9: Management of the Patient With Advanced Heart Failure</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1.	Burden of Advanced HF</a></li>
				<li class="chapter-btn chapter-2"><a>2.	Identifying Advanced Disease</a></li>
				<li class="chapter-btn chapter-3"><a>3.	Guideline-Recommended Approaches to Advanced HF Care</a></li>
				<li class="chapter-btn chapter-4"><a>4.	A Team Approach to Advanced HF Management</a></li>
				<li class="chapter-btn chapter-5"><a>5.	Shared Decision-Making in Advanced HF</a></li>
				<li class="chapter-btn chapter-6" metric="completion"><a>6.	Summary: Determining the Appropriate Treatment Course</a></li>
				<li class="chapter-btn chapter-7"><a>References</a></li>
				<li class="chapter-btn chapter-8"><a>Keywords</a></li>
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-9-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>Module 9: Management of the Patient With Advanced Heart Failure</h1>

						<h2>Module Description</h2>

						<p>This module will review advanced heart failure (HF) and discuss approaches to managing patients who have developed advanced HF.</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Educate healthcare providers regarding the progression to advanced HF</p></li>
							<li><p>Establish methods with which to identify patients who have developed advanced HF</p></li>
							<li><p>Present the guideline-recommended treatment options for patients with advanced HF</p></li>
							<li><p>Discuss the unique considerations associated with managing these patients</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p style="width: 605px;">Heart failure (HF) affects 2.4% of the adult population and more than 11% of those aged over 80 years.<sup>1</sup> The current approaches to managing HF generally slow rather than reverse disease progression. The prevalence of symptomatic HF and the duration of advanced disease have thus both increased.<sup>1</sup></p>

						<p style="width: 605px;">Some patients with HF will progress to advanced HF and experience symptoms despite usual HF treatment.<sup>1</sup>  As disease severity increases, the intensity of care will also increase.<sup>1</sup>  Managing patients who progress to advanced HF poses numerous challenges to healthcare practitioners. Because patients with advanced HF are refractory to guideline-directed medical therapies, limited treatment options are available.<sup>2</sup>  Patients with advanced HF have a compromised quality of life, a poor prognosis, and a high risk of clinical events.<sup>1,3</sup>  Furthermore, not all available treatment options will be appropriate for all patients, and some may not align with the patient&rsquo;s goals and lifestyle. Decision-making regarding the treatment of advanced HF should involve an advanced care planning approach and requires effective communication between patients and healthcare provider(s).<sup>4</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						
						<h2>1. Burden of Advanced HF</h2>

						<p>Although the clinical course of HF varies among patients, it is often characterized by a progressive decline in quality of life (QoL) that may be punctuated by acute decompensations or sudden cardiac death.<sup>1</sup>  Patients who progress to advanced HF become refractory to standard oral therapies, including renin-angiotensin-aldosterone system inhibitors and &beta;-blockers, and require specialized approaches to care (<strong>Figure 1</strong>).<sup>1,3,5</sup></p>

						<div class="figure">
							<h4>Figure 1. The Clinical Course of HF<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-1.png') }}" width="527" height="252" alt="figure-1">

							<h5>CHF, chronic heart failure; MCS, mechanical circulatory support.</h5>
							<h5>1. Allen LA et al.  <em>Circulation.</em> 2012;125:1928-1952.</h5>
						</div>

						<p>Owing to shifting definitions of advanced HF and the fact that many signs of advanced HF may be nonspecific, limited epidemiologic data on advanced HF are available.<sup>6</sup>  The prevalence of end-stage HF was assessed in ADHERE LM, the largest available registry of patients with advanced HF, which estimated that approximately 5% of patients who are living with HF have advanced disease.<sup>5,7</sup>  This study included 1433 adult patients with chronic stage D HF with refractory symptoms on oral medication and New York Heart Association (NYHA) functional class III or IV who had an HF hospitalization &ge;2 times in the previous year.<sup>5</sup> Data from these patients were prospectively collected and compared with ADHERE CM, a multicenter registry designed to prospectively collect data across the entire spectrum of acute decompensated HF, to determine prevalence and outcomes.<sup>5</sup> </p>

						<p>Patients with advanced HF experience death and hospitalization at high rates. The 1-year survival rate observed in patients with advanced HF included in ADHERE LM was about 72% (<strong>Figure 2</strong>).<sup>5</sup>  Estimated 1-year freedom from death or hospitalization was 32.9%  (95% CI 30.2%-35.6%) in this patient population.<sup>5</sup></p>

						<div class="figure">
							<h4>Figure 2. Kaplan-Meier–estimated Survival of Patients with Stage D Heart Failure Included in ADHERE LM<sup>2</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-2.png') }}" width="534" height="343" alt="figure-2">

							<h5>2. Costanzo MR et al.  <em>Am Heart J</em>. 2008;155:341-349.</h5>
						</div>
					
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>2. Identifying Advanced Disease</h2>

						<p>The American College of Cardiology (ACC) and the American Heart Association (AHA) define stage D HF as &ldquo;patients with truly refractory HF who might be eligible for specialized, advanced treatment strategies, such as mechanical circulatory support (MCS), procedures to facilitate fluid removal, continuous inotropic infusions, or cardiac transplantation or other innovative or experimental surgical procedures, or for end-of-life care, such as hospice.&rdquo;<sup>8</sup> This definition aligns with that given by the European Society of Cardiology for advanced HF (<strong>Figure 3</strong>).<sup>8</sup>  These overlapping definitions characterize a subset of patients with HF who experience a high symptom burden and diminished QoL.<sup>1</sup> </p>

						<div class="figure">
							<h4>Figure 3. European Society of Cardiology Definition of Advanced HF<sup>1</sup></h4>
							
							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-3.png') }}" width="516" height="471" alt="figure-3">

							<h5>BNP, B-type natriuretic peptide; CRT, cardiac resynchronization therapy; GDMT, guideline-directed medical therapy; LVEF, left ventricular ejection fraction; NT-pro-BNP, N-terminal pro-B-type natriuretic peptide; NYHA, New York Heart Association; P, pulmonary artery; PCWP, pulmonary capillary wedge pressure; RAP, right atrial pressure; Vo<sub>2</sub>, oxygen consumption.</h5>

							<h5>1. Yancy CW et al. <em>Circulation</em>. 2013;128:e240-e327.</h5>
						</div>

						<p>Recognizing patients who are progressing to advanced HF may help ensure that appropriate management strategies are initiated in a timely manner. Clinicians may be able to identify patients with advanced HF or patients who are progressing towards advanced HF by assessing clinical findings. Common clinical features to help identify patients with advanced HF are presented in <strong>Figure 4</strong>.<sup>8</sup>  These clinical features may predict poor outcomes and patients experiencing these events may benefit from a referral to an advanced HF treatment center for management approaches that are not routinely available in the community.<sup>9</sup> Determining the prognosis is challenging in patients with advanced HF. To ensure that patients with advanced HF are being managed appropriately, the clinical trajectory should be reassessed frequently.<sup>1</sup></p>
						
						<div class="figure">
							<h4>Figure 4. Clinical Events and Findings Useful for Identifying Patients<br>With Advanced HF<sup>1</sup> </h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-4.png') }}" width="572" height="245" alt="figure-4">

							<h5>ACE, angiotensin-converting enzyme; BUN, blood urea nitrogen; ED, emergency department; ICD, implantable cardioverter-defibrillator.</h5>

							<h5>1. Yancy CW et al. <em>Circulation</em>. 2013;128:e240-e327.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>3. Guideline-Recommended Approaches to Advanced HF Care</h2>

						<p>Because patients with advanced HF are refractory to traditional HF therapy, specialized approaches to management are necessary.<sup>2</sup>  ACC/AHA recommendations for managing patients with advanced HF include inotropic support, heart transplantation, and MCS (<strong>Figure 5</strong>).<sup>8</sup> </p>

						<div class="figure">
							<h4>Figure 5. ACC/AHA Recommendations and Warnings for Inotropic Support, MCS,<br>and Cardiac Transplantation<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-5.png') }}" width="452" height="446" alt="figure-5">

							<h5>BTT, bridge to transplantation; GDMT, guideline-directed medical therapy; HFrEF, heart failure with reduced ejection fraction.</h5>

							<h5>1. Yancy CW et al. <em>Circulation</em>. 2013;128:e240-e327.</h5>
						</div>

						<p><em><u>Inotropic support</u></em></p>

						<p>Inotropes are medications that improve cardiac contractility and are used in patients with low cardiac output or end-organ dysfunction.<sup>2</sup>  In patients with advanced HF, inotropes may provide symptomatic relief and may be used as bridge therapy in patients awaiting MCS or heart transplantation, or as palliation for those ineligible for either option.<sup>8</sup>  However, prolonged use is associated with increased mortality, primarily due to arrhythmic events.<sup>2,8,10</sup>  ACC/AHA recommendations suggest that inotropes should be considered only for patients with end-organ hypoperfusion and systemic congestion.<sup>8,11</sup> </p>

						<p><em><u>Heart transplant</u></em></p>

						<p>Cardiac transplant improves both long-term survival and QoL in patients with advanced HF. The 1-year survival after heart transplant is approximately 90%, and 50% of patients survive for &gt;11 years.<sup>12,13</sup>  A United Network of Organ Sharing (UNOS) retrospective analysis found that mortality rates in advanced HF heart transplant recipients were lower than for those who remained on the waiting list (<strong>Figure 6</strong>).<sup>13</sup>  Due to the scarcity of donor organs, ACC/AHA guidelines limit heart transplants to select patients who have advanced HF despite guideline-directed medical therapy, devices, and surgical management.<sup>8,13,14</sup></p>

						<div class="figure">
							<h4>Figure 6. Survival of Transplant Candidates Who Did or Did Not Undergo Transplant<sup>2</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-6.png') }}" width="499" height="346" alt="figure-6">

							<h5>Lietz K et al.  <em>J Am Coll Cardiol</em>. 2007;50:1282-1290.</h5>
						</div>

						<p><em><u>MCS</u></em></p>

						<p>Left-ventricular assist devices (LVADs) are a type of guideline-recommended MCS designed to enhance the function of the failing native heart (<strong>Figure 7A</strong>).<sup>8,15</sup>  They may serve as a bridge to cardiac transplant or as a permanent therapeutic option in transplant-ineligible patients.<sup>2,8,15</sup>  More than 50% of patients with advanced HF awaiting cardiac transplant are managed with an MCS.<sup>14</sup>  Analysis of the UNOS database demonstrated that LVADs reduce mortality rates in patients on the heart transplant waiting list (<strong>Figure 7B</strong>).<sup>16</sup></p>

						<div class="figure">
							<h4>Figure 7. (A) Components of a Continuous-flow Left Ventricular Assist Device.<sup>3</sup><br>(B) Survival Probability in Propensity-matched Patients With and Without LVADs on the Heart Transplant Waiting List.<sup>4</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-7a.png') }}" width="344" height="368" alt="figure-7a">

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-7b.png') }}" width="485" height="329" alt="figure-7b">

							<!-- <img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-7ab.png') }}" width="493" height="206" alt="figure-7ab"> -->

							<h5>LVAD, left-ventricular assist device.</h5>

							<h5>3. Pagani FD et al.  <em>J Am Coll Cardiol</em>. 2009;54:312-321. 4. Trivedi JR et al.  <em>Ann Thorac Surg</em>. 2014;98:830-834.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>4. A Team Approach to Advanced HF Management</h2>

						<p>Patients with advanced HF and their caregivers face considerable challenges during the progression of disease. Team-based approaches to care may improve QoL and reduce the burden of care.<sup>17</sup>  In addition to cardiologists, other physicians, and pharmacists, an effective HF treatment team may include nurses, dieticians, physical therapists, exercise physiologists, mental health providers, social workers, and other specialists (<strong>Figure 8</strong>).<sup>17</sup>  HF nurses are responsible for a wide range of interventions that have been shown to have a positive impact on patient self-care, hospital readmissions, and cost. Furthermore, since diet plays an important role in the chronic treatment of patients with HF, a dietitian may aid patient compliance with dietary requirements. Similarly, the likely importance of exercise and lifestyle modification in these patients supports the need for physical therapists or exercise physiologists in their treatment paradigm. Mental health disorders such as depression are common in patients with HF, highlighting the need for mental health providers such as psychologists. Finally, management and coordination of these numerous services and resources may be rendered less complicated for the patient by the involvement of social workers.<sup>17</sup> In addition to improving QoL, multidisciplinary programs may improve patient outcomes. The impact of team-based approaches to care was assessed in a meta-analysis of transitional interventions in patients with HF (including advanced HF) who were discharged from the hospital.<sup>18</sup>  This analysis included 4 trials which assessed the effect of outpatient multidisciplinary care on outcomes and demonstrated that multidisciplinary care reduced 3- to 6-month all-cause rehospitalization and mortality rates.<sup>18</sup></p>

						<div class="figure">
							<h4>Figure 8. Members of a Multidisciplinary Care Team Who Support Patients<br>With Advanced HF<sup>1</sup> </h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-8.png') }}" width="523" height="505" alt="figure-8">

							<h5>1. Cooper LB et al. <em>Heart Fail Clin.</em> 2015;11:499-506.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>5. Shared Decision-Making in Advanced HF</h2>

						<p>Patients with advanced HF may have similar to worse symptom burden than those with late-stage cancer and may benefit from palliative care.<sup>19</sup>  The AHA advocates the inclusion of palliative care into the management of patients with cardiovascular disease.<sup>20,21</sup>   Patients with advanced HF may prioritize improved QoL over prolonged survival, so treatment goals should include both quality and quantity of life.<sup>1</sup>  Moreover, research suggests that early implementation of palliative care has the potential to improve outcomes and QoL in patients with cardiovascular diseases.<sup>20</sup></p>

						<p>A randomized controlled trial assessed the impact of palliative care on outcomes in 60 patients with chronic HF.<sup>22</sup>  Twenty-eight patients received palliative care designed to address their physiologic, social, and psychological needs.<sup>22</sup>  This care including home-based meetings between patients and nurses who used a person-centered palliative care approach. A multidisciplinary team also met twice monthly to discuss each patient&rsquo;s condition and treatment plan.<sup>22</sup>  Patients in the control group (n=32) received usual care provided by physicians and/or nurses at an HF clinic.<sup>22</sup>  After 6 months of follow-up, patients in the palliative care group were significantly more likely to see improvements in NYHA functional class (<em>P</em>=.015) (<strong>Figure 9A</strong>).<sup>22</sup>  Additionally, the mean number of hospitalizations was significantly lower in the palliative care group than the control group (<em>P</em>=.009) (<strong>Figure 9B</strong>).<sup>22</sup>  Palliative therapies work hand-in-hand with traditional therapies designed to improve outcomes (<strong>Figure 10</strong>).<sup>1,23</sup></p>

						<div class="figure">

							<h4>Figure 9. Change in (A) NYHA Functional Class and (B) Mean Number of Hospitalizations in Patients Who Received Palliative Care<sup>1</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-9a.png') }}" width="540" height="379" alt="figure-9a">

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-9b.png') }}" width="540" height="379" alt="figure-9b">

							<h5>1. Brännström M, Boman K. <em>Eur J Heart Fail.</em> 2014;16:1142-1151.</h5>

						</div>

						<div class="figure">
							<h4>Figure 10. Integrating Palliative Care into HF Management<sup>2</sup></h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_09/img/figure-10.png') }}" width="532" height="364" alt="figure-10">

							<h5>2. McIlvennan CK, Allen LA.  <em>BMJ.</em> 2016;352:i1010.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>6. Summary: Determining the Appropriate Treatment Course</h2>

						<p>Patients who progress to advanced HF experience reduced QoL, can become refractory to traditional oral HF therapies, and require complex approaches to care. Some patients with a severe burden of disease value QoL over quantity of life, and management strategies for patients with advanced HF should be personalized to consider each patient&rsquo;s wishes. Effective communication between healthcare providers and patients is critical to ensuring that treatment and management goals are aligned with patient lifestyles and both healthcare provider and patient goals. Moreover, taking a multidisciplinary approach and palliative care measures can help ensure that patients with advanced HF and their support networks receive the care that best suits their individual needs.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Allen LA, Stevenson LW, Grady KL, et al; on behalf of the American Heart Association Council on Quality of Care and Outcomes Research, Council on Cardiovascular Nursing, Council on Clinical Cardiology, Council on Cardiovascular Radiology and Intervention, and Council on Cardiovascular Surgery and Anesthesia. Decision making in advanced heart failure: a scientific statement from the American Heart Association. <em>Circulation</em>. 2012;125:1928-1952.</li>
							<li>Ginwalla M. Home inotropes and other palliative care. <em>Heart Fail Clin</em>. 2016;12:437-448.</li>
							<li>Metra M, Ponikowski P, Dickstein K, et al; on behalf of the Heart Failure Association of the European Society of Cardiology. Advanced chronic heart failure: a position statement from the Study Group on Advanced Heart Failure of the Heart Failure Association of the European Society of Cardiology. <em>Eur J Heart Fail</em>. 2007;9:684-694.</li>
							<li>Dunlay SM, Pereira NL, Kushwaha SS. Contemporary strategies in the diagnosis and management of heart failure. <em>Mayo Clin Proc</em>. 2014;89:662-676.</li>
							<li>Costanzo MR, Mills RM, Wynne J. Characteristics of "Stage D" heart failure: insights from the Acute Decompensated Heart Failure National Registry Longitudinal Module (ADHERE LM). <em>Am Heart J</em>. 2008;155:341-349.</li>
							<li>Chaudhry S-P, Stewart GC. Advanced heart failure: prevalence, natural history, and prognosis. <em>Heart Fail Clin</em>. 2016;12:323-333.</li>
							<li>AbouEzzeddine OF, Redfield MM. Who has advanced heart failure? Definition and epidemiology. <em>Congest Heart Fail</em>. 2011;17:160-168.</li>
							<li>Yancy CW, Jessup M, Bozkurt B, et al. 2013 ACCF/AHA guideline for the management of heart failure: a report of the American College of Cardiology Foundation/American Heart Association Task Force on Practice Guidelines. <em>Circulation</em>. 2013;128:e240-e327.</li>
							<li>Russell SD, Miller LW, Pagani FD. Advanced heart failure: a call to action. <em>Congest Heart Fail</em>. 2008;14:316-321.</li>
							<li>Hauptman PJ, Mikolajczak P, George A, et al. Chronic inotropic therapy in end-stage heart failure. <em>Am Heart J</em>. 2006;152:1096.e1-1096.e8.</li>
							<li>Hashim T, Sanam K, Revilla-Martinez M, et al. Clinical characteristics and outcomes of intravenous inotropic therapy in advanced heart failure. <em>Circ Heart Fail</em>. 2015;8:880-886.</li>
							<li>Mancini D, Lietz K. Selection of cardiac transplantation candidates in 2010. <em>Circulation</em>. 2010;122:173-183.</li>
							<li>Lietz K, Miller LW. Improved survival of patients with end-stage heart failure listed for heart transplantation: analysis of organ procurement and transplantation network/U.S. United Network of Organ Sharing data, 1990 to 2005. <em>J Am Coll Cardiol</em>. 2007;50:1282-1290. </li>
							<li>Lund LH, Edwards LB, Dipchand AI, et al; for the International Society for Heart and Lung Transplantation. The registry of the International Society for Heart and Lung Transplantation: Thirty-third Adult Heart Transplantation Report—2016. Focus theme: primary diagnostic indications for transplant. <em>J Heart Lung Transplant</em>. 2016;35:1158-1169.</li>
							<li>Pagani FD, Miller LW, Russell SD, et al; for the HeartMate II Investigators. Extended mechanical circulatory support with a continuous-flow rotary left ventricular assist device. <em>J Am Coll Cardiol</em>. 2009;54:312-321.</li>
							<li>Trivedi JR, Cheng A, Singh R, Williams ML, Slaughter MS. Survival on the heart transplant waiting list: impact of continuous flow left ventricular assist device as bridge to transplant. <em>Ann Thorac Surg</em>. 2014;98:830-834.</li>
							<li>Cooper LB, Hernandez AF. Assessing the quality and comparative effectiveness of team-based care for heart failure: who, what, where, when, and how. <em>Heart Fail Clin</em>. 2015;11:499-506.   </li>
							<li>Feltner C, Jones CD, Cené CW, et al. Transitional care interventions to prevent readmissions for persons with heart failure: a systematic review and meta-analysis. <em>Ann Intern Med</em>. 2014;160:774-784.</li>
							<li>Bekelman DB, Rumsfeld JS, Havranek EP, et al. Symptom burden, depression, and spiritual well-being: a comparison of heart failure and advanced cancer patients. <em>J Gen Intern Med</em>. 2009;24:592-598.</li>
							<li>American Heart Association/American Stroke Association website. Principles for palliative care (September 2013). Available at: https://www.heart.org/idc/groups/ahaecc-public/@wcm/@adv/documents/downloadable/ucm_467599.pdf. Accessed December 29, 2016.</li>
							<li>Braun LT, Grady KL, Kutner JS, et al. Palliative care and cardiovascular disease and stroke: a policy statement from the American Heart Association/American Stroke Association. <em>Circulation</em>. 2016;134:e198-e225.</li>
							<li>Br&auml;nnstr&ouml;m M, Boman K. Effects of person-centred and integrated chronic heart failure and palliative home care. PREFER: a randomized controlled study. <em>Eur J Heart Fail</em>. 2014;16:1142-1151.</li>
							<li>McIlvennan CK, Allen LA. Palliative care in patients with heart failure. <em>BMJ</em>. 2016;352:i1010.</li>
						</ol>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						<h2>Keywords</h2>

						<ol>
							<li>Advanced heart failure (HF)</li>
							<li>Stage D heart failure (HF)</li>
							<li>Heart transplant</li>
							<li>Left-ventricular assist devices (LVADs)</li>
							<li>Multidisciplinary care in heart failure</li>
							<li>Shared decision-making in heart failure</li>
							<li>Palliative care in heart failure</li>
						</ol>
					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->