<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 3: The Natriuretic Peptide System in Heart Failure</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1.	Synthesis of ANP, BNP, and CNP</a></li>
				<li class="chapter-btn chapter-2"><a>2.	Preclinical data suggest that NPs mediate cardiac effects extending beyond the control of blood pressure and volume</a></li>
				<li class="chapter-btn chapter-3"><a>3.	NPs provide counterregulatory balance to the RAAS and SNS</a></li>
				<li class="chapter-btn chapter-4"><a>4.	The prognostic value of NPs in HF</a></li>
				<li class="chapter-btn chapter-5" metric="completion"><a>5.	Summary</a></li>
				<li class="chapter-btn chapter-6"><a>References</a></li>

			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-3-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>Module 3: The Natriuretic Peptide System in Heart Failure</h1>

						<h2>Module Description</h2>

						<p>Explore the important role of natriuretic peptides in the pathogenesis and diagnosis of heart failure </p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Characterize the role of the natriuretic peptides system in counterbalancing the renin-angiotensin-aldosterone system and sympathetic nervous system in heart failure</p></li>
							<li><p>Discuss the clinical utility of natriuretic peptides as prognostic factors in heart failure</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p>In heart failure (HF), various neurohormonal mechanisms are activated after the index event and initial decline in pumping capacity of the heart occur. These neurohormonal systems include the deleterious effects of overactivity in the renin-angiotensin-aldosterone system (RAAS) and the sympathetic nervous system (SNS), in addition to the compensatory effects of the natriuretic peptides (NPs) system <strong>[Figure 1]</strong>.<sup>1,2</sup></p>

						<div class="figure">
							<h4>Figure 1: NPs represent an emerging target to combat cardiovascular disease</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-1.png') }}" width="594" height="345" alt="figure-1">

							<h5>BP, blood pressure; CAD, coronary artery disease; CO, carbon monoxide; HT, hypertension; SVR, systemic vascular resistance.</h5>

							<h5>Volpe M et al. Natriuretic peptides in cardiovascular diseases: current use and perspectives. <em>Eur Heart J.</em> 2014;35(7):419-425.</h5>
						</div>

						<p>The NPs include atrial NP (ANP); brain-type NP (BNP), which is synthesized primarily in the heart; and C-type NP (CNP), which is synthesized in endothelial cells.<sup>3</sup></p>

						<p>Normally, increases in atrial pressures and ventricular myocardium stretch trigger ANP and BNP release; acting on the kidney and peripheral circulation, ANP and BNP increase the excretion of sodium and water and inhibit the release of renin and aldosterone, thus reducing cardiac load. In patients with HF, however, the effects of NPs are attenuated, leading to unopposed activity of the RAAS and SNS and, in turn, exacerbating HF.<sup>1,4</sup></p>

						<p>NP levels have established utility as a guide to disease monitoring in HF.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">
						
						<h2>1. Synthesis of ANP, BNP, and CNP</h2>

						<p>The NP system predominantly consists of three structurally similar, but genetically and physiologically distinct, endogenous compensatory peptides involved in cardiorenal homeostasis: ANP, BNP, and CNP <strong>[Figure 2]</strong>.<sup>1,5</sup></p>

						<div class="figure">
							<h4>Figure 2: The NP System</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-2.png') }}" width="594" height="275" alt="figure-2">

							<h5>Volpe Clinical Science pg 58 Figure 1. (<strong>citation: (4)</strong> Volpe M, Carnovali M, Mastromarino V. The natriuretic peptides system in the pathophysiology of heart failure: from molecular basis to treatment. <em>Clin Sci (Lond)</em>. 2016;130(2):57-77.)</h5>
						</div>

						<p>ANP and BNP secretion occurs mainly in response to increasing cardiac wall tension; however, other factors, such as neurohormones <nobr>(eg, angiotensin II [Ang II]),</nobr> or changes in renal function play a regulatory role. ANP and BNP are initially synthesized as prohormones and subsequently proteolytically cleaved by corin and furin, respectively, to yield biologically inactive N-terminal fragments <nobr>(NT-ANP and NT-BNP)</nobr> and biologically active peptides <nobr>(ANP and BNP).</nobr> CNP, produced by endothelial cells, is also released as a prohormone that is cleaved into a biologically inactive form <nobr>(NT-CNP)</nobr> and a 22-amino-acid biologically active form: CNP.<sup>1</sup></p>

						<p>ANP, a 28-amino-acid peptide hormone, is produced principally in the cardiac atria and secreted in short bursts in response to acute changes in atrial pressure.<sup>1</sup></p>

						<p>BNP, a 32-amino-acid peptide, is primarily produced in the cardiac ventricle myocytes in response to chronic increases in atrial/ventricular pressure and wall stretch secondary to volume overload. Levels of BNP are usually low but are elevated in vascular disease, suggesting BNP functions as a “cardiac stress hormone” or an emergency cardiac hormone against cardiac overload.<sup>1,2,5</sup> BNP levels are higher in patients with dyspnea secondary to HF than dyspnea triggered by other causes.<sup>6</sup></p>

						<p>CNP is predominantly secreted by the vascular endothelium following stimulation by proinflammatory cytokines, such as interleukin-1 (IL-1) and tumor necrosis factor alpha. Similar to BNP, levels of CNP are low in the absence of disease, such as HF. Acting at both an endothelial and ventricular level, CNP may attenuate cardiac fibrosis, hypertrophy, and left ventricular enlargement, although its precise physiological function remains to be fully understood.<sup>2,6</sup></p>

						<p>The physiological effects of NPs are mediated by the production of the intracellular second messenger cyclic guanosine monophosphate (cGMP), which can be triggered by interaction with specific membrane receptors. NP-A receptors preferentially bind ANP and BNP, and the NP-B receptor preferentially bind CNP <strong>[Figure 3]</strong>.<sup>1,2</sup></p>

						<p>Other NPs include urodilatin (an isoform of ANP) and dendroaspis NP, though less is known about their functions in HF.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 3: NP-binding receptors, intracellular signaling, and degradation processes</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-3.png') }}" width="594" height="440" alt="figure-3">

							<h5>Volpe Clinical Science pg 61 Figure 2. (<strong>citation: (4)</strong> Volpe M, Carnovali M, Mastromarino V. The natriuretic peptides system in the pathophysiology of heart failure: from molecular basis to treatment. <em>Clin Sci (Lond)</em>. 2016;130(2):57-77.)</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>2. Preclinical data suggest that NPs mediate cardiac effects extending beyond the control of blood pressure and volume</h2>

						<p>NPs have been shown to exert vascular effects independent of blood pressure and volume reductions potentially beneficial in patients with HF <strong>[Figure 4]</strong>.</p>

						<div class="figure">
							<h4>Figure 4: The effects of individual NPs on cardiac remodeling</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-4.png') }}" width="594" height="452" alt="figure-4">

							<h5>1. Tokudome T et al. Regulator of G-protein signaling subtype 4 mediates antihypertrophic eﬀect of locally secreted natriuretic peptides in the heart. <em>Circulation.</em> 2008;117(18):2329-2339. 2. Fujita S et al. Atrial natriuretic peptide exerts protective action against angiotensin II-induced cardiac remodeling by attenuating inﬂammation via endothelin-1/endothelin receptor A cascade. <em>Heart Vessels.</em> 2013;28(5):646-657. 3. Calderone A et al. Nitric oxide, atrial natriuretic peptide, and cyclic GMP inhibit the growth-promoting eﬀects of norepinephrine in cardiac myocytes and ﬁbroblasts. <em>J Clin Invest.</em> 1998;101(4):812-818. 4. Kapound AM et al. B-type natriuretic peptide exerts broad functional opposition to transforming growth factor-beta in primary human cardiac ﬁbroblasts: ﬁbrosis, myoﬁbroblast conversion, proliferation, and inﬂammation. <em>Circ Res.</em> 2004;94(4):453-461. 5. Cao L and Gardner DG. Natriuretic peptides inhibit DNA synthesis in cardiac ﬁbroblasts. <em>Hypertension</em>. 1995;25(2):227-234. 6. Kousholt BS. Natriuretic peptides as therapy in cardiac ischaemia/reperfusion. <em>Dan Med J.</em> 2012;59(6):B4469. 7. Hobbs A et al. Natriuretic peptide receptor-C regulates coronary blood ﬂow and prevents myocardial ischemia/reperfusion injury: novel cardioprotective role for endothelium-derived C-type natriuretic peptide. <em>Circulation.</em> 2004;110(10):1231-1235</h5>
						</div>

						<p>NPs act via cardiac guanyl cyclase-A to lower blood pressure, induce diuresis and natriuresis, and promote vasodilation.<sup>7</sup> Preclinical data indicate that the action of NP on this receptor can also yield additional independent benefits by protecting the heart from cardiac hypertrophy.</p>

						<p>Further, preclinical studies reveal that ANP reduced cardiac inflammation and fibrosis, and reversed myocyte hypertrophy without reducing blood pressure, while enhancing systolic and diastolic function.<sup>8</sup> ANP may also counter Ang II-induced cardiac remodeling by blunting inflammation.<sup>8</sup> ANP also has been shown to attenuate norepinephrine-induced growth of cardiac myocytes and fibroblasts, likely through cGMP-mediated inhibition of norepinephrine-induced calcium ion influx, suggesting an important role for ANP in reducing the effects of the SNS on the heart muscle.<sup>2,9</sup></p>

						<p>In an in vitro study, BNP was shown to inhibit transforming growth factor-beta (TGF-β)–induced cardiac fibrosis and to reduce cardiac fibroblast proliferation and TGF-β recruitment of inflammatory mediators (eg, COX2, IL-6), suggesting an important role in the prevention of cardiac fibrosis.<sup>10</sup> Other in vitro studies have also shown that BNP, like ANP, plays a significant role in regulating fibroblast growth during cardiac hypertrophy.<sup>7,11,12</sup></p>

						<p>Preclinical findings suggest that CNP can regulate coronary blood flow and tissue perfusion by reducing infarct size and maintaining coronary perfusion pressure and left ventricular pressure at preischemic levels.<sup>13</sup> In addition, in vivo studies show that CNP can reduce cardiomyocyte hypertrophy and myocardial interstitial fibrosis triggered by Ang II, without affecting systolic blood pressure.<sup>14</sup> In fact, the abundance of CNP in the endothelium is thought to provide a significant protective role within the vasculature by inhibiting proinflammatory responses and smooth muscle proliferation and promoting angiogenesis, in addition to a beneficial role in addressing the consequences of tissue ischemia.<sup>2,15</sup> Further, preclinical data suggest that CNP can prevent cardiac remodeling after a myocardial infarction and can induce potent, dose-dependent relaxation of coronary arteries, in addition to other potential cardiovascular protective actions relevant to HF.<sup>13,16</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>3. NPs provide counterregulatory balance to the RAAS and SNS</h2>

						<p>The RAAS and the SNS function in a mutually reinforcing manner to modulate vascular tone, electrolyte and fluid regulation, and cardiac remodeling and hypertrophy—factors that are important in the pathogenesis of HF. Further, <nobr>Ang II―the</nobr> principal effector hormone of the RAAS―binds with the Ang II type 1 receptor to amplify SNS activity and, with prolonged activation, exacerbates HF pathophysiology.<sup>2,3</sup></p>

						<p>As endogenous compensatory peptides, the NP system acts as a natural antagonist of the RAAS and the SNS <strong>[Figure 5]</strong>. The NP system diminishes renin and aldosterone secretion, resulting in suppression of the RAAS.<sup>17,18</sup> In addition, NPs interfere with autonomic and baroreﬂex control of circulation, blunting the effects of the SNS and producing no significant effects on parasympathetic nerve activity.<sup>19</sup> In HF, NPs act on the kidney and peripheral circulation to unload volume pressure on the heart by increasing the excretion of sodium and water and inhibiting the release of renin and aldosterone.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 5: NPs provide counterregulatory balance to the RAAS and SNS</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-5.png') }}" width="594" height="420" alt="figure-5">

							<h5>Volpe Int J Cardiol 2014, p633, Figure 1. (<strong>citation: (2)</strong> Volpe M. Natriuretic peptides and cardio-renal disease. Int J Cardiol. 2014;176(3):630-639.)</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>4. The prognostic value of NPs in HF</h2>

						<p>The NPs, ANP and BNP, represent potentially useful biomarkers for HF diagnosis, severity, and prognosis, and perhaps for treatment <strong>[Figure 6]</strong>. In both acute and chronic HF, BNP provides important prognostic information, with every <nobr>100 pg/mL</nobr> elevation linked to a 35% increase in the relative risk of death.<sup>20</sup> Normal BNP and ANP plasma levels of approximately 3.5 and <nobr>20 pg/mL</nobr>, respectively, are elevated 10- to 100-fold in patients with HF.<sup>4</sup></p>

						<div class="figure">
							<h4>Figure 6: Conceptual approach for NP-guided HF care</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-6.png') }}" width="594" height="484" alt="figure-6">

							<h5><sup>a</sup>Therapy choices include angiotensin-converting enzyme inhibitors (ACEIs), angiotensin receptor blockers, mineralocorticoid receptor antagonists, beta-blockers, cardiac synchronization therapy, and loop diuretics. Troughton R, Michael Felker G, Januzzi JL Jr. Natriuretic peptide-guided heart failure management. <em>Eur Heart J.</em> 2014;35(1):16-24.</h5>
						</div>

						<p>Highly precise assays are available for the detection of commonly measured NPs, including BNP and its amino-terminal cleavage product, N-proBNP <strong>[Figure 7]</strong>. A midregional pro-ANP assay is also available in the United States for the assessment of ANP, and it appears to deliver results comparable to those for BNP assays in HF.<sup>1</sup> In the emergency department setting, the understanding and assessment of NP has been associated with more rapid diagnosis, lower admission rate, shorter length of hospital stay, and reduced cost.<sup>1</sup></p>

						<div class="figure">
							<h4>Figure 7: Relationship between BNP and NT-proBNP</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-7.png') }}" width="594" height="498" alt="figure-7">

							<h5>Vardeny O et al. First-in-class angiotensin receptor neprilysin inhibitor in heart failure. <em>Clin Pharmacol Ther.</em> 2013;94:445-448.</h5>
						</div>

						<div class="figure inline-figure-right" style="width: 398px;">
							<h4>Figure 8: BNP levels correlate with NYHA class</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-8.png') }}" width="362" height="294" alt="figure-8">

							<h5>Maisel AS, et al. Rapid measurement of B-type natriuretic peptide in the emergency diagnosis of heart failure. <em>N Engl J Med.</em> 2002;347(3):161-167.</h5>
						</div>

						<p>NP levels provide useful prognostic information across all stages of HF. In fact, BNP levels have been shown to correlate with the New York Heart Association (NYHA) HF class <strong>[Figure 8]</strong>.<sup>21</sup> The risk for HF is directly proportional to NP concentration; thus, changes in NP level represent an independent predictor of clinical outcome.<sup>22</sup> Further, NP values independently correlate with risk markers for cardiovascular disease, including age, renal dysfunction, and left ventricular dysfunction.<sup>22</sup> In one study that included patients with HF who presented to an HF clinic, BNP level demonstrated 97% sensitivity and 84% specificity for the diagnosis of HF when BNP values were <nobr>≥22.2 pmol/L—results</nobr> more accurate than those obtained with chest radiographs.<sup>23</sup> Care should be taken to interpret patient BNP test results because of considerable variability among patients with HF. In one study in outpatients with stable chronic HF, 21% of symptomatic patients (NYHA class II–III) demonstrated normal BNP values.<sup>24</sup></p>

						<p>A single NP assessment is prognostically meaningful, but serial follow-up measurements add greater prognostic information.<sup>1</sup> For example, acutely decompensated HF patients who do not have a robust reduction in NT-proBNP or BNP by the time of hospital discharge are at increased risk for morbidity and mortality. Therefore, a 30% or greater decrease in BNP or NT-proBNP is recommended before hospital discharge.</p>

						<p>Further, ambulatory HF patients with chronically elevated or increasing NP concentrations are a high-risk population.<sup>1</sup> Because HF therapies may lower concentrations of BNP and NT-proBNP, the link between NPs and prognostic monitoring has led to their use as a guide to HF therapy. A meta-analysis intended to examine the effects of NP-guided HF therapy on all-cause mortality that included 2000 patients (1006 received NP-guided therapy and the remainder standard care) found a significant reduction in all-cause mortality with NP-guided care in patients younger, but not older, than 75 years <strong>[Figure 9]</strong>.<sup>25</sup> However, in this study, significant reductions in hospitalization due to HF or cardiovascular disease were noted with NP-guided therapy irrespective of age.</p>

						<div class="figure" style="margin-left: 0; margin-right: 0;">
							<h4>Figure 9: NP-guided therapy improves survival for patients aged &lt;75 years</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-9.png') }}" width="616" height="156" alt="figure-9">

							<h5>CI, confidence interval; HR, hazard ratio.</h5>

							<h5>Troughton RW, et al. Effect of B-type natriuretic peptide-guided treatment of chronic heart failure on total mortality and hospitalization: an individual patient meta-analysis. <em>Eur Heart J.</em> 2014;35(23):1559-1567.</h5>
						</div>

						<p>Both American and European clinical guidelines for the management of HF advocate a role for BNP or NT-proBNP in the diagnosis and prognosis of HF, and as a guide to optimal dosing in HF therapy.<sup>26,27</sup></p>

					</div>
				</div>
			</div>

<!-- 			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>5. NPs as a therapeutic target in HF</h2>

						<p>Aberrations in the NP system have been implicated in the pathophysiology of HF.<sup>4</sup> In patients with HF, ANP release in response to acute volume overload or chronic salt loading appears impaired. ANP acts as a counterregulatory hormone that can stem the actions of the SNS and the RAAS.<sup>3</sup> Moreover, the infusion of recombinant human ANP and BNP in patients with HF exerts beneficial hemodynamic effects characterized by decreases in arterial and venous pressures and increases in cardiac output.<sup>1</sup></p>

						<p>Additionally, patients with HF manifest elevated levels of inactive BNP but lower levels of biologically active BNP and its precursors, resulting in a physiologic response that is inadequate to counter deleterious neurohumoral effects on the cardiorenal function characteristic of HF.<sup>2,28</sup> This issue has spurred interest in why this may be. One possibility is due to neprilysin (NEP). NPs are removed from the circulation through enzymatic degradation by NEP, a membrane-bound enzyme expressed mostly in the kidneys.<sup>2</sup> Elevated myocardial NEP mRNA levels in patients with HF imply enhanced NP degradation, supporting the concept that HF may be characterized by a deficiency in biologically active NPs. Thus, boosting NP levels in these patients could be of therapeutic value.</p>

						<p>Current therapeutic approaches in HF primarily focus on targeting the SNS and RAAS systems; however, either directly increasing NP system activity or blocking anti–NP systems may provide beneficial effects in patients with HF in terms of improved sodium excretion and vasodilation <strong>[Figure 11]</strong>.<sup>4</sup> For instance, in patients with decompensated HF requiring hospitalization, infusions of nesiritide, a recombinant BNP, yielded improvements in hemodynamic function and rapid and sustained improvements in clinical status.<sup>29</sup> However, enhancing NP levels through NEP inhibition alone, although a plausible alternative therapeutic strategy, has not yielded favorable clinical results, perhaps since NEP has myriad substrates, some of which can induce pharmacological effects harmful to patients with HF.<sup>4</sup></p>

						<div class="figure">
							<h4>Figure 11: Current therapeutic approaches in HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_03/img/figure-11.png') }}" width="594" height="559" alt="figure-11">

							<h5>Volpe, 2016, pg67, Figure 4. Citation (4) Volpe M, Carnovali M, Mastromarino V. The natriuretic peptides system in the pathophysiology of heart failure: from molecular basis to treatment. <em>Clin Sci (Lond).</em> 2016;130(2):57-77.</h5>
						</div>

						<p>Another strategy for targeting the NP system is the combined use of an NEP inhibitor with an ACEI; indeed, the combined use of ACEIs and NEP inhibitors yielded greater inhibition of bradykinin metabolism and increased bradykinin levels than with ACEIs or NEP inhibitors alone.<sup>4</sup> However, kinin peptides are known to be involved in angioedema, and the combined use of NEP inhibitors and ACEIs may increase the occurrence of angioedema.<sup>4</sup> Furthermore, clinical results have not confirmed a benefit from the combined use of ACEIs and NEP inhibitors.<sup>4</sup></p>

						<p>Recently, a combination angiotensin receptor NEP inhibitor (ARNI), a novel pharmacological approach for bolstering NP and blunting the RAAS, has shown notable promise in the management of HF.<sup>4</sup> Clinical findings suggest that this therapeutic strategy can yield impressive reductions in all-cause mortality, hospitalizations, and clinical progression in patients with HF.<sup>30-32</sup></p>

					</div>
				</div>
			</div> -->

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>5. Summary</h2>

						<p>Over the past several decades there has been a growing recognition of the importance of NPs as endogenous compensatory peptides with key roles in modulating the pathogenesis of HF by counterbalancing the SNS and the RAAS. Consequently, the NP system has emerged as an important biomarker for HF. Since the signs and symptoms of HF are often nonspecific, the use of NP levels has provided an invaluable tool to clinicians for identifying patients experiencing abnormally high cardiac wall stress and increased stretch. In fact, for an untreated patient, a normal NP level could exclude significant cardiac disease, perhaps obviating the requirement for certain diagnostic tests, such as an echocardiogram.<sup>27</sup> </p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Hasenfuss G, Mann DL. Pathophysiology of heart failure. In: Mann DL, Zipes DP, Libby P, eds. <em>Braunwald’s Heart Disease: A Textbook of Cardiovascular Medicine</em>. 10th ed. Philadelphia, PA: Elsevier; 2015.</li>
							
							<li>Volpe M. Natriuretic peptides and cardio-renal disease. <em>Int J Cardiol</em>. 2014;176(3):630-639.</li>
							
							<li>Volpe M, Rubattu S, Burnett J Jr. Natriuretic peptides in cardiovascular diseases: current use and perspectives. <em>Eur Heart J</em>. 2014;35(7):419-425.</li>
							
							<li>Volpe M, Carnovali M, Mastromarino V. The natriuretic peptides system in the pathophysiology of heart failure: from molecular basis to treatment. <em>Clin Sci (Lond)</em>. 2016;130(2):57-77.</li>
							
							<li>McKie PM, Burnett JC Jr. Rationale and therapeutic opportunities for natriuretic peptide system augmentation in heart failure. <em>Curr Heart Fail Rep</em>. 2015;12(1):7-14.</li>
							
							<li>Federico C. Natriuretic Peptide system and cardiovascular disease. <em>Heart Views</em>. 2010;11(1):10-15.</li>
							
							<li>Tokudome T, Kishimoto I, Horio T, et al. Regulator of G-protein signaling subtype 4 mediates antihypertrophic effect of locally secreted natriuretic peptides in the heart. <em>Circulation</em>. 2008;117(18):2329-2339.</li>
							
							<li>Fujita S, Shimojo N, Terasaki F, et al. Atrial natriuretic peptide exerts protective action against angiotensin II-induced cardiac remodeling by attenuating inflammation via endothelin-1/endothelin receptor A cascade. <em>Heart Vessels</em>. 2013;28(5):646-657.</li>
							
							<li>Calderone A, Thaik CM, Takahashi N, Chang DL, Colucci WS. Nitric oxide, atrial natriuretic peptide, and cyclic GMP inhibit the growth-promoting effects of norepinephrine in cardiac myocytes and fibroblasts. <em>J Clin Invest</em>. 1998;101(4):812-818.</li>
							
							<li>Kapoun AM, Liang F, O'Young G, et al. B-type natriuretic peptide exerts broad functional opposition to transforming growth factor-beta in primary human cardiac fibroblasts: fibrosis, myofibroblast conversion, proliferation, and inflammation. <em>Circ Res</em>. 2004;94(4):453-461.</li>
							
							<li>Cao L, Gardner DG. Natriuretic peptides inhibit DNA synthesis in cardiac fibroblasts. <em>Hypertension</em>. 1995;25(2):227-234.</li>
							
							<li>Kousholt BS. Natriuretic peptides as therapy in cardiac ischaemia/reperfusion. <em>Dan Med J</em>. 2012;59(6):B4469.</li>
							
							<li>Hobbs A, Foster P, Prescott C, Scotland R, Ahluwalia A. Natriuretic peptide receptor-C regulates coronary blood flow and prevents myocardial ischemia/reperfusion injury: novel cardioprotective role for endothelium-derived C-type natriuretic peptide. <em>Circulation</em>. 2004;110(10):1231-1235.</li>
							
							<li>Izumiya Y, Araki S, Usuku H, Rokutanda T, Hanatani S, Ogawa H. Chronic C-type natriuretic peptide infusion attenuates angiotensin II-induced myocardial superoxide production and cardiac remodeling. <em>Int J Vasc Med</em>. 2012;2012:246058.</li>
							
							<li>Yamahara K, Itoh H, Chun TH, et al. Significance and therapeutic potential of the natriuretic peptides/cGMP/cGMP-dependent protein kinase pathway in vascular regeneration. <em>Proc Natl Acad Sci U S A</em>. 2003;100(6):3404-3409.</li>
							
							<li>Soeki T, Kishimoto I, Okumura H, et al. C-type natriuretic peptide, a novel antifibrotic and antihypertrophic agent, prevents cardiac remodeling after myocardial infarction. <em>J Am Coll Cardiol</em>. 2005;45(4):608-616.</li>
							
							<li>Atarashi K, Mulrow PJ, Franco-Saenz R. Effect of atrial peptides on aldosterone production. <em>J Clin Invest</em>. 1985;76(5):1807-1811.</li>
							
							<li>Burnett JC, Jr., Granger JP, Opgenorth TJ. Effects of synthetic atrial natriuretic factor on renal function and renin release. <em>Am J Physiol</em>. 1984;247(5 Pt 2):F863-866.</li>
							
							<li>Luchner A, Schunkert H. Interactions between the sympathetic nervous system and the cardiac natriuretic peptide system. <em>Cardiovasc Res</em>. 2004;63(3):443-449.</li>
							
							<li>Doust JA, Pietrzak E, Dobson A, Glasziou P. How well does B-type natriuretic peptide predict death and cardiac events in patients with heart failure: systematic review. <em>BMJ</em>. 2005;330(7492):625.</li>
							
							<li>Maisel AS, Krishnaswamy P, Nowak RM, et al. Rapid measurement of B-type natriuretic peptide in the emergency diagnosis of heart failure. <em>N Engl J Med</em>. 2002;347(3):161-167.</li>
							
							<li>Troughton R, Michael Felker G, Januzzi JL Jr. Natriuretic peptide-guided heart failure management. <em>Eur Heart J</em>. 2014;35(1):16-24.</li>
							
							<li>Cowie MR, Struthers AD, Wood DA, et al. Value of natriuretic peptides in assessment of patients with possible new heart failure in primary care. <em>Lancet</em>. 1997;350(9088):1349-1353.</li>
							
							<li>Tang WH, Girod JP, Lee MJ, et al. Plasma B-type natriuretic peptide levels in ambulatory patients with established chronic symptomatic systolic heart failure. <em>Circulation</em>. 2003;108(24):2964-2966.</li>
							
							<li>Troughton RW, Frampton CM, Brunner-La Rocca HP, et al. Effect of B-type natriuretic peptide-guided treatment of chronic heart failure on total mortality and hospitalization: an individual patient meta-analysis. <em>Eur Heart J</em>. 2014;35(23):1559-1567.</li>
							
							<li>Yancy CW, Jessup M, Bozkurt B, et al. 2013 ACCF/AHA guideline for the management of heart failure: executive summary: a report of the American College of Cardiology Foundation/American Heart Association Task Force on practice guidelines. <em>Circulation</em>. 2013;128(16):1810-1852.</li>

							<li>McMurray JJ, Adamopoulos S, Anker SD, et al. ESC guidelines for the diagnosis and treatment of acute and chronic heart failure 2012: The Task Force for the Diagnosis and Treatment of Acute and Chronic Heart Failure 2012 of the European Society of Cardiology. Developed in collaboration with the Heart Failure Association (HFA) of the ESC. <em>Eur J Heart Fail</em>. 2012;14(8):803-869.</li>
							
							<li>Niederkofler EE, Kiernan UA, O'Rear J, et al. Detection of endogenous B-type natriuretic peptide at very low concentrations in patients with heart failure. <em>Circ Heart Fail.</em> 2008;1(4):258-264.</li>
							
							<li>Colucci WS, Elkayam U, Horton DP, et al. Intravenous nesiritide, a natriuretic peptide, in the treatment of decompensated congestive heart failure. Nesiritide Study Group. <em>N Engl J Med</em>. 2000;343(4):246-253.</li>
							
							<li>Packer M, McMurray JJ, Desai AS, et al. Angiotensin receptor neprilysin inhibition compared with enalapril on the risk of clinical progression in surviving patients with heart failure. <em>Circulation</em>. 2015;131(1):54-61.</li>
							
							<li>McMurray J, Packer M, Desai A, et al. A putative placebo analysis of the effects of LCZ696 on clinical outcomes in heart failure. <em>Eur Heart J</em>. 2015;36(7):434-439.</li>

							<li>McMurray J, Packer M, Desai A, et al. Angiotensin-neprilysin inhibition versus enalapril in heart failure. <em>N Engl J Med</em>. 2014;371(11):993-1004.</li>
						</ol>

					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->