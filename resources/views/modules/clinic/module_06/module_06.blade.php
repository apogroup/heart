<div class="module-wrapper">

	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 6: Bradykinin: The Double-Edged Sword</h3>
			
			<ul class="module-menu-buttons">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1. Bradykinin Synthesis, Localization, and Metabolism</a></li>
				<li class="chapter-btn chapter-2"><a>2. The Detrimental and Beneficial Actions of Bradykinin</a></li>
				<li class="chapter-btn chapter-3"><a>3. Role of Bradykinin as an Inflammatory Mediator</a></li>
				<li class="chapter-btn chapter-4"><a>4. Cardioprotective Effects of Bradykinin</a></li>
				<li class="chapter-btn chapter-5"><a>5. Bradykinin as a Therapeutic Target in Heart Failure</a></li>
				<li class="chapter-btn chapter-6" metric="completion"><a>6. Summary</a></li>
				<li class="chapter-btn chapter-7"><a>References</a></li>
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-6-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h1>Module 6: Bradykinin: The Double-Edged Sword</h1>

						<h2>Module Description</h2>

						<p>Learn how bradykinin acts as a &ldquo;double-edged sword&rdquo; in the pathophysiology of heart failure by mediating both cardioprotective and inflammatory mechanisms.</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>Characterize the cardioprotective and pro-inflammatory roles of bradykinin in heart failure (HF)</p></li>
							<li><p>Provide a mechanistic rationale for the cardioprotective effects (via bradykinin) of angiotensin-converting enzyme and neprilysin inhibition in HF</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p>The kallikrein-kinin system (KKS) is complex and is comprised of enzymes (eg, tissue and plasma kallikreins), protein precursors (eg, kininogens), and potent vasoactive peptides known as kinins (eg, bradykinin and kallidin) <strong>[Figure 1]</strong>.<sup>1-3</sup> KKS activation plays important physiologic roles in the regulation of blood pressure and inflammatory responses.<sup>2</sup> These actions occur through the biologic effects of bradykinin, which has been shown to play a dual role in normal physiology and various disease states, including heart failure (HF).<sup>1,4</sup> For example, bradykinin exerts numerous cardioprotective effects in HF, such as reducing left ventricular hypertrophy and the progression of HF.<sup>5,6</sup> Given that bradykinin peptides are metabolized by both angiotensin-converting enzyme (ACE) and neprilysin, the inhibition of ACE and neprilysin (also called neutral endopeptidase) has been shown to increase bradykinin levels.<sup>7,8</sup> This suggests that the beneficial effects of ACE and neprilysin inhibitors are due in part to the augmentation of bradykinin levels.</p>
						
						<div class="figure">
							<h4>Figure 1: Overview of the Kallikrein-Kinin System</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-1.png') }}" width="594" height="435" alt="figure-1">
							<h5>Bas M, Adams V, Suvorava T, et al. Nonallergic angioedema: role of bradykinin. <em>Allergy</em>. 2007;62:842&ndash;856.</h5>
						</div>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">
						
						<h2>1. Bradykinin Synthesis, Localization, and Metabolism</h2>

						<p>Bradykinin, which is also known as BK-(1-9), belongs to a family of short-lived, potent endogenous compensatory peptides that are predominantly synthesized by the proteolytic actions of kallikreins on inactive protein precursors known as kininogens via 2 pathways.<sup>2,7-9</sup> Plasma kallikreins act on high-molecular-weight kininogens to generate BK-(1-9), while tissue kallikreins act on low-molecular-weight kininogens to generate Lys<sup>0</sup>-BK-(1-9), which is also known as kallidin. Lys<sup>0</sup>-BK-(1-9) can be converted into BK-(1-9)through the actions of aminopeptidases <strong>[Figure 2]</strong>.<sup>2,7-9</sup></p>

						<div class="figure">
							<h4>Figure 2: Generation of Kinin Peptides by Tissue and Plasma Kallikrein</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-2.png') }}" width="594" height="270" alt="figure-2">

							<h5>Campbell DJ. Towards understanding the kallikrein-kinin system: insights from measurement of kinin peptides. <em>Braz J Med Biol Res</em>. 2000;33:665&ndash;677.</h5>
						</div>

						<p>In normal rats, kinin peptides are predominantly localized to tissues (eg, blood vessels, adipose tissue, adrenal glands, kidneys, lungs, brain), with very low circulating levels of kinin peptides <strong>[Figure 3]</strong>.<sup>10</sup> Similarly, blood levels of the kinin peptides, bradykinin and kallidin, are also low in humans.<sup>11</sup> Furthermore, kinin peptide levels are higher in venous than arterial blood, which is consistent with local tissue production of kinins.<sup>11</sup></p>

						<div class="figure">
							<h4>Figure 3: Concentrations of Bradykinin 1-7, 1-8, and 1-9 in Different Tissues of Rats</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-3.png') }}" width="594" height="430" alt="figure-3">

							<h5>Data are mean &plusmn; SEM, n=6.</h5>
							<h5>Campbell DJ, Kladis A, Duncan A-M. Bradykinin peptides in kidney, blood, and other tissues of the rat. <em>Hypertension</em>. 1993;21:155&ndash;165.</h5>
						</div>

						<p>Bradykinin may be further metabolized by enzymes known as kininases, which play an important role in regulating the biologic and pathologic effects of bradykinin <strong>[Figure 4]</strong>.<sup>8</sup> Carboxypeptidase (also known as kininase I) converts bradykinin to the bioactive metabolite BK-(1-8).<sup>8</sup> In contrast, ACE and neprilysin both convert bradykinin to inactive degradation products.<sup>8</sup></p>

						<div class="figure">
							<h4>Figure 4: Metabolism of Kinin Peptides by Kininases and Kinin Biologic Targets</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-4.png') }}" width="594" height="406" alt="figure-4">

							<h5>ACE, angiotensin-converting enzyme.</h5>
							<h5>Campbell DJ. Towards understanding the kallikrein-kinin system: insights from measurement of kinin peptides. <em>Braz J Med Biol Res</em>. 2000;33:665&ndash;677.</h5>
						</div>
					
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>2. The Detrimental and Beneficial Actions of Bradykinin</h2>

						<p>Bradykinin is a potent vasodilator that is involved in mediating both inflammatory and cardioprotective effects, thus playing important roles in a number of pathophysiologic processes, including the regulation of blood pressure, renal and cardiovascular function, and inflammation.<sup>2,12,13</sup></p>

						<p>Bradykinin exerts its biologic effects via two transmembrane G protein&ndash;coupled receptors: bradykinin B<sub>1</sub> receptor (B<sub>1</sub>) and bradykinin B<sub>2</sub> receptor (B<sub>2</sub>).<sup>2</sup> B<sub>1</sub> receptor expression is thought to be inducible under pro-inflammatory conditions (eg, during tissue injury or in the presence of cytokines and endotoxins).<sup>2</sup> In contrast, B<sub>2</sub> receptors are constitutively expressed in many tissues, and are believed to be the main mediator of both the pro-inflammatory and cardioprotective effects of bradykinin <strong>[Figure 5]</strong>.<sup>2,3</sup> For example, the binding of bradykinin to B<sub>2</sub> receptors on endothelial cells triggers the release of nitric oxide (NO), prostacyclin, and hyperpolarizing factor, resulting in vasodilation and increased vascular permeability.<sup>12</sup></p>

						<div class="figure">
							<h4>Figure 5: Detrimental and Beneficial Effects of Activation of the B<sub>2</sub> Receptor in Humans</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-5.png') }}" width="594" height="533" alt="figure-5">

							<h5>tPA, tissue plasminogen activator.</h5>
							<h5>Bas M, Adams V, Suvorava T, et al. Nonallergic angioedema: role of bradykinin. <em>Allergy</em>. 2007;62:842&ndash;856.</h5>
						</div>

						<p>The cardioprotective effects of bradykinin include stimulation of endothelial NO and tissue plasminogen activator (tPA) release and inhibition of collagen accumulation.<sup>3,12,14</sup> However, the vasodilatory effects of bradykinin may promote vascular permeability, which can potentially result in the development of angioedema.<sup>3</sup> Furthermore, the accumulation of bradykinin may also result in the activation of pro-inflammatory factors, and may play a role in diseases with a strong immune component (eg, rheumatoid arthritis, multiple sclerosis, diabetes).<sup>2</sup> Therefore, bradykinin is considered a &ldquo;double-edged sword&rdquo; because it plays a dual role in both normal physiology and various disease states <strong>[Figure 6]</strong><sup>.3,12</sup> For example, the beneficial effects of ACE inhibitor therapy in HF that are attributable to endogenous bradykinin may be offset by the potential risk of bradykinin-induced angioedema.<sup>3,15</sup></p>

						<div class="figure">
							<h4>Figure 6: Effects of Bradykinin on Various Pathophysiologic Processes</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-6.png') }}" width="594" height="403" alt="figure-6">

							<h5>Maurer M, Bader M, Bas M, et al. New topics in bradykinin research. <em>Allergy</em>. 2011;66:1397&ndash;1406.</h5>
							<h5></h5>
						</div>



					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>3. Role of Bradykinin as an Inflammatory Mediator</h2>

						<p>Bradykinin is a potent mediator of acute and chronic inflammatory processes that can induce vascular permeability and immune responses (eg, the production of interleukin 1 and tumor necrosis factor alpha).<sup>12</sup> The injection of bradykinin into the skin of animal models and humans has been shown to induce acute signs of inflammation (eg, redness, swelling, heat, and pain) and increase vascular permeability, which appears to be mediated by B<sub>2</sub> receptors <strong>[Figure 7]</strong>.<sup>1-3,12</sup> Bradykinin is also thought to be involved in chronic inflammatory processes, which may be regulated by bradykinin&rsquo;s interaction with B<sub>1</sub> receptors.<sup>2</sup></p>

						<div class="figure">
							<h4>Figure 7: Bradykinin Can Cause Inflammation and Edema</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-7.png') }}" width="594" height="334" alt="figure-7">

							<h5>HMWK, high-molecular-weight kininogen.</h5>
							<h5>Bas M, Adams V, Suvorava T, et al. Nonallergic angioedema: role of bradykinin. <em>Allergy</em>. 2007;62:842&ndash;856.</h5>
						</div>

						<p>Substantially elevated levels of plasma bradykinin during acute angioedema attacks<sup>16</sup> suggests that bradykinin plays a role in the indirect and direct development of angioedema due to excessive vascular permeability.<sup>3</sup> In particular, angioedema is a potentially life-threatening adverse reaction associated with ACE inhibitor therapy <strong>[Figure 8]</strong>.<sup>1</sup> The prolonged use of ACE inhibitors to treat cardiovascular disorders such as HF increases bradykinin plasma levels and their biologic activity by decreasing the degradation of these peptides.<sup>3</sup> Additionally, treatment with the ACE-neprilysin inhibitor omapatrilat has been shown to further increase the incidence of angioedema relative to ACE inhibitor therapy alone.<sup>17</sup></p>

						<div class="figure">
							<h4>Figure 8: Example of a Patient with ACE Inhibitor-Related Angioedema</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-8.png') }}" width="390" height="570" alt="figure-8">

							<h5>Bramante RM, Rand M. Angioedema. <em>N Engl J Med</em>. 2011;365:e4.</h5>

						</div>

						<p>Bradykinin&rsquo;s involvement in inflammatory processes and its ability to induce vascular permeability may also contribute to various disease states, including neurologic disorders (eg, Alzheimer&rsquo;s disease, traumatic brain injury), respiratory diseases (eg, asthma, allergic rhinitis), type 1 diabetes, cancer (eg, increased permeability of tumors), arthritis, sepsis, and pain <strong>[Figure 9]</strong>.<sup>1,12</sup></p>

						<div class="figure">
							<h4>Figure 9: Inflammatory Disease States Associated with Elevated Bradykinin Activity Via Elevated Vascular Permeability</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-9.png') }}" width="594" height="312" alt="figure-9">

							<h5>Sharma JN, AL-Sherif GJ. The kinin system: present and future pharmacological targets. <em>Am J Biomed Sci</em>. 2011;3:156&ndash;169.</h5>
							<h5>Maurer M, Bader M, Bas M, et al. New topics in bradykinin research. <em>Allergy</em>. 2011;66:1397&ndash;1406.</h5>
						</div>



					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>4. Cardioprotective Effects of Bradykinin</h2>

						<p>The cardiovascular actions of bradykinin are thought to be mainly mediated through activation of B<sub>2</sub> receptors on endothelial cells, although B<sub>1</sub> receptor upregulation under inflammatory conditions may also induce vasodilation and hypotension.<sup>1</sup> The activation of endothelial B<sub>2</sub> receptors results in elevated intracellular calcium and the increased production of the potent vasodilatory mediators NO, prostacyclin (also known as prostaglandin I2), and endothelium- derived hyperpolarization factor, which act on vascular smooth muscles and result in the induction of vasodilation and coronary dilatation <strong>[Figure 10]</strong>.<sup>1,3,12</sup></p>

						<div class="figure">
							<h4>Figure 10: Role of Bradykinin on Vasodilation Via Activation of Calcium and NO Signaling</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-10.png') }}" width="594" height="526" alt="figure-10">

							<h5>Bas M, Adams V, Suvorava T, et al. Nonallergic angioedema: role of bradykinin. <em>Allergy</em>. 2007;62:842&ndash;856.</h5>
							<h5></h5>
						</div>

						<p>The additional cardioprotective effects of bradykinin include its antithrombotic and profibrinolytic properties.<sup>3,18</sup> Animal studies have demonstrated the relationship between NO and prostacyclin release and the antithrombotic effect of ACE inhibitors.<sup>19,20</sup> Studies in both animals and healthy volunteers have also shown bradykinin to be a powerful stimulator of tPA release from the endothelium.<sup>21,22</sup> Furthermore, ACE inhibition potentiates bradykinin-induced tPA release and activity, which may in part explain the vascular benefits of ACE inhibitors.<sup>22</sup></p>

						<p>Bradykinin has also been shown to offer cardioprotection through its ability to inhibit pathologic growth and angiotensin II-induced hypertrophy. Although bradykinin was shown to have a hypertrophic effect on isolated ventricular cardiomyocytes in rats, endothelial cells blocked hypertrophy due to bradykinin, abolishing angiotensin II-induced hypertrophy of left ventricular cardiomyocytes.<sup>23</sup> Knockout studies in mice indicate that the B<sub>2</sub> receptor may be involved in the regulation of ventricular hypertrophy and cardiomyocyte enlargement.<sup>24</sup> Indeed, B<sub>2</sub> receptor gene polymorphisms were associated with left ventricular hypertrophy in Japanese patients with essential hypertension.<sup>25</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>5. Bradykinin as a Therapeutic Target in Heart Failure</h2>

						<p>The renin-angiotensin-aldosterone-system (RAAS), sympathetic nervous system (SNS), and endogenous compensatory peptides (eg, bradykinin) play important roles in the maintenance of healthy heart function through numerous effects on contractility, heart rate, arterial pressure, and blood volume.<sup>7</sup> In the setting of HF, a state of neurohumoral imbalance can result in overexpression of the RAAS and SNS <strong>[Figure 11]</strong>.<sup>7,26</sup> Therefore, a major goal of HF therapy is to reduce this imbalance.</p>

						<div class="figure">
							<h4>Figure 11: The Role of Neurohumoral Balance in HF</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-11.png') }}" width="594" height="349" alt="figure-11">

							<h5>McMurray JJV. Neprilysin inhibition to treat heart failure: a tale of science, serendipity, and second chances. <em>Eur J Heart Fail</em>. 2015;17:242&ndash;247.</h5>

						</div>

						<p>The membrane-bound endopeptidase neprilysin metabolizes natriuretic peptides and other vasoactive peptides, including bradykinin.<sup>26</sup> ACE is one of the enzymes responsible for the degradation of endogenous bradykinin to inactive products and the generation of the potent vasoconstrictor angiotensin II, which exerts its vascular effects by binding to angiotensin II type 1 receptors.<sup>7,27</sup> As a result, ACE inhibition prevents the formation of angiotensin II while increasing the accumulation of bradykinin. This in turn increases the production of NO, which is thought to mediate the vasodilatory effects of bradykinin.<sup>27</sup> Indeed, bradykinin has been shown to contribute to the systemic hemodynamic effects of ACE inhibition in healthy volunteers and in patients with HF receiving long-term ACE inhibitor therapy.<sup>13,15,28</sup> Therefore, this mechanism may explain the apparent clinical differences between ACE inhibitors and angiotensin II receptor blockers (ARBs) in the treatment of HF <strong>[Figure 12]</strong>.<sup>15,27</sup></p>

						<div class="figure">
							<h4>Figure 12: Dual Role of the ACE and Sites of Action of Antagonizing Agents</h4>

							<img src="{{ asset('assets/css/img/modules/clinic/module_06/img/figure-12.png') }}" width="594" height="399" alt="figure-12">

							<h5>ACE, angiotensin-converting enzyme; ARB, angiotensin II receptor blocker; RAAS, renin-angiotensin-aldosterone system.</h5>
							<h5>Wiggins KJ, Kelly DJ. Aliskiren: a novel renoprotective agent or simply an alternative to ACE inhibitors? <em>Int Soc Nephrol</em>. 2009;76:23&ndash;31.</h5>
						</div>



					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>6. Summary</h2>

						<p>In summary, bradykinin is a potent endogenous compensatory peptide that plays an important role in many pathophysiologic processes, including vasodilation and inflammation. Bradykinin is considered a &ldquo;double-edged sword&rdquo; in HF because it contributes to the cardioprotective effects of ACE inhibitor therapy while increasing the risk of angioedema due to increased vascular permeability. The cardiovascular actions of bradykinin are mediated by B<sub>2</sub> receptors on endothelial cells, which stimulate the release of multiple factors that promote vasodilation and fibrinolysis while inhibiting thrombosis and pathological growth. Bradykinin is degraded by neprilysin, ACE, DPP-IV, and APP, and ACE also regulates the formation of angiotensin II.</p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Sharma JN, AL-Sherif GJ. The kinin system: present and future pharmacological targets. <em>Am J Biomed Sci</em>. 2011;3:156&ndash;169.</li>
							<li>Golias Ch, Charalabopoulos A, Stagikas D, et al. The kinin system&ndash;bradykinin: biological effects and clinical implications. Multiple role of the kinin system &ndash; bradykinin. <em>Hippokratia</em>. 2007;11:124&ndash;128.</li>
							<li>Bas M, Adams V, Suvorava T, et al. Nonallergic angioedema: role of bradykinin. <em>Allergy</em>. 2007;62:842&ndash;856.</li>
							<li>Costa-Neto CM, Dillenburg-Pilla P, Heinrich TA, et al. Participation of kallikrein&ndash;kinin system in different pathologies. <em>Int Immunopharmacol</em>. 2008;8:135&ndash;142.</li>
							<li>Liu Y-H, Yang X-P, Sharov VG, et al. Effects of angiotensin-converting enzyme inhibitors and angiotensin II type 1 receptor antagonists in rats with heart failure: role of kinins and angiotensin II type 2 receptors. <em>J Clin Invest</em>. 1997;99:1926&ndash;1935.</li>
							<li>McDonald KM, Mock J, D&rsquo;Aloia A, et al. Bradykinin antagonism inhibits the antigrowth effect of converting enzyme inhibition in the dog myocardium after discrete transmural myocardial necrosis. <em>Circulation</em>. 1995;91:2043&ndash;2048.</li>
							<li>Mann DL. Pathophysiology of heart failure. In: Mann DL, Zipes DP, Libby P, et al, eds. <em>Braunwald&rsquo;s Heart Disease: A Textbook of Cardiovascular Medicine</em>. 10th edition. Philadelphia, PA: Elsevier Saunders; 2015.</li>
							<li>Campbell DJ. Towards understanding the kallikrein-kinin system: insights from measurement of kinin peptides. <em>Braz J Med Biol Res</em>. 2000;33:665&ndash;677.</li>
							<li>Campbell DJ. The kallikrein&ndash;kinin system in humans. <em>Clin Exp Pharmacol Physiol</em>. 2001;28:1060&ndash;1065.</li>
							<li>Campbell DJ, Kladis A, Duncan A-M. Bradykinin peptides in kidney, blood, and other tissues of the rat. <em>Hypertension</em>. 1993;21:155&ndash;165.</li>
							<li>Duncan A-M, Kladis A, Jennings GL, et al. Kinins in humans. <em>Am J Physiol Regul Integr Comp Physiol</em>. 2000;278:R897&ndash;R904.</li>
							<li>Maurer M, Bader M, Bas M, et al. New topics in bradykinin research. <em>Allergy</em>. 2011;66:1397&ndash;1406.</li>
							<li>Hornig B, Kohler C, Drexler H. Role of bradykinin in mediating vascular effects of angiotensin-converting enzyme inhibitors in humans. <em>Circulation</em>. 1997;95:1115&ndash;1118.</li>
							<li>Unger T, Li J. The role of the renin-angiotensin-aldosterone system in heart failure. <em>J Renin Angiotensin Aldosterone Syst</em>. 2004;5(suppl 1):S7&ndash;S10.</li>
							<li>Cruden NLM, Witherow FN, Webb DJ, et al. Bradykinin contributes to the systemic hemodynamic effects of chronic angiotensin-converting enzyme inhibition in patients with heart failure. <em>Arterioscler Thromb Vasc Biol</em>. 2004;24:1043&ndash;1048.</li>
							<li>Nussberger J, Cugno M, Amstutz C, et al. Plasma bradykinin in angio-oedema. <em>Lancet</em>. 1998;351:1693&ndash;1697.</li>
							<li>Kostis JB, Packer M, Black HR, et al. Omapatrilat and enalapril in patients with hypertension: the Omapatrilat Cardiovascular Treatment vs. Enalapril (OCTAVE) trial. <em>Am J Hypertens</em>. 2004;17:103&ndash;111.</li>
							<li>Buczko W, Kramkowski K, Mogielnicki A. Are the endothelial mechanisms of ACE-Is already established? <em>Pharmacological Rep</em>. 2006;58(suppl):126&ndash;131.</li>
							<li>Pawlak R, Chabielska E, Golatowski J, et al. Nitric oxide and prostacyclin are involved in antithrombotic action of captopril in venous thrombosis in rats. <em>Thromb Haemost</em>. 1998;79:1208&ndash;1212.</li>
							<li>Chabielska E, Pawlak R, Golatowski J, et al. The antithrombotic effect of captopril and losartan on experimental arterial thrombosis in rats. <em>J Physiol Pharmacol</em>. 1998;49:251&ndash;260.</li>
							<li>Smith D, Gilbert M, Owen WG. Tissue plasminogen activator release in vivo in response to vasoactive agents. <em>Blood</em>. 1985;66:835&ndash;839.</li>
							<li>Labinjoh C, Newby DE, Pellegrini MP, et al. Potentiation of bradykinin-induced tissue plasminogen activator release by angiotensin- converting enzyme inhibition. <em>J Am Coll Cardiol</em>. 2001;38:1402&ndash;1408.</li>
							<li>Ritchie RH, Marsh JD, Lancaster WD, et al. Bradykinin blocks angiotensin II-induced hypertrophy in the presence of endothelial cells. <em>Hypertension</em>. 1998;31:39&ndash;44.</li>
							<li>Maestri R, Milia AF, Salis MB, et al. Cardiac hypertrophy and microvascular deficit in kinin B<sub>2</sub> receptor knockout mice. <em>Hypertension</em>. 2003;41:1151&ndash;1155.</li>
							<li>Fu Y, Katsuya T, Matsuo A, et al. Relationship of bradykinin B<sub>2</sub> receptor gene polymorphism with essential hypertension and left ventricular hypertrophy. <em>Hypertens Res</em>. 2004;27:933&ndash;938.</li>
							<li>McMurray JJV. Neprilysin inhibition to treat heart failure: a tale of science, serendipity, and second chances. <em>Eur J Heart Fail</em>. 2015;17:242&ndash;247.</li>
							<li>Wiggins KJ, Kelly DJ. Aliskiren: a novel renoprotective agent or simply an alternative to ACE inhibitors? <em>Int Soc Nephrol</em>. 2009;76:23&ndash;31.</li>
							<li>Witherow FN, Helmy A, Webb DJ, et al. Bradykinin contributes to the vascular effects of chronic angiotensin-converting enzyme inhibition in patients with heart failure. <em>Circulation</em>. 2001;104:2177&ndash;2181.</li>
						</ol>

					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->