<div class="module-wrapper">
				
	<div class="modal-sidebar">
		<div class="modal-sidebar-content">

			<h3 class="module-home-btn">Module 2: Neurohormonal Control of Heart Failure: The Role of Endogenous Compensatory Peptides</h3>
			
			<ul class="module-menu-buttons smalltext-list">
				<li class="chapter-btn chapter-0"><a>Module Introduction</a></li>
				<li class="chapter-btn chapter-1"><a>1.	Neurohormonal mechanisms of HF</a></li>
				<li class="chapter-btn chapter-2"><a>2.	Endogenous compensatory peptides exhibit counterregulatory effects on RAAS and SNS</a></li>
				<li class="chapter-btn chapter-3"><a>3.	Many endogenous compensatory peptides exert effects via activation of cyclic guanosine monophosphate (cGMP)</a></li>
				<li class="chapter-btn chapter-4"><a>4.	Endogenous compensatory peptides are degraded by the enzyme neprilysin</a></li>
				<li class="chapter-btn chapter-5"><a>5.	Some endogenous compensatory peptides have utility as prognostic factors or biomarkers in heart failure</a></li>
				<li class="chapter-btn chapter-6" metric="completion"><a>6. Summary</a></li>
				<li class="chapter-btn chapter-7"><a>References</a></li>
			</ul>

		</div>
	</div>

	<div class="container">
		
		<div class="gallery-page-status"><p>PAGE</p><p class="gallery-status"></p></div>
		
		<div class="gallery" id="module-2-gallery">

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h1>Module 2: Neurohormonal Control of Heart Failure: The Role of Endogenous Compensatory Peptides</h1>

						<h2>Module Description</h2>

						<p>The SNS and RAAS are not the only key neurohormonal regulators in the development of heart failure. Learn about endogenous compensatory peptides and why are they are important in heart failure.</p>

						<h2>Objectives</h2>

						<ul>
							<li><p>To introduce endogenous compensatory peptides and their roles in heart failure (HF)</p></li>

							<li><p>To set the stage for future modules that provide more granularity about the function of individual endogenous compensatory peptides</p></li>
						</ul>
						
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>Module Introduction</h2>

						<p>Once initiated by an “index event,” such as ischemia or myocardial infarction, weakening or loss of myocardium function progressively leads to declining pumping capacity <strong>[Figure 1]</strong>.<sup>1,2</sup> </p>

						<p>In response, the cardiovascular system can adapt regulatory pathways to maintain cardiac output and reduce the accumulation of fluid and sodium in end organs.<sup>1</sup> Over the long-term, however, this compensatory signaling by cytokines and neurohormones leads to LV remodeling and heart failure (HF).<sup>1</sup></p>  

						<p>As described in the previous module, patients with HF are at high risk of sudden cardiac death (SCD), regardless of the severity of symptoms present in the patient.<sup>3</sup></p> 

						<p>For all patients with HF, North American and European guidelines recommend modulating the renin-angiotensin-aldosterone system (RAAS) with angiotensin-converting enzyme (ACE) inhibitors while concurrently modulating the sympathetic nervous system with beta-blockers.<sup>2,4</sup> There have been small, but significant, improvements in 5-year survival in patients with HFrEF despite increased availability of new therapeutic approaches since 1987.<sup>5</sup></p> 
						
						<div class="figure">
							<h4>Figure 1: Normal Heart vs Heart Failure</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-1.png') }}" width="594" height="347" alt="figure-1">
						</div>
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">
						
						<h2>1. Neurohormonal Mechanisms of HF </h2>

						<p>After an index event, the cardiovascular system responds by initiating compensatory pathways to maintain cardiac output and homeostasis.<sup>1</sup> Over time, however, these compensatory pathways can increase stiffening in the myocardium, impairing contractility, and progress into HF <strong>[Figure 2]</strong>.<sup>1,6</sup></p> 

						<div class="figure">
							<h4>Figure 2: Relationship between index event and HF</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-2.png') }}" width="594" height="488" alt="figure-2">
							<h5>ANP, atrial natriuretic peptide; BNP, brain natriuretic peptide; LV, left ventricular;</h5>
							<h5>NOS, nitric oxide synthase; RAS, renin-angiotensin system; ROS, reactive oxygen</h5>
							<h5>Hasenfuss G, Mann DL. Pathophysiology of heart failure. In: Mann DL et al, eds.</h5>
							<h5><em>Braunwald's Heart Disease: A Textbook of Cardiovascular Medicine</em>. 10th ed. Philadelphia, PA: Elsevier; 2015.</h5>
						</div>

						<p>Thus far, the neurohormonal mechanisms that have been primarily focused on in HF are the SNS and RAAS. The balance of the sympathetic and parasympathetic nervous systems become altered early in the course of HF <strong>[Figure 3]</strong>.<sup>1</sup> SNS activation leads to increases in heart rate, the force of myocardial contraction, cardiac output, and peripheral arterial vasoconstriction, while the concomitant withdrawal of the parasympathetic nervous system activation can decrease nitric oxide (NO) production, increased inflammation, and worsening left ventricular remodeling.<sup>1</sup></p>
					
						<div class="figure">
							<h4>Figure 3: Activation of the SNS in HF</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-3.png') }}" width="594" height="380" alt="figure-3">
							<h5>Hasenfuss, 2015, Figure 22-2  (citation: (1)Hasenfuss G and Mann DL. Pathophysiology of heart failure. In:  Mann DL, Zipes DP, Libby P, eds. <em>Braunwald’s Heart Disease: A Textbook of Cardiovascular Medicine</em>. 10th ed. Philadelphia, PA: Elsevier; 2015.)</h5>
						</div>

						<p>Later in HF, increased activity of the RAAS pathway can lead to sustained expression of angiotensin II and aldosterone.<sup>1</sup> Although short-term expression of angiotensin II can help maintain circulatory homeostasis in times of stress, continuous expression can cause fibrosis in the heart, kidneys, and other organs as well as enhanced SNS activation and aldosterone production.<sup>1</sup> Similarly, aldosterone can induce hypertrophy and fibrosis of the vasculature and myocardium, endothelial cell dysfunction, baroreceptor dysfunction, and oxidative stress.<sup>1,6</sup> Aldosterone can also induce sodium reabsorption and potassium excretion in the kidneys, and the activation of inflammatory pathways in the blood vessels, leading to vascular injury.<sup>1</sup></p>

						<p>Together, the activation of the SNS and RAAS provides short-term benefits to a patient after a cardiovascular event.<sup>1,6</sup> Sustained activation puts further strain on the weakened heart, and contributes to the progressive development of hypertrophy, vascular remodeling, and fibrosis that lead to heart failure.<sup>6</sup></p>
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>2. Endogenous Compensatory Peptides Exhibit Counterregulatory Effects on RAAS and SNS</h2>
					
						<p>There are several counterregulatory neurohormonal systems that become activated in HF to offset the deleterious effects of the vasoconstricting neurohormones.<sup>1</sup></p>

						<p>Counterregulatory neurohormonal systems that become activated in HF include natriuretic peptides, bradykinin, angiotensin 1-7, and adrenomedullin, which are otherwise known as endogenous compensatory peptides <strong>[Figure 4]</strong>.<sup>1,7,8</sup></p>

						<div class="figure">
							<h4>Figure 4: Endogenous compensatory peptides</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-4.png') }}" width="594" height="144" alt="figure-04">
						</div>

						<div class="figure">
							<h4>Figure 5: Endogenous compensatory peptides in healthy hearts vs HF</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-5.png') }}" width="594" height="216" alt="figure-04">
						</div>

						<p>Endogenous compensatory peptides can exert multiple beneficial effects on cardiac remodeling. Vasorelaxation by vessel dilation can improve fluid homeostasis.<sup>9-11</sup> The endogenous compensatory peptides can also modulate LV remodeling with antihypertrophic and antifibrotic activity.<sup>12-15</sup> Further, endogenous compensatory peptides can protect against ischemic injury by reducing inflammation, oxidative stress, and apoptosis.<sup>16-18</sup></p>

						<p>The natriuretic peptides regulate several aspects of RAAS activation, making them important endogenous compensatory peptides.<sup>1</sup> Release of ANP and BNP after atrial or myocardial stretch maintains fluid homeostasis by increasing removal of sodium and water from the heart while inhibiting the release of renin and aldosterone.<sup>1</sup> Bradykinin, a member of the kinin family of vasodilators, regulates vascular tone during HF.<sup>1</sup> Bradykinin is normally degraded by the angiotensin converting enzyme (ACE), and so some of the benefits of ACE inhibition may be due to increased levels of this endogenous compensatory peptide.<sup>1</sup> Angiotensin (1-7) is a vasodilator present in the peripheral vascular system that can lower blood pressure, modulate renal fluid absorption, and attenuate LV remodeling.<sup>15,16,19</sup></p>

						<p>However, the effects of some endogenous compensatory peptides, such as natriuretic peptides, may appear to become less potent with advancing HF, leaving the effects of RAAS unregulated <strong>[Figure 5]</strong>.<sup>1,20,21</sup> Why and how natriuretic peptide signaling weakens over time is unclear, but may include low renal perfusion pressure, lower circulating levels of peptides, altered molecular forms of peptides, or deficient levels of their receptors.<sup>1,20</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>3. Many Endogenous Compensatory Peptides Exert Effects Via Activation of Cyclic Guanosine Monophosphate (cGMP)</h2>

						<p>Many endogenous compensatory peptides exert effects directly or indirectly via activation of the second-messenger molecule, cyclic guanosine monophosphate (cGMP) <strong>[Figure 6]</strong>. Activation of this pathway has several beneficial effects in cardiac myocytes, including depressing contractility, accelerating relaxation, and modulating stiffness.<sup>22</sup></p>

						<div class="figure">
							<h4>Figure 6: Relationship between cGMP and endogenous compensatory peptides</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-6.png') }}" width="594" height="537" alt="figure-6">
							<h5>Cite Hasenfuss 2015 (<strong>citation: (1)</strong>Hasenfuss G and Mann DL. Pathophysiology of heart failure. In:  Mann DL, Zipes DP, Libby P, eds. <em>Braunwald’s Heart Disease</em>. 10th ed. Philadelphia, PA: Elsevier; 2015.)</h5>
						</div>

						<p>The natriuretic peptides stimulate the production of cGMP by binding the natriuretic peptide A receptor (NPR-A) or natriuretic peptide B receptor (NPR-B).<sup>1,21</sup> Both NPR-A and NPR-B are guanylyl cyclases (GC) that can generate cGMP upon ligand binding; NPR-A and NPR-B are also called GC-A and GC-B, respectively.<sup>22</sup> The ensuing intracellular cascade includes protein kinase G, which mediates the majority of cGMP activities in the cell.<sup>22</sup> This pathway can be disrupted by a third receptor, NPR-C, which binds NPs for removal from circulation, by desensitization of NPR-A, or by proteolytic degradation by neprilysin.<sup>21,22</sup></p>

						<p>Bradykinin indirectly signals through cGMP by inducing NO production in the peripheral vasculature <strong>[Figure 7]</strong>. A free radical gas with a short half-life, NO is generated by the enzyme NO synthase (NOS).<sup>1</sup> All 3 isoforms of NOS are expressed in the heart, including the inducible isoform NOS2, which is expressed in response to inflammatory cytokines.<sup>1</sup> Nitric oxide signaling suppresses vasoconstricting factors and induces the production of cGMP in peripheral vasculature to relax the vascular smooth muscle.<sup>1,23</sup></p>

						<div class="figure">
							<h4>Figure 7: Effects of NO on the Cardiovascular System</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-7.png') }}" width="594" height="522" alt="figure-7">
							<h5>B&ouml;ger RH. The emerging role of asymmetric dimethylarginine as a novel cardiovascular risk factor. <em>Cardiovas Res</em>. 2003;59(4), 824-833.</h5>
						</div>
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>4. Endogenous Compensatory Peptides are Degraded by the Enzyme Neprilysin</h2>
						
						<p>During the progression of HF, the counterregulatory pathways become blunted over time.<sup>1</sup> This effect may mediated, in part, by the degradation of endogenous compensatory peptides by neprilysin.<sup>1</sup> Neprilysin is a membrane-bound zinc metalloendopeptidase <strong>[Figure 8]</strong>.<sup>24</sup> The primary targets of neprilysin are ANP and CNP with less affinity for BNP, but neprilysin is also a secondary enzymatic pathway for the other substrates, including additional endogenous compensatory peptides: adrenomedullin and bradykinin.<sup>24-26</sup> In the cardiac tissue, neprilysin metabolizes approximately half of available bradykinin.<sup>25</sup> Degradation of these endogenous compensatory peptides can remove the counterregulation of the RAAS pathway and SNS, leading to worsening cardiovascular remodeling.</p>

						<div class="figure">
							<h4>Figure 8: Neprilysin</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-8.png') }}" width="594" height="327" alt="figure-8">
						</div>
						
						<div class="inline-figure-right" style="width: 386px;">
							<h4>Figure 9: Renal Neprilysin Activity Is Increased in HF</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-9.png') }}" width="366" height="255" alt="figure-9">
							<h5 style="margin-bottom: 0;">Knecht M et al. Increased expression of renal neutral endopeptidase in severe heart failure. <em>Life Sci</em>. 2002;71(23):2701-2712.</h5>
						</div>

						<p>Neprilysin activity is increased in HF <strong>[Figure 9]</strong>.<sup>27</sup> Neprilysin is broadly expressed in the body, including the heart, lung, gut, adrenal glands, and brain.<sup>25</sup> Expression has been detected in endothelial cells, smooth muscle cells, cardiac myocytes, renal epithelial cells, and fibroblasts.<sup>25</sup> Preclinical work with neprilysin and NPs has demonstrated that induced HF after an experimental index event was associated with increased neprilysin expression and activity in the kidney.<sup>27</sup> Although initially produced as a cell-surface molecule, the cleavage of the catalytic extracellular domain of neprilysin from its transmembrane anchor permits release from cells via exosomes into the blood stream, generating circulating neprilysin.<sup>28</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>5. Some Endogenous Compensatory Peptides Have Utility as Prognostic Factors or Biomarkers in HF</h2>
						
						<p>Incorporation of biomarker panels in the clinic has allowed for better characterization of patient health and the underlying condition, giving new insights on how to diagnose and treat a variety of conditions, including heart failure. Recent work has investigated whether endogenous compensatory peptides may provide prognostic information in patients with HF.<sup>29,30</sup></p>

						<p>Brain natriuretic peptide is produced from the heart ventricle during periods of ventricular hemodynamic changes and stress, and so was identified early as a potential marker of ventricular dysfunction.<sup>29</sup> An inactive N-terminal fragment of prohormone BNP (NT-proBNP), created during cleavage of <nobr>pro-BNP</nobr> to release an active C-terminal fragment that is not a substrate of neprilysin, provided another option for measuring ventricular dysfunction <strong>[Figure 10]</strong>.<sup>26,29</sup></p>
						
						<div class="figure">
							<h4>Figure 10: Release of NPs From Cardiac Myocytes</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-10.png') }}" width="594" height="407" alt="figure-10">
							<h5>NT-proBNP, N-terminal prohormone of brain natriuretic peptide. Taub PR et al. Biomarkers of heart failure. <em>Congest Heart Fail.</em> 2010;16 Suppl 1:S19-S24.</h5>
						</div>

						<div class="inline-figure-right" style="width: 343px;">
							<h4>Figure 11: Reduction in NT-proBNP Levels According to Clinical Course of Hospitalization</h4>
							<img src="{{ asset('assets/css/img/modules/clinic/module_02/img/figure-11.png') }}" width="323" height="234" alt="figure-11">
							<h5>Yoo BS. Clinical significance of B-type natriuretic peptide in heart failure. <em>J Lifestyle Med</em>. 2014(1):34-38.</h5>
						</div>

						<p>Both BNP and <nobr>N-terminal</nobr> <nobr>pro-B–type</nobr> natriuretic peptide <nobr>(NT-proBNP)</nobr> have been recommended as diagnostic biomarker for patients with HF.<sup>4,31</sup> This information complements the medical history, clinical examination, and standard diagnostic procedures to improve accuracy for early diagnosis and/or exclusion of HF in the outpatient setting.<sup>31</sup> These biomarkers can also be used to optimize and personalize the dosage of guideline directed medical therapy.<sup>4</sup> The valuable information provided by BNP and NT-proBNP levels have led to the hypothesis that these biomarkers may be prognostic for clinical outcome, including incidence and mortality rates associated with HF <strong>[Figure 11]</strong>.<sup>29</sup> Future clinical trials will investigate this possibility.<sup>29</sup></p>

						<p>Immunoassays that detect the prohormone form of adrenomedullin have also been studied for prognostic value.<sup>30</sup> In the BACH (Biomarkers in the Assessment of Congestive Heart Failure) trial, proadrenomedullin was shown to predict cardiovascular-related death at 90 days, and future work may provide more evidence for the use of this assay in HF prognosis.<sup>30</sup></p>

					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>6. Summary</h2>

						<p>Early in the progression from an index event to HF, the activity of compensatory pathways is modulated by endogenous compensatory peptides.<sup>1</sup> The SNS and RAAS pathways initiate cardiovascular remodeling that permits recovery from the index event, but their sustained activity is maladaptive, leading to cardiovascular remodeling that can impair cardiovascular function.<sup>1,6</sup> Recent research has shown that endogenous compensatory peptides play a key role in the modulation of the SNS and RAAS pathways.<sup>1,7,8</sup> Signaling by endogenous compensatory peptides—including ANP, BNP, CNP, bradykinin, and angiotensin (1-7), among others—can reduce damage during ischemia, improve vasodilation, and suppress inflammation.<sup>9,16-18</sup> However, the cardiovascular system can become desensitized to endogenous compensatory peptide signaling over time, allowing the RAAS and SNS pathways to escape counterregulation, an important phase of the pathogenesis of disease.<sup>1,20,21</sup> Further research on how endogenous compensatory peptides can be used in the monitoring and treatment of HF is ongoing.</p>
							
					</div>
				</div>
			</div>

			<div class="gallery-cell">
				<div class="cell-scroll">	
					<div class="cell-content module-screen">

						<h2>References</h2>

						<ol>
							<li>Hasenfuss G and Mann DL. Pathophysiology of heart failure. In:  Mann DL, Zipes DP, Libby P, eds. <em>Braunwald’s Heart Disease: A Textbook of Cardiovascular Medicine</em>. 10th ed. Philadelphia, PA: Elsevier; 2015. </li>

							<li>McMurray JJ, Adamopoulos S, Anker SD, et al. ESC guidelines for the diagnosis and treatment of acute and chronic heart failure 2012: The Task Force for the Diagnosis and Treatment of Acute and Chronic Heart Failure 2012 of the European Society of Cardiology. Developed in collaboration with the Heart Failure Association (HFA) of the ESC. <em>Eur J Heart Fail</em>. 2012;14:803-869.</li>

							<li>Mozaffarian D, Anker SD, Anand I, et al. Prediction of mode of death in heart failure: the Seattle Heart Failure Model. <em>Circulation</em>. 2007;116:392-398.</li>

							<li>Yancy CW, Jessup M, Bozkurt B, et al. 2013 ACCF/AHA guideline for the management of heart failure: a report of the American College of Cardiology Foundation/American Heart Association Task Force on Practice Guidelines. <em>J Am Coll Cardiol</em>. 2013;62:e147-e239.</li>

							<li>Owan TE, Hodge DO, Herges RM, Jacobsen SJ, Roger VL, Redfield MM. Trends in prevalence and outcome of heart failure with preserved ejection fraction. <em>N Engl J Med</em>. 2006;355:251-259.</li>

							<li>Heusch G, Libby P, Gersh B, et al. Cardiovascular remodelling in coronary artery disease and heart failure. <em>Lancet</em>. 2014;383:1933-1943.</li>

							<li>Grobe JL, Mecca AP, Lingis M, et al. Prevention of angiotensin II-induced cardiac remodeling by angiotensin-(1-7). <em>Am J Physiol Heart Circ Physiol</em>. 2007;292:H736-H742.</li>

							<li>Minami K, Segawa K, Uezono Y, et al. Adrenomedullin inhibits the pressor effects and decrease in renal blood flow induced by norepinephrine or angiotensin II in anesthetized rats. <em>Jpn J Pharmacol</em>. 2001;86:159-164.</li>

							<li>Hobbs A, Foster P, Prescott C, Scotland R, Ahluwalia A. Natriuretic peptide receptor-C regulates coronary blood flow and prevents myocardial ischemia/reperfusion injury: novel cardioprotective role for endothelium-derived C-type natriuretic peptide. <em>Circulation</em>. 2004;110:1231-1235.</li>

							<li>More AS, Kim HM, Zhao R et al. COX-2 mediated induction of endothelium-independent contraction to bradykinin in endotoxin-treated porcine coronary artery. <em>J Cardiovasc Pharmacol</em>. 2014;64:209-217.</li>

							<li>Hasbak P, Sheykhzade M, Schifter S, Edvinsson L. Potentiated adrenomedullin-induced vasorelaxation during hypoxia in organ cultured porcine coronary arteries. <em>J Cardiovasc Pharmacol</em>. 2014;63:58-67.</li>

							<li>Niu P, Shindo T, Iwata H et al. Protective effects of endogenous adrenomedullin on cardiac hypertrophy, fibrosis, and renal damage. <em>Circulation</em>. 2004;109:1789-1794.</li>

							<li>Izumiya Y, Araki S, Usuku H, Rokutanda T, Hanatani S, Ogawa H. Chronic C-type natriuretic peptide infusion attenuates angiotensin II-induced myocardial superoxide production and cardiac remodeling. <em>Int J Vasc Med</em>. 2012;2012:246058.</li>

							<li>Cao L, Gardner DG. Natriuretic peptides inhibit DNA synthesis in cardiac fibroblasts. <em>Hypertension</em>. 1995;25:227-234.</li>

							<li>Tallant EA, Ferrario CM, Gallagher PE. Angiotensin-(1-7) inhibits growth of cardiac myocytes through activation of the mas receptor. <em>Am J Physiol Heart Circ Physiol</em>. 2005;289:H1560-H1566.</li>

							<li>Qi Y, Shenoy V, Wong F et al. Lentivirus-mediated overexpression of angiotensin-(1-7) attenuated ischaemia-induced cardiac pathophysiology. <em>Exp Physiol</em>. 2011;96:863-874.</li>

							<li>Li H, Bian Y, Zhang N, et al. Intermedin protects against myocardial ischemia-reperfusion injury in diabetic rats. <em>Cardiovasc Diabetol</em>. 2013;12:91.</li>

							<li>Burley DS, Baxter GF. B-type natriuretic peptide at early reperfusion limits infarct size in the rat isolated heart. <em>Basic Res Cardiol</em>. 2007;102:529-541.</li>

							<li>Ren Y, Garvin JL, Carretero OA. Vasodilator action of angiotensin-(1-7) on isolated rabbit afferent arterioles. <em>Hypertension</em>. 2002;3:799-802.</li>

							<li>Lourenco P, Araujo JP, Azevedo A, Ferreira A, Bettencourt P. The cyclic guanosine monophosphate/B-type natriuretic peptide ratio and mortality in advanced heart failure. <em>Eur J Heart Fail</em>. 2009;11:185-190.</li>

							<li>McKie PM, Burnett JC, Jr. Rationale and therapeutic opportunities for natriuretic peptide system augmentation in heart failure. <em>Curr Heart Fail Rep</em>. 2015;12:7-14.</li>

							<li>Takimoto E. Cyclic GMP-dependent signaling in cardiac myocytes. <em>Circ J</em>. 2012;76:1819-1825.</li>

							<li>Hayakawa H, Hirata Y, Kakoki M, et al. Role of nitric oxide-cGMP pathway in adrenomedullin-induced vasodilation in the rat. <em>Hypertension</em>. 1999;33:689-693.</li>

							<li>Buggey J, Mentz RJ, DeVore AD, Velazquez EJ. Angiotensin receptor neprilysin inhibition in heart failure: mechanistic action and clinical impact. <em>J Card Fail</em>. 2015;21:741-750.</li>

							<li>Corti R, Burnett JC, Jr., Rouleau JL, Ruschitzka F, Luscher TF. Vasopeptidase inhibitors: a new therapeutic concept in cardiovascular disease? <em>Circulation</em>. 2001;104:1856-1862.</li>

							<li>Volpe M. Natriuretic peptides and cardio-renal disease. <em>Int J Cardiol</em>. 2014;176(3):630-639.</li>

							<li>Knecht M, Pagel I, Langenickel T. et al. Increased expression of renal neutral endopeptidase in severe heart failure. <em>Life Sci</em>. 2002;71:2701-2712.</li>

							<li>Bayes-Genis A, Barallat J, Galan A et al. Soluble neprilysin is predictive of cardiovascular death and heart failure hospitalization in heart failure patients. <em>J Am Coll Cardiol</em>. 2015;65:657-665.</li>

							<li>Yoo BS. Clinical significance of B-type natriuretic peptide in heart failure. <em>J Lifestyle Med</em>. 2014;4:34-38.</li>

							<li>Taub PR, Gabbai-Saldate P, Maisel A. Biomarkers of heart failure. <em>Congest Heart Fail</em>. 2010;16 Suppl 1:S19-S24.</li>

							<li>Roberts E, Ludman AJ, Dworzynski K, et al. The diagnostic accuracy of the natriuretic peptides in heart failure: systematic review and diagnostic meta-analysis in the acute care setting. <em>BMJ</em>. 2015;350:h910.</li>

						</ol>
							
					</div>
				</div>
			</div>

		</div> <!-- Flickity Gallery -->
	</div> <!-- Container -->
</div> <!-- Module Wrapper -->