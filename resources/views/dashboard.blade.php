@extends('layouts.default')

@section('content')

	<div class="dashboard">

		<div class="card card-header dashboard-header">
			<div class="card-content">
				<h1>Your Dashboard</h1>
			
				<h2 class="heart-logo"><i class="icon-heart"></i>heart</h2>
				<h2 class="menu-hamburger"><i class="material-icons icon-menu">&#xE5D2;</i></h2>

				<h4>The online educational community for <span>clinicians treating heart failure</span></h4>
			</div>

			<div class="dashboard-header-grid">
				<a href="{{route('clinic')}}" class="grid-item new-learning">
					<div class="grid-item-content">

						<i class="icon-learning"></i>
						<p><span>{{ $lrcount }}</span> heart Clinic Resources</p>

						<div class="detail-count">
							<h4><span>{{ $lrcompleted }}</span> Viewed</h4>
							<h4><span>{{ $lrcount - $lrcompleted }}</span> Remaining</h4>
						</div>
					
					</div>
				</a>
				<a href="{{route('library')}}" class="grid-item new-literature">
					<div class="grid-item-content">

						<i class="icon-literature"></i>
						<p><span>{{ $literaturecount }}</span> heart Library Articles</p>

						<div class="detail-count">
							<h4><span>{{ $literaturecompleted }}</span> Viewed</h4>
							<h4><span>{{ $literaturecount - $literaturecompleted }}</span> Remaining</h4>
						</div>
					
					</div>
				</a>
				<a href="{{route('theater')}}" class="grid-item new-videos">
					<div class="grid-item-content">

						<i class="icon-videos"></i>
						<p><span>{{ $videocount }}</span> heart Theater Videos</p>

						<div class="detail-count">
							<h4><span>{{ $videocompleted }}</span> Viewed</h4>
							<h4><span>{{ $videocount - $videocompleted }}</span> Remaining</h4>
						</div>
					
					</div>
				</a>
				<div class="grid-item user-card">
					<div class="grid-item-content">
					
						<h4>Logged In As:</h4>

						<p class="user-name">{{ Auth::user()->name }}</p>

						<a href="{{ url('/logout') }}" class="action-btn logout-btn">
							<div class="btn-content">
								<i class="icon-login"></i>
								<p>Logout</p>
							</div>
						</a>
					
					</div>
				</div>
			</div>
			
		</div> <!-- .dashboard-header -->

		<div class="dashboard-grid-wrap">
			<div class="dashboard-grid-left">

				{{-- <div class="card dashboard-expert-exchange">
					<div class="card-content preview-card">

						<h3>Expert Exchange</h3>
		
						<div class="event-container">
							<div class="headshot-block">
								<img src="{{ asset('assets/css/img/butler_headshot.png') }}" width="110" height="154" alt="butler_headshot" />
							</div>
							<div class="event-container-content">
								<h3>REGISTER NOW TO BE A PART OF:</h3>

								<p>Bringing the New Heart Failure Guidelines to Life: The 2016 ACC/AHA/HFSA Focused Update on New Pharmacological Therapy</p>

								<div class="presented-block">
									<h3>PRESENTED BY:</h3>

									<h2>Javed Butler</h2>

									<h4>MD, MPH, MBA</h4>
								</div>

								@if($registeredforevent)

								<div class="time-block">
									<h3><span>LIVE!</span> 7:00 PM EDT</h3>

									<p>Wednesday, August 24, 2016</p>

									<p class="registered">You are registered for<br />this event, thank you.</p>
								</div>

								@elseif(!$registeredforevent)

								<div class="time-block">
									<h3><span>LIVE!</span> 7:00 PM EDT</h3>

									<p>Wednesday, August 24, 2016</p>

									<a href="#" class="action-btn preview-launch-btn register-event-btn" data-toggle="modal" data-target="#eventRegistrationThankYou" onclick="registerForEvent({{ Auth::user()->id }}, 'august_24_2016_butler', '{{ csrf_token() }}');">
										<p>Register For This Event</p>
									</a>

									<p class="registered-hidden">You are registered for<br />this event, thank you.</p>

								</div>

								@endif

							</div>
						</div>

					</div>
				</div> --}} <!-- .dashboard-expert-exchange -->

				{{-- <div class="card dashboard-expert-exchange-live">
					<div class="card-content preview-card">

						<h3>Expert Exchange</h3>
		
						<div class="event-container">
							<div class="headshot-block">
								<img src="{{ asset('assets/css/img/butler_headshot.png') }}" width="110" height="154" alt="butler_headshot" />
							</div>
							<div class="event-container-content">
								<h3>REGISTER NOW TO BE A PART OF:</h3>

								<p>Bringing the New Heart Failure Guidelines to Life: The 2016 ACC/AHA/HFSA Focused Update on New Pharmacological Therapy</p>

								<div class="presented-block">
									<h3>PRESENTED BY:</h3>

									<h2>Javed Butler</h2>

									<h4>MD, MPH, MBA</h4>
								</div>

								<div class="time-block">
									<h3><span>Happening Now!</span></h3>

									<p>Today &ndash; Wednesday, August 24, 2016</p>

									<a href="/heartforum" class="action-btn preview-launch-btn">
										<p>Join Webcast Now</p>
									</a>
								</div>

							</div>
						</div>

					</div>
				</div> --}} <!-- .dashboard-expert-exchange-live -->

				<div class="card dashboard-video">
					<div class="card-content preview-card">
					
						<h3>Featured Video</h3>

						<div class="preview-image">
							<img src="https://i.vimeocdn.com/video/584603581_590x332.jpg" width="325" height="185" alt="preview-image" />
						</div>

						<div class="preview-content">

							<h2 class="preview-title">The Vulnerable Heart: Mechanisms Underlying the Unpredictable Clinical Course of Heart Failure</h2>

							<p class="preview-summary">Learn about the vulnerability of the myocardium in heart failure at the cellular level.</p>

							<a href="/hearttheater?modal=videomoduleModal-6" class="action-btn preview-launch-btn">
								<i class="icon-play"></i>
								<p>Play Video</p>
							</a>
						</div>
					
					</div>
				</div> <!-- .dashboard-video -->

				<div class="card dashboard-event">
					<div class="card-content preview-card">

						<h3>Next Event</h3>

						<div class="preview-image">
							<img src="http://heartstaging.apothecomgroup.com/assets/css/img/event-map.png" width="325" height="185" alt="event-map">
						</div>

						<div class="preview-content">
							<h2 class="preview-title">HFSA</h2>

							<h3 class="preview-subtitle">21st Annual Scientific Meeting</h3>

							<p class="preview-summary">HFSA’s annual meeting has gained a reputation as an outstanding forum for presentation of the latest information available (clinical and basic research, treatment and care of patients) in the field of heart failure.</p>

							<a href="#" class="action-btn preview-launch-btn" data-toggle="modal" data-target="#exitModalDashboard">
								<i class="icon-expand"></i>
								<p>Learn More</p>
							</a>
						</div>

					</div>
				</div> <!-- .dashboard-video -->

			</div> <!-- .dashboard-grid-left -->
			<div class="dashboard-grid-right">

				<div class="card dashboard-featured">
					<div class="card-content">

						<h3>Featured Content</h3>

						<ul class="featured-list">
							<li>

								<?php 

									$lr 			= json_decode($learningresources);
									$random_lr 		= array_rand($lr);
									$random_lr_id 	= $learningresources[$random_lr]['id'] - 1; // arrays 0 - infinity

								?>

								<a href="/heartclinic?modal=learningresourceModal-{{ $learningresources[$random_lr_id]->id }}">
									<i class="icon-learning"></i>

									<p>{{ $learningresources[$random_lr_id]->title }}</p>

								</a>

							</li>
						</ul>
					
					</div>
				</div> <!-- .dashboard-featured -->
				
			</div> <!-- .dashboard-grid-right -->
		</div> <!-- .dashboard-grid-wrap -->
		
	</div> <!-- .dashboard -->

	@include('modals.exit_dashboard')

	@include('modals.event_registration')

@endsection