<!DOCTYPE html>
<html class="lockscreen">
	<head>
		<meta charset="UTF-8">
		<title>heart - the online educational community for clinicians treating heart failure</title>
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
		<meta name="format-detection" content="telephone=no">

		<meta name="description" content="“Heart: The Online Educational Community For Clinicians Treating Heart Failure">

		<meta name="keywords" content="Heart failure,Sudden cardiac death,Neprilysin,Endogenous compensatory peptides,Mode of death ,Mortality,Pathophysiology,Natriuretic peptides,Bradykinin,Angiotensin 1-7,Adrenomedullin,Predictors,Risk factors,Heart decompensation,Atrial natriuretic factor,Brain natriuretic peptide,Natriuretic peptide, C-type,Renin-angiotensin system,Renin-angiotensin-aldosterone System,Sympathetic nervous system,ANP,BNP,CNP,Prognosis,Treatment,Pathogenesis" />

		{!! HTML::style('assets/css/main.css') !!} 

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

		<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('apple-touch-icon-57x57.png') }}">
		<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('apple-touch-icon-60x60.png') }}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('apple-touch-icon-72x72.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('apple-touch-icon-76x76.png') }}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('apple-touch-icon-114x114.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('apple-touch-icon-120x120.png') }}">
		<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('apple-touch-icon-144x144.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('apple-touch-icon-152x152.png') }}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon-180x180.png') }}">
		<link rel="icon" type="image/png" href="{{ asset('favicon-32x32.png" sizes="32x32') }}">
		<link rel="icon" type="image/png" href="{{ asset('android-chrome-192x192.png" sizes="192x192') }}">
		<link rel="icon" type="image/png" href="{{ asset('favicon-96x96.png" sizes="96x96') }}">
		<link rel="icon" type="image/png" href="{{ asset('favicon-16x16.png" sizes="16x16') }}">
		<link rel="manifest" href="{{ asset('manifest.json') }}">
		<link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg" color="#c71d23') }}">
		<meta name="msapplication-TileColor" content="#c71d23">
		<meta name="msapplication-TileImage" content="{{ asset('mstile-144x144.png') }}">
		<meta name="theme-color" content="#c71d23">

		<!-- This script tag to go in to the header section of your page -->
		{{-- <script type="text/javascript" src="https://www.google.com/jsapi?autoload={%27modules%27:[{%27name%27:%27visualization%27,%27version%27:%271.0%27,%27packages%27:[%27corechart%27]}]}"></script> --}}
		
	</head>

	<body>

		<div class="mobile-overlay">
			<div class="card">
				<div class="card-content">
					<img src="{{ asset('assets/css/img/heart_logo_red.png') }}" width="200" height="80" alt="heart" />
					<p>Welcome! This website is optimized for use on your tablet, laptop, or desktop computer.  Mobile-friendly version coming soon!</p>
				</div>
			</div>
		</div>

		<div class="wrapper">
			<div class="fluid-container">
				
				<div class="page user">
					<div class="main-wrapper">

							<div class="main">
								<div class="main-content">
									
									<div class="main-scroll">

										@yield('content')

									</div>

								</div>
							</div>

							<div class="sidebar">
								
							@include('partials.menu')

							</div>

					</div> <!-- .main-wrapper -->
				</div> <!-- .page -->
			
			</div> <!-- .container -->
		</div> <!-- .wrapper -->
		
		<!-- Modal Injection -->

		@if(Request::is('heartclinic')) 

			@include('modals.module')

		@elseif(Request::is('heartlibrary'))

			@include('modals.literature')

		@elseif(Request::is('hearttheater'))

			@include('modals.video')

			@include('modals.module_video')

		@endif

		@include('partials.legal')

		@include('partials.hcp')

		<!-- Scripts -->

		{!! HTML::script('assets/js/vendor.js') !!} 
		{!! HTML::script('assets/js/main.js') !!} 

		<!-- Google Analytics -->

		@if(Auth::check())
		
		<div id="ga-data" user-id="{{ Auth::user()->id }}" user-email="{{ Auth::user()->email }}" user-name="{{ Auth::user()->name }}" user-specialty="{{ Auth::user()->specialty }}" style="display: none;"></div>

		@else

		<div id="ga-data" user-id="0" user-email="unregistered@theheartcommunity.com" user-name="Guest" user-specialty="Guest" style="display: none;"></div>

		@endif

		@include('partials.analytics')
		

	</body>
</html>