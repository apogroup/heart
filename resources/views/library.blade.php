@extends('layouts.default')

@section('content')

	<div class="literature">

		@if(!Auth::check())
		<div class="card register-call">
			<div class="card-content">
				<div class="register-left">
					<div class="register-table">
						<div class="register-content">

							<p class="register-call-existing">Already a member? <a href="{{ url('/login') }}"><nobr>Login Now</nobr></a></p>
						
						</div>
					</div>
				</div>
				<div class="register-center">
					<div class="register-table">
						<div class="register-content">
							
							<h3>Still not a member of <span>TheHeartCommunity.com?</span></h3>
						
						</div>
					</div>
				</div>
				<div class="register-right">
					<div class="register-table">
						<div class="register-content">

							<a href="{{ url('/register') }}" class="action-btn preview-launch-btn register-call-btn">
								<div class="register-call-btn-content">
									<i class="icon-heart"></i>
									<p>Register Now</p>
								</div>
							</a>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
		
		<div class="card card-header literature-header">
			<div class="card-content">
				
				{{-- <h1>Literature</h1> --}}

				<h2 class="heart-logo"><i class="icon-heart"></i>heart Library</h2>
				<h2 class="menu-hamburger"><i class="material-icons icon-menu">&#xE5D2;</i></h2>

				<h4>Keep abreast of the latest medical literature in heart failure.</h4>
			
			</div>
		</div> <!-- .literature-header -->

		<div class="card card-header search-card">
			<div class="card-content">

				<div class="query-sort">

					{{-- <div class="sort-container">
						<i class="sort-close icon-expand"></i> 
						<span>Sort By</span>
						<ul class="sort-list">
							@if(!empty($query))
							<li><a href="?query={{ $query }}&sort=views&order=desc"">Most Viewed</a></li>
						  	<li><a href="?query={{ $query }}&sort=title&order=asc">Title Ascending</a></li>
						  	<li><a href="?query={{ $query }}&sort=title&order=desc">Title Descending</a></li>
						  	@else
						  	<li><a href="?sort=views&order=desc"">Most Viewed</a></li>
						  	<li><a href="?sort=title&order=asc">Title Ascending</a></li>
						  	<li><a href="?sort=title&order=desc">Title Descending</a></li>
						  	@endif
						</ul>  
					</div> --}}

					<div class="sort-container">
						
						<p><strong>Sort By:</strong> &nbsp;&nbsp;
						@if(!empty($query))
							<a href="?query={{ $query }}&sort=views&order=desc">
							@if(isset($_GET["sort"]) && $_GET["sort"] == 'views') <strong>Most Viewed</strong>
							@else Most Viewed 
							@endif
							</a> &nbsp;&bull;&nbsp;
						  	<a href="?query={{ $query }}&sort=title&order=asc">
						  	@if(isset($_GET["sort"]) && $_GET["sort"] == 'title' && $_GET["order"] == 'asc') <strong>Title Ascending</strong>
							@else Title Ascending
							@endif
						  	</a> &nbsp;&bull;&nbsp;
						  	<a href="?query={{ $query }}&sort=title&order=desc">
						  	@if(isset($_GET["sort"]) && $_GET["sort"] == 'title' && $_GET["order"] == 'desc') <strong>Title Descending</strong>
							@else Title Descending
							@endif
						  	</a>
						@else
						  	<a href="?sort=views&order=desc">
							@if(isset($_GET["sort"]) && $_GET["sort"] == 'views') <strong>Most Viewed</strong>
							@else Most Viewed 
							@endif
							</a> &nbsp;&bull;&nbsp;
						  	<a href="?sort=title&order=asc">
						  	@if(isset($_GET["sort"]) && $_GET["sort"] == 'title' && $_GET["order"] == 'asc') <strong>Title Ascending</strong>
							@else Title Ascending
							@endif
						  	</a> &nbsp;&bull;&nbsp;
						  	<a href="?sort=title&order=desc">
						  	@if(isset($_GET["sort"]) && $_GET["sort"] == 'title' && $_GET["order"] == 'desc') <strong>Title Descending</strong>
							@else Title Descending
							@endif
						  	</a>
						@endif
						</p>

					</div>

				</div>

			</div>
		</div>

		<div class="card card-header search-card">
			<div class="card-content">

				<div class="search">

					{!! Form::open(array('id' => 'library-search', 'method' => 'POST',  url(''), 'class' => 'search-form')) !!}

						{!! Form::text('query', null, array('required' => 'required', 'placeholder'=>'')) !!}
						
						<div class="submit-btn">
							{!! Form::submit('Search', array('class' => 'btn btn-primary')) !!}
						</div>

					{!! Form::close() !!}
				
				</div>

				<div class="display-query">

					@if(!empty($query))

					<p>Displaying search results for: "<span class="query">{{ $query }}</span>" &nbsp;&nbsp; <a href="/heartlibrary">Clear</a></p>

					@endif

				</div>

			</div>
		</div>

		<?php $index=0; ?>
			
		@foreach ($literature as $literatureitem)

			<?php $index++ ?>

			<div class="card single-item">
				<div class="card-content">
					<h3>{{ $literatureitem->title }}</h3>
					<!-- <div class="single-icon"></div> -->
					
					<div class="single-action">
						<a href="#" class="action-btn single-action-btn literatureModal-{{$literatureitem->id}}" data-toggle="modal" data-target="#literatureModal-{{$literatureitem->id}}" onclick="markResourceViewed(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif, {{ $literatureitem->id }},'library-article');">
							<i class="icon-expand"></i>
							<p>Go to Article</p>
						</a>
					</div>

					<div class="single-content">
						<p>{{ str_limit($literatureitem->description, 400) }}</p>
					</div>
				</div>
			</div>

		@endforeach


	</div> <!-- .literature -->

@endsection