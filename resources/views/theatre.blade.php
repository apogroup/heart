@extends('layouts.default')

@section('content')

	<div class="videos">

		@if(!Auth::check())
		<div class="card register-call">
			<div class="card-content">
				<div class="register-left">
					<div class="register-table">
						<div class="register-content">

							<p class="register-call-existing">Already a member? <a href="{{ url('/login') }}"><nobr>Login Now</nobr></a></p>
						
						</div>
					</div>
				</div>
				<div class="register-center">
					<div class="register-table">
						<div class="register-content">
							
							<h3>Still not a member of <span>TheHeartCommunity.com?</span></h3>
						
						</div>
					</div>
				</div>
				<div class="register-right">
					<div class="register-table">
						<div class="register-content">

							<a href="{{ url('/register') }}" class="action-btn preview-launch-btn register-call-btn">
								<div class="register-call-btn-content">
									<i class="icon-heart"></i>
									<p>Register Now</p>
								</div>
							</a>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
		
		<div class="card card-header videos-header">
			<div class="card-content">

				<h2 class="heart-logo"><i class="icon-heart"></i>heart Theater</h2>
				<h2 class="menu-hamburger"><i class="material-icons icon-menu">&#xE5D2;</i></h2>

				<h4>Watch the experts speak on the clinical challenges, current practices, and emerging opportunities in heart failure.</h4>
			
			</div>
		</div> <!-- .videos-header -->

		<div class="video-masonry">
			
			@foreach ($videos as $video)

				<div class="card video-card">
					<div class="card-content">

						@if($video->type == "video")

						<div class="video-thumbnail-wrapper" data-toggle="modal" data-target="#videoModal-{{$video->id}}" video-id="{{$video->id}}" onclick="initVimeo('video-{{$video->id}}'); markResourceViewed(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif, {{ $video->id }},'theater-video');">
							<div class="video-thumbnail">
								<img src="{{ $video->thumbnail }}" alt="preview-image" />
							</div>
						</div>

						<div class="video-card-content">
							<h3 class="video-title">{{ $video->title }}</h3>

							<h4 class="video-subtitle">{{ $video->subtitle }}</h4>

							<p class="video-description">{{ str_limit($video->description, 150) }}</p>

							<a href="#" class="action-btn video-launch-btn" data-toggle="modal" data-target="#videoModal-{{$video->id}}" video-id="{{$video->id}}" onclick="initVimeo('video-{{$video->id}}'); markResourceViewed(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif, {{ $video->id }},'theater-video');">
								<i class="icon-play"></i>
								<p>Play Video</p>
							</a>

						</div>

						@endif

						@if($video->type == "module")

						<div class="video-thumbnail-wrapper" data-video='{{ $video->id }}' data-toggle="modal" data-target="#videomoduleModal-{{ $video->id }}" onclick="getTheaterModule({{ $video->id }}); markResourceViewed(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif, {{ $video->id }},'theater-video-chaptered');">
							<div class="video-thumbnail">
								<img src="{{ $video->thumbnail }}" alt="preview-image" />
							</div>
						</div>

						<div class="video-card-content">
							<h3 class="video-title">{{ $video->title }}</h3>

							<h4 class="video-subtitle">{{ $video->subtitle }}</h4>

							<p class="video-description">{{ str_limit($video->description, 150) }}</p>

							<a href="#" class="action-btn video-launch-btn" data-video='{{ $video->id }}' data-toggle="modal" data-target="#videomoduleModal-{{ $video->id }}" onclick="getTheaterModule({{ $video->id }}); markResourceViewed(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif, {{ $video->id }},'theater-video-chaptered');">
								<i class="icon-play"></i>
								<p>Play Video</p>
							</a>

						</div>

						@endif

					</div>
				</div>

			@endforeach

		</div> <!-- .video-masonry -->

	</div> <!-- .videos -->

@endsection