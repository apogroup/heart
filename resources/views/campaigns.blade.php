
<html>
  <head>
  <title>heart - the online educational community for clinicians treating heart failure</title>

  <script>

    setTimeout(redirect, 100);

    function redirect(){

    window.location = "http://theheartcommunity.com/{{ $event_referral }}?utm_source={{ $event_label }}&utm_medium={{ $event_category }}&utm_campaign={{ $event_label }}";
    }  

  </script>

  <!-- Current Campaigns
		
		http://theheartcommunity.com/campaigns/email/open/July%2026th%202016%20Registration%20Email%20Campaign/register
    http://theheartcommunity.com/campaigns/email/open/August%205th%202016%20Event%20Registration%20Email%20Campaign/register
    http://theheartcommunity.com/campaigns/email/open/August%2017th%202016%20Event%20Registration%20Email%20Campaign/register
    http://theheartcommunity.com/campaigns/email/open/August%2023rd%202016%20Event%20Registration%20Email%20Campaign/register
    http://theheartcommunity.com/campaigns/email/open/August%2019th%202016%20Event%20Registration%20Email%20Campaign/register

    http://theheartcommunity.com/campaigns/email/open/August%2018th%202016%20Event%20Reminder%20Email%20Campaign/heartforum

    http://theheartcommunity.com/campaigns/email/open/September%2015th%202016%20Content%20Update%20Email%20Campaign/heartclinic?modal=learningresourceModal-6
    http://theheartcommunity.com/campaigns/email/open/September%2015th%202016%20Content%20Update%20Email%20Campaign/hearttheater?modal=videomoduleModal-7

    http://theheartcommunity.com/campaigns/email/open/September%2027th%202016%20Content%20Update%20Email%20Campaign/heartlibrary
    http://theheartcommunity.com/campaigns/email/open/September%2027th%202016%20Content%20Update%20Email%20Campaign/hearttheater

    http://theheartcommunity.com/campaigns/email/open/October%204th%202016%20Registration%20Email%20Campaign/register

    http://theheartcommunity.com/campaigns/email/open/October%2010th%202016%20Content%20Update%20Email%20Campaign/hearttheater

    http://theheartcommunity.com/campaigns/email/open/October%2018th%202016%20Registration%20Email%20Campaign/register

    http://theheartcommunity.com/campaigns/email/open/November%202nd%202016%20Registration%20Email%20Campaign/register
    http://theheartcommunity.com/campaigns/email/open/November%209th%202016%20Registration%20Email%20Campaign/register

    http://theheartcommunity.com/campaigns/email/open/November%2023rd%202016%20Content%20Update%20Email%20Campaign/hearttheater

    http://theheartcommunity.com/campaigns/email/open/November%2029th%202016%20Registration%20Email%20Campaign/register

    http://theheartcommunity.com/campaigns/email/open/December%2013th%202016%20Registration%20Email%20Campaign/register

    http://theheartcommunity.com/campaigns/email/open/January%2012th%202017%20Registration%20Email%20Campaign/register

    http://theheartcommunity.com/campaigns/email/open/January%2018th%202017%20Registration%20Email%20Campaign/register

	-->

  </head>
</html>