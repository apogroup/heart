@extends('layouts.default')

@section('content')

	<div class="learning">
		
		@if(!Auth::check())
		<div class="card register-call">
			<div class="card-content">
				<div class="register-left">
					<div class="register-table">
						<div class="register-content">

							<p class="register-call-existing">Already a member? <a href="{{ url('/login') }}"><nobr>Login Now</nobr></a></p>
						
						</div>
					</div>
				</div>
				<div class="register-center">
					<div class="register-table">
						<div class="register-content">
							
							<h3>Still not a member of <span>TheHeartCommunity.com?</span></h3>
						
						</div>
					</div>
				</div>
				<div class="register-right">
					<div class="register-table">
						<div class="register-content">

							<a href="{{ url('/register') }}" class="action-btn preview-launch-btn register-call-btn">
								<div class="register-call-btn-content">
									<i class="icon-heart"></i>
									<p>Register Now</p>
								</div>
							</a>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
		
		<div class="card card-header learning-header preview-card">
			<div class="card-content">
				
				{{-- <h1>Learning Resources</h1> --}}

				<h2 class="heart-logo"><i class="icon-heart"></i>heart Clinic</h2>
				<h2 class="menu-hamburger"><i class="material-icons icon-menu">&#xE5D2;</i></h2>

				<h4>Discover a wide range of online learning resources in heart failure.</h4>

				<div class="preview-image">
					<img src="{{ asset('assets/css/img/preview-module-1.png') }}" width="325" height="185" alt="preview-module" />
				</div>

				<div class="preview-content">
					<h2 class="preview-title">Module 1: The “Frailty” of the Heart Failure (HF) Patient</h2>

					<p class="preview-summary">Take a deeper look into the unpredictable clinical course of heart failure and the underlying mechanisms of sudden cardiac death in these patients.</p>

					<a href="#" class="action-btn preview-launch-btn module-modal-open" data-resource='1' data-toggle="modal" data-target="#learningresourceModal-1" onclick="getClinicModule(1); markResourceViewed(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif,1,'clinic-module');">
						<i class="icon-expand"></i>
						<p>Launch Module</p>
					</a>
				</div>
			
			</div>
		</div> <!-- .learning-header -->

		<div class="card learning-library">
			<div class="card-content" style="padding-bottom: 0;">
				
				<h3 style="margin-bottom: 0;">All Resources</h3>

			</div> <!-- .card-content -->

			<!-- <div class="learning-library-tabs">
				<div class="library-tab">
					<img src="" alt="">
					<p>E-Modules</p>
				</div>
			</div> --> <!-- .learning-library-tabs -->

			<!-- <div class="learning-library-list-header">

				<div class="column-head column-title">
					<p>Title</p>
				</div>

				<div class="column-head column-date">
					<p>Date Posted</p>
				</div>
				
			</div> --> <!-- learning-library-list-header -->

			<div class="learning-library-list">
				
				<ul>
						
					@foreach ($learningresources as $learningresource)

						<li>
							<div class="item-title">
								<a href="#" class="light-btn" >
									
									<div class="light-btn-content module-modal-open" data-resource='{{ $learningresource->id }}' data-toggle="modal" data-target="#learningresourceModal-{{ $learningresource->id }}" onclick="getClinicModule({{ $learningresource->id }}); markResourceViewed(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif,{{ $learningresource->id }},'clinic-module');">
										<i class="material-icons">&#xE051;</i>
										<p>{{ $learningresource->title }}</p>
									</div>
								</a>
							</div>
						</li>

					@endforeach

				</ul>

			</div> <!-- learning-library-list -->

		</div> <!-- .learning-library -->

	</div> <!-- .learning -->

@endsection