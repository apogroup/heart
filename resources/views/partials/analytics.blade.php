<script>
	
// GA INTERFACE /////////////////////////////////////////////////////////////////////

	// production

	// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	// ga('create', 'UA-75776347-1', 'auto');
	// ga('send', 'pageview');

	// development

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');




// GA INIT & SEND ///////////////////////////////////////////////////////////////////


	// initialize ga

	ga('create', 'UA-75776347-1', 'auto'); // production
	// ga('create', 'UA-75776347-2', 'auto'); // dev
	

	// retreive data points from dom

	// create the object to data points
	var gaUser = {};
		gaUser.email 		= 'guest';
		gaUser.name 		= 'guest';
		gaUser.specialty 	= 'guest';

	// when the dom is ready -- lets get the data points built into by the server
	$(function(ga){

		e 	= document.getElementById('ga-data').getAttribute('user-email');
		n 	= document.getElementById('ga-data').getAttribute('user-name');
		s 	= document.getElementById('ga-data').getAttribute('user-specialty');

		if( e != '' ) gaUser.email 		= document.getElementById('ga-data').getAttribute('user-email');
		if( n != '' ) gaUser.name 		= document.getElementById('ga-data').getAttribute('user-name');
		if( s != '' ) gaUser.specialty 	= document.getElementById('ga-data').getAttribute('user-specialty');

		// console.log(gaUser);

	});

	// send pageview

	ga('send', 'pageview', { 'dimension1': gaUser.email, 'demension2': gaUser.name, 'demension3': gaUser.specialty });


</script>