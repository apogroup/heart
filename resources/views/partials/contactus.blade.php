<div class="contactus">

	<h3>Call Novartis Medical Information</h3>

	<h3>1-855-ASK-4MIC (1-855-275-4642)</h3>

	<p>Health care professionals can call Novartis Pharmaceuticals Medical Information directly with questions regarding our products or services. Call us Monday–Friday, from 8:30 AM to 5:00 PM ET.</p>
	

	<h3 style="padding-top: 1em;">Visit Medical Information Website</h3>

	<p>Novartis provides timely access to important information through targeted searches to keep you informed when you have questions about our medicines</p>

	<p>Login: <a href="https://medinfo.novartispharmaceuticals.com/" target="_blank">https://medinfo.novartispharmaceuticals.com/</a></p>


	<h3 style="padding-top: 1em;">Email Technical Support</h3>

	<p style="margin-bottom: 0;">For technical support items related to this site, email us at <a href="mailto:support@TheHeartCommunity.com">support@TheHeartCommunity.com</a>. Please include your username (if applicable) and preferred mode of contact. You should anticipate a response to your message within 2 business days.</p>

</div>  <!-- .card-content -->