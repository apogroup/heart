<!-- LEGAL MODALS -->

<div class="modal module-modal fade" id="privacyPolicyModal" role="dialog">
    <div class="modal-dialog">
        
      <div class="modal-content">
        
        <div class="modal-header">
          <h3 class="header-title">Privacy Policy</h3>
          <button type="button" class="close" data-dismiss="modal">
            <i class="material-icons">&#xE5CD;</i>
            <p>Close</p>
          </button>
        </div>

        <div class="modal-body">

          <div class="fullwidth-wrapper">

            <div class="fullwidth-content">
                
                @include('partials.privacypolicy')
                
            </div> <!-- .privacy-policy -->

          </div>

        </div>

      </div> <!-- .modal-content -->

    </div> <!-- .modal-dialog -->
  </div> <!-- .modal -->

  <div class="modal module-modal fade" id="termsOfServiceModal" role="dialog">
    <div class="modal-dialog">
        
      <div class="modal-content">
        
        <div class="modal-header">
          <h3 class="header-title">Terms of Use</h3>
          <button type="button" class="close" data-dismiss="modal">
            <i class="material-icons">&#xE5CD;</i>
            <p>Close</p>
          </button>
        </div>

        <div class="modal-body">

          <div class="fullwidth-wrapper">

            <div class="fullwidth-content">
                
                @include('partials.termsofservice')
                
            </div> <!-- .privacy-policy -->

          </div>

        </div>

      </div> <!-- .modal-content -->

    </div> <!-- .modal-dialog -->
  </div> <!-- .modal -->

  <div class="modal fade" id="contactUsModal" role="dialog">
    <div class="modal-dialog">
        
      <div class="modal-content">
        
        <div class="modal-header">
          <h3 class="header-title">Contact Us</h3>
          <button type="button" class="close" data-dismiss="modal">
            <i class="material-icons">&#xE5CD;</i>
            <p>Close</p>
          </button>
        </div>

        <div class="modal-body">

          <div class="fullwidth-wrapper">

            <div class="fullwidth-content">
                
                @include('partials.contactus')
                
            </div> <!-- .privacy-policy -->

          </div>

        </div>

      </div> <!-- .modal-content -->

    </div> <!-- .modal-dialog -->
  </div> <!-- .modal -->