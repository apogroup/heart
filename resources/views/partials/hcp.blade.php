<div class="modal fade hcp-modal" id="hcpModal" role="dialog">
    <div class="modal-dialog">
        
		<div class="modal-content">

			<div class="modal-header">
				<h3 class="header-title">Welcome to heart</h3>
			</div>

			<div class="modal-body login">

				<div class="card card-header login-card">
					<div class="card-content">

						<h2><i class="icon-heart"></i>heart</h2>
						<h4>The online educational community for <span>clinicians treating heart failure</span></h4>

						<div id="pre-confirmation">

							<p class="login-message">Welcome to <strong>heart</strong>! Please confirm you are a US health care professional.</p> 

							<div class="confirm-yes-btn" data-dismiss="modal">
								<input class="btn red" id="confimation-yes-button" type="submit" value="Yes, I am a US health care professional"/>
							</div>

							<div class="confirm-no-btn">
								<a href="http://www.google.com"><input class="btn" id="confimation-no-button" type="submit" value="No, I am not a US health care professional" /></a>
							</div>

						</div>
					
					</div>
				</div>

			</div> <!-- .modal-body -->

		</div> <!-- .modal-content -->

	</div> <!-- .modal-dialog -->

</div> <!-- .modal -->





{{-- IPAD KIOSK --}}


<div class="modal fade timeout-modal" id="timeoutModal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        
		<div class="modal-content">

			<div class="modal-header">
				<h3 class="header-title">Are you still there?</h3>
			</div>

			<div class="modal-body login">

				<div class="card card-header login-card">
					<div class="card-content">

						<h2><i class="icon-heart"></i>heart</h2>
						<h4>The online educational community for <span>clinicians treating heart failure</span></h4>

						<div id="pre-confirmation">

							<div class="confirm-yes-btn reset-no-btn" data-dismiss="modal">
								<input class="btn red" id="confimation-yes-button" type="submit" value="Yes, I am still browsing"/>
							</div>

							<div class="confirm-no-btn reset-yes-btn">
								<input class="btn" id="confimation-no-button" type="submit" value="Resetting in 30 seconds" />
							</div>

						</div>
					
					</div>
				</div>

			</div> <!-- .modal-body -->

		</div> <!-- .modal-content -->

	</div> <!-- .modal-dialog -->

</div> <!-- .modal -->