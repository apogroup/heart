<div class="privacypolicy">
	
	<h3>Last Updated: April 1, 2016</h3>

	<h1>PRIVACY POLICY</h1>
	
	<p>Welcome to the website of “heart: the online educational community for clinicians treating heart failure” located at <a href="http://www.theheartcommunity.com" target="_blank">www.theheartcommunity.com</a> (the “Site”) and owned by Novartis Pharmaceutical Corporation (“we”, “us” or “our”). This Privacy Policy describes how we use and disclose the information we collect from you through the Site, and the choices you have about how we use that information.</p>

	<p>This Site is intended for use by U.S. health care professionals only. </p>

	<p>By accessing or using the Site, you are deemed to accept the terms of this Privacy Policy. If you do not agree to this Privacy Policy, you may not access or otherwise use the Site. We reserve the right, at our discretion, to change, modify, add or remove portions of this Privacy Policy from time to time. Such changes will be posted on the Site and may be made known to you via email. We encourage you to periodically review the Site for the latest information on our privacy practices.</p>

	<h2>What Information Do We Collect?</h2>

	<p><u>Personally Identifiable Information</u></p>

	<p>Through the Site, we may collect certain personally identifiable information that you voluntarily provide when you use the Site, such as when you register, participate in an interactive feature or forum (such as chat features), or contact us with a question, comment, or request. Such personally identifiable information may include, without limitation, your full name, email address, your user name, your area of specialty, information about health and/or medical conditions and specialties. If you submit personal information relating to other people to us or to other users, you represent that you have the authority to do so and to permit us to use the information in accordance with this Privacy Policy.</p>


	<p><u>Non-Personally Identifiable Information</u></p>

	<p>We or our third party service providers may collect information automatically from your device including:</p>

	<p><i>Through your browser or device</i>: Certain information is collected by most browsers or automatically through your device, such as your Media Access Control (MAC) address, computer type (Windows or Macintosh), screen resolution, operating system name and version, device manufacturer and model, language, Internet browser type and version and the name and version of the browser you are using. We use this information to ensure that the Site functions properly.</p>

	<p><i>Through Cookies</i>: Cookies are small text files stored directly on your device. We use cookies to collect information such as time spent on the Site, pages visited, the pages you view immediately before and after you access the Site, the search terms you enter, and other anonymous traffic data. Cookies allow us to recognize you and personalize your experience, to facilitate navigation, and to display information more effectively. We also use cookies to gather statistical information about the use of the Site in order to understand how they are used, improve them, and resolve questions about them. If you do not want information collected through the use of cookies, most browsers allow you to automatically decline cookies or to be given the choice of declining or accepting cookies from a particular site. You may wish to refer to http://www.allaboutcookies.org/manage-cookies/index.html. If you do not accept our cookies, you may experience some inconvenience in your use of the Site.</p>

	<p><i>Through use of Pixel Tags and Other Similar Technologies</i>: We may use pixel tags (also known as web beacons and clear GIFs) to, among other things, track the actions of users of the Site and our email recipients and compile statistics about usage of the Site.</p>

	<p><i>IP Address</i>: Your IP Address is a number that is automatically assigned to the computer that you are using by your Internet Service Provider. An IP Address may be identified and logged automatically in our server log files whenever a user accesses the Site, along with the time of the visit and the page(s) visited. Collecting IP Addresses is standard practice and is done automatically by many websites, applications and other services. We use IP Addresses for purposes such as calculating usage levels of the Site, helping diagnose server problems, and administering the Site.</p>

	<h2>How Do We Use Your Information?</h2>

	<p><i>Personally Identifiable Information</i></p>

	<p>In addition to using your personally identifiable information to deliver the information you request, and in connection with your use of the Site, we may also use it:</p>

	<ul>
		<li>to provide customer support or respond to your queries/complaints;</li>
		<li>to provide you with information about the Site, or services, your accounts, and notices;</li>
		<li>to permit you to participate in promotions or other interactive features, such as chat features;</li>
		<li>to personalize your experience and better tailor content to you;</li>
		<li>to help us, our subsidiaries, affiliates, and business partners better understand our audiences, evaluate user interest in the Site, improve the Site, provide any services in connection with the Site, and perform other market research activities;</li>
		<li>for our business purposes, such as data analysis; audits; monitoring and prevention of fraud, infringement, and other potential misuse of the Site; modifying the Site; and operating and expanding our business activities; and</li>
		<li>as we believe to be necessary or appropriate: (a) under applicable law, including laws outside your country of residence; (b) to comply with legal process; (c) to respond to requests from public and government authorities, including public and government authorities outside your country of residence; (d) to enforce our terms and conditions; (e) to protect our operations or those of any of our affiliates; (f) to protect our rights, privacy, safety or property, and/or that of our affiliates, you or others; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
	</ul>

	<p><i>Non-Personally Identifiable Information</i></p>

	<p>We may use information that does not personally identify you for any purpose, except where we are required to do otherwise under applicable law. We may combine, aggregate, or anonymize any of the information we collect from you with information we may collect from or about you from any other online or offline source.</p>

	<h2>When and to Whom Do We Disclose Your Information?</h2>

	<p>The Personally Identifiable Information we collect from and about you may be disclosed:</p>

	<ul>
		<li>to our subsidiaries and affiliates within the Novartis global group, for the purposes described in this Privacy Policy;</li>
		<li>to third parties that provide services to us, such as operating the Site on our behalf, fulfilling requests for information, answering calls, or delivering other communications;</li>
		<li>to identify you to any person to whom you send messages;</li>
		<li>by you, on message boards, chats, and other services to which you post information and materials. Because any information you post may become public, we urge you to be very careful when deciding to disclose any information on or through the Site;</li>
		<li>as required by law, such as to law enforcement, to health authorities to report possible adverse events, during government inspections or audits, as ordered or directed by courts or other governmental agencies, or in order to comply with a subpoena or other legal process;</li>
		<li>when we believe in good faith that disclosure is necessary to protect legal rights or the security or integrity of the Site; protect your safety or the safety of others; investigate fraud, a breach of contract, or a violation of law; or respond to a government request; and</li>
		<li>to third parties, advisors, and other entities to the extent reasonably necessary for development of or to proceed with the negotiation or completion of a corporate or commercial transaction, including a reorganization, merger, acquisition, joint venture, sale or other disposition of all or a portion of our business, assets, or stock (including in connection with any bankruptcy or similar proceedings).</li>
	</ul>

	<p>We may disclose information that does not personally identify you for any purpose, except where we are required to do otherwise under applicable law.</p>

	<h2>Your Choices</h2>

	<p>We offer you the ability to discontinue your participation or to opt out of receiving marketing communications (including email alerts) in the communication itself, or you can visit our Contact Us page, write to us at support@theheartcommunity.com. We will process your request within a reasonable time after receipt.</p> 

	<p>We do not currently respond to web browser "do not track" signals or other mechanisms that provide a method to opt out of the collection of information across websites or other online services.</p>

	<h2>Information Security</h2>

	<p>We use appropriate technical, administrative, and physical safeguards to protect the information collected through the Site. Unfortunately, no organization can guarantee the absolute security of information, especially information transmitted over the Internet.</p>

	<p>You may register on the Site. At the time you register you will create a password. Please remember your password as you will be asked to provide it each time you attempt to access your personal information. You are responsible for maintaining the confidentiality of your password and for all uses of your password, whether or not authorized by you. You should not disclose your password to any other person. In order to protect the security of your account and personal information, when your visit is complete you should be sure to log off of the Site and shut down your browser.</p>

	<h2>Children's Information</h2>

	<p>We do not knowingly collect personally identifiable information online from a child under the age of 13. This Site is not intended for use by children under the age of 13. If a child under the age of 13 has provided us with personally identifiable information, we will use all reasonable efforts to delete such information from our database.</p>   

	<h2>Accessing and Updating Your Information</h2>

	<p>We encourage you to update the personally identifiable information you provide to us to help us continue to provide information that best meets your needs. Please email us at support@theheartcommunity.com. For your protection, we may only implement requests with respect to the personally identifiable information associated with the particular email address that you use to send us your request, and we may need to verify your identity before implementing your request.</p>

	<h2>Links to Third Party Websites</h2>

	<p>The Site may contain links to third-party services that are not under our control. We are not responsible for the collection and use of your information by such services, and we encourage you to review their privacy policies. </p>

	<h2>Texas Residents</h2>

	<p>Pursuant to the Texas Health and Safety Code, Sec. 181.154, please be advised that if we receive any information that identifies you and relates to your past, present or future physical or mental health, healthcare or payment for your healthcare, such information may be subject to electronic disclosure by such means as file transfers or email.</p>

	<h2>United States of America</h2>

	<p>Our Site is maintained in the United States of America.  By using the Site, you authorize the export of personally identifiable information to the U.S.A. and its storage and use as specified in this Privacy Policy.</p>

	<h2>Changes to This Privacy Policy</h2>

	<p>This Privacy Policy became effective on April 1, 2016. We may update it from time to time by posting a new Privacy Policy on the Site. You are advised to consult the Site regularly for any changes, and your continued use of the Site after such changes have been made constitutes acceptance of those changes.</p>

	<h2>How to Contact Us</h2>
	
	<p>If you have any questions about our Privacy Policy, please visit our Contact Us page, write to us at Novartis Pharmaceuticals Corporation, 1 Health Plaza, East Hanover, New Jersey, 07936 attention Privacy Office, or call us at 1-888-NOW-NOVA <nobr>(1-888-669-6682)</nobr>.</p>

</div>  <!-- .card-content -->