<div class="sidebar-content">
	<div class="sidebar-logo">

		<img src="{{ asset('assets/css/img/heart_logo.png') }}" width="158" height="64" alt="heart" />
		<i class="material-icons icon-menu-close">&#xE14C;</i>

	</div>
	<div class="sidebar-menu">

		@if(!Auth::check())
		<a href="{{ url('/login') }}" @if(route('login', '') == Request::url()) class="sidebar-menubtn active" @else class="sidebar-menubtn" @endif>
			<div class="sidebar-menubtn-content">
				<div class="sidebar-menubtn-icon">
					<i class="icon-login"></i>
				</div>
				<p class="sidebar-menubtn-label">Login</p>
			</div>
		</a>
		@endif
		@if(Auth::check())
		<a href="{{route('dashboard')}}" @if(route('dashboard') == Request::url()) class="sidebar-menubtn active" @else class="sidebar-menubtn" @endif>
			<div class="sidebar-menubtn-content">
				<div class="sidebar-menubtn-icon">
					<i class="icon-heart"></i>
				</div>
				<p class="sidebar-menubtn-label">Your Dashboard</p>
			</div>
		</a>
		@endif
		<a href="{{route('clinic')}}" @if(route('clinic') == Request::url()) class="sidebar-menubtn active" @else class="sidebar-menubtn" @endif>
			<div class="sidebar-menubtn-content">
				<div class="sidebar-menubtn-icon">
					<i class="icon-learning"></i>
				</div>
				<p class="sidebar-menubtn-label">heart Clinic</p>
			</div>
		</a>
		@if(Auth::check())
		{{-- <a href="{{route('forum')}}" @if(route('forum') == Request::url()) class="sidebar-menubtn active" @else class="sidebar-menubtn" @endif>
			<div class="sidebar-menubtn-content">
				<div class="sidebar-menubtn-icon">
					<i class="icon-exchange"></i>
				</div>
				<p class="sidebar-menubtn-label">heart Forum</p>
			</div>
		</a> --}}
		@endif
		<a href="{{route('library')}}" @if(route('library') == Request::url()) class="sidebar-menubtn active" @else class="sidebar-menubtn" @endif>
			<div class="sidebar-menubtn-content">
				<div class="sidebar-menubtn-icon">
					<i class="icon-literature"></i>
				</div>
				<p class="sidebar-menubtn-label">heart Library</p>
			</div>
		</a>
		<a href="{{route('theater')}}" @if(route('theater') == Request::url()) class="sidebar-menubtn active" @else class="sidebar-menubtn" @endif>
			<div class="sidebar-menubtn-content">
				<div class="sidebar-menubtn-icon">
					<i class="icon-videos"></i>
				</div>
				<p class="sidebar-menubtn-label">heart Theater</p>
			</div>
		</a>

	</div>
	<div class="sidebar-footer">
		
		<div class="sidebar-footer-logo">
			<a href="http://www.novartis.com/" target="_blank">
				<img src="{{ asset('assets/css/img/novartis_logo.png') }}" width="130" height="25" alt="novartis" />
			</a>
		</div>
		
		<div class="sidebar-footer-text">
			<p>&copy; {{ date('Y') }} Novartis Pharmaceuticals Corporation&emsp;</p>
			<div class="sidebar-footer-middle">
				<p><a data-toggle="modal" data-target="#contactUsModal">Contact Us</a></p>
				<p><a data-toggle="modal" data-target="#termsOfServiceModal">Terms of Use</a></p>
				<p><a data-toggle="modal" data-target="#privacyPolicyModal" class="open-privacy">Privacy Policy</a></p>
				<p style="display:none;"><a data-toggle="modal" data-target="#hcpModal" data-backdrop="static" class="open-hcp">HCP</a></p>
			</div>
		</div>

	</div>
</div>