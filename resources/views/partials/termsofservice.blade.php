<div class="termsofuse">

	<h3>Last Updated: April 1, 2016</h3>
	
	<h1>TERMS OF USE</h1>

	<p>Welcome to the website of “Heart: The Online Educational Community For Clinicians Treating Heart Failure” located at <a href="/" target="_blank">http://www.theheartcommunity.com</a> (the “Site”) and owned by Novartis Pharmaceutical Corporation (“we”, “us” or “our”). Your use of the Site is subject at all times to these Terms of Use (the “Agreement”), our Privacy Policy, and all applicable laws, rules and regulations.</p>  

	<p>This Site is intended for and open to use by U.S. health care providers only.</p>

	<h2>Acceptance of and Modifications to the Agreement</h2>

	<p>By accessing this Site, you agree that you have read, understand and agree to be legally bound by this Agreement.  If you do not agree to be bound by this Agreement, please do not access or use this Site.  We reserve the right to modify this Agreement at any time, and without prior notice, by notifying you of such changes by any reasonable means, including by posting amended terms on the Site. We encourage you to review this Agreement periodically for any updates or changes; please consult the “<em>Last Updated</em>” date above to learn when this Agreement was last revised. Your use of the Site following any updates or changes to this Agreement will constitute your acceptance of such updates or changes. Any such updates or changes will not apply to any dispute between you and us arising prior to the date on which we posted the amended Agreement incorporating such updates or changes, or otherwise notified you of such changes.  </p>  

	<h2>Use of the Site</h2>

	<p>BY USING THE SITE, YOU AFFIRM THAT (A) ALL INFORMATION THAT YOU SUBMIT IS TRUTHFUL AND ACCURATE; (B) YOU WILL MAINTAIN THE ACCURACY OF SUCH INFORMATION; (C) YOU ARE 13 YEARS OF AGE OR OLDER; (D) YOUR USE OF THE SITE DOES NOT VIOLATE ANY APPLICABLE LAW OR REGULATION AND WILL, AT ALL TIMES, BE COMPLIANT WITH ALL ADDITIONAL TERMS AND CONDITIONS AS PROVIDED BY US, (E) YOU WILL NOT INTERFERE WITH, DISRUPT, OR CREATE AN UNDUE BURDEN ON THE SITE OR THE NETWORKS OR SERVICES CONNECTED TO THE SITE.</p>

 	<ul>
		<li>Subject to the terms and conditions of this Agreement, you may use any text, images, audio, video, and other information, content or materials available on or through the Site (the “Site Content”) that we provide to you hereunder, solely for your non-commercial, personal use during the term of this Agreement.</li>

		<li>You must retain and reproduce each and every copyright notice or other proprietary rights notice contained in any Site Content that you download or otherwise reproduce. You may not distribute, modify, transmit, reuse or repost the Site Content, or use the Site Content for public or commercial purposes, without our prior written permission. You should assume that everything you see or read on this Site is copyrighted unless otherwise noted and may not be used except as provided in this Agreement without our written permission. We neither warrant nor represent that the Site Content, or your use of the Site Content, will not violate or infringe any intellectual property or other rights.</li>

		<li>With the exception of the foregoing limited authorization, no license to or right in the Site or any Site Content, including with respect to any copyright of Novartis Pharmaceuticals Corporation or any other party, is granted or conferred to you.</li>

		<li>The Site may contain or reference trademarks, patents, proprietary information, trade secrets, technologies, products, processes or other proprietary rights of Novartis and/or other parties. No license to or right in any such trademarks, patents, proprietary information, trade secrets, technologies, products, processes or other proprietary rights of Novartis and/or any other party is granted to or conferred upon you.</li>

		<li>DISCLAIMER. WHILE WE USE REASONABLE EFFORTS TO INCLUDE ACCURATE AND UP-TO-DATE SITE CONTENT IN THE SITE, WE MAKE NO WARRANTIES OR REPRESENTATIONS WITH RESPECT TO THE SITE OR SITE CONTENT, WHICH ARE PROVIDED “AS IS.” WE ACCEPT NO RESPONSIBILITY OR LIABILITY WHATSOEVER ARISING FROM OR IN ANY WAY CONNECTED WITH THE USE OF THE SITE OR SITE CONTENT. IN PARTICULAR, WE WILL NOT BE LIABLE FOR THE ACCURACY, COMPLETENESS, ADEQUACY, TIMELINESSOR COMPREHENSIVENESSOF THE SITE OR SITE CONTENT.SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE EXCLUSION MAY NOT APPLY TO YOU. WE ALSO ASSUME NO RESPONSIBILITY, AND SHALL NOT BE LIABLE FOR, ANY DAMAGES TO, OR VIRUSES THAT MAY INFECT, YOUR COMPUTER EQUIPMENT OR OTHER PROPERTY ON ACCOUNT OF YOUR USE OF THE SITE OR YOUR DOWNLOADING OF ANY SITE CONTENT. WE RESERVE THE RIGHT TO SUSPEND OR DISCONTINUE, OR CHARGE FEES IN CONNECTION WITH, ANY OR ALL OF THE FUNCTIONALITY OF THE SITE OR ANY OR ALL SITE CONTENT. WE ACCEPT NO RESPONSIBILITY OR LIABILITY WHATSOEVER FOR ANY INTERRUPTION OR DISCONTINUANCE OF ANY OR ALL FUNCTIONALITY OF THE SITE, WHETHER OR NOT THE RESULT OF ANY OUR/NOVARTIS OR ANY AFFILIATE’S ACTION OR OMISSION.</li>

		<li>The Site Content may contain inaccuracies or typographical errors. We reserve the right to remove, or correct or otherwise modify, any Site Content (including your User Content, as defined below) at any time without notice.</li>

		<li>Because we have no control over and do not endorse any of the sites or other online resources to which the Site is linked, and because we have not reviewed any or all of such sites and resources, you acknowledge and agree that we are not responsible for the content of any off-Site pages or any other sites linked to the Site. Your linking to the Site, off-Site pages or other sites is at your own risk and is without our permission.</li>

		<li>Although we may from time to time monitor or review discussions, chats, postings, transmissions, bulletin boards and other User Content and activities on the Site, we are under no obligation to do so, and assumes no responsibility or liability arising from the content of any such features or functionalities, nor for any error, defamation, libel, slander, omission, falsehood, obscenity, pornography, profanity, danger or inaccuracy contained in any User Content or other Site Content. You are prohibited from posting or transmitting any unlawful, threatening, libelous, defamatory, obscene, scandalous, inflammatory, pornographic or profane material, or any material that could constitute or encourage conduct that would be considered a criminal offense, give rise to civil liability or otherwise violate any law. We reserve the right to fully cooperate with any law enforcement authorities, court order or other legal process requesting or directing us to disclose the identity of anyone posting any such information or materials.</li>

		<li>Links to this Site are permitted only to the home page. You are prohibited from “deep linking” to any other page within the Site without our prior written consent.</li>

		<li>You are prohibited from “framing” the Site or any portion thereof without our prior written consent. In-line linking or any other manner of incorporating parts of this Site into other sites is also prohibited without our prior written consent.</li>

		<li>You are responsible for obtaining, maintaining and paying for all hardware and all telecommunications and other services needed to use the Site.</li>

	</ul>

	<h2>Privacy</h2>

	@if(!Auth::check())
	<p>Your submission of information through the Site is governed by the <a href="/login?modal=privacy" target="_blank">Privacy Policy</a>.</p>
	@else
	<p>Your submission of information through the Site is governed by the <a href="/dashboard?modal=privacy" target="_blank">Privacy Policy</a>.</p>
	@endif

	<h2>Site Access and Registration</h2>

	<p>Since access to this Site is limited to medical physicians, upon visiting the Site, you will first be required to acknowledge that you are a U.S. health care provider.</p> 

	<p>While you can view some features of this Site without registering, you must register to use all of the features of the Site. When you register for the Site, you are required to provide certain information such as your first and last name, your email, a password and your area of specialty. You must provide us with current, complete and accurate information as requested by the registration form, and promptly update such information as necessary. We will send you an e-mail at the address you provide to confirm your Site membership, after which your Site membership will be activated. You will be asked to select a user name and password. User names are subject to our approval and may not contain misleading or objectionable material. Your screenname and password are for your personal use only. It is your responsibility to maintain the confidentiality of your password and account. Additionally, you are responsible for any and all activities that occur under your account. You agree to notify us immediately of any unauthorized use of your account. We are not liable for any loss that you may incur as a result of someone else using your password or account, either with or without your knowledge.</p>

	<h2>This Site Does Not Provide Medical Advice</h2>

	<p>All Site Content, including any Site Content designated as “expert” content or otherwise, is for informational purposes only. We do not recommend or endorse any specific tests, physicians, products, procedures, opinions or other information that may be mentioned on the Site. Reliance on any information provided by us, by persons appearing on the Site, or by other Site members is solely at your own risk.</p>
	  
	<h2>International Use</h2>

	<p>The Site is controlled or operated (or both) from the United States, and is not intended to subject us or Novartis to any non-U.S. jurisdiction or law. The Site is for U.S. users only.</p>

	<h2>Intellectual Property Rights</h2>

	<p><i>User Content Posted on the Site</i>: You are solely responsible for the content and other materials you post on or through the Site or transmit to or share with other Users or recipients (collectively, “User Content”).</p>

	<p>You represent and warrant that you have all rights necessary to grant the licenses granted in this section, and that your User Content, and your provision thereof through and in connection with the Site, are complete and accurate, and are not fraudulent, tortious or otherwise in violation of any applicable law or any right of any third party. You further irrevocably waive any “moral rights” or other rights with respect to attribution of authorship or integrity of materials regarding any User Content that you may have under any applicable law under any legal theory.</p>

	<p>For purposes of clarity, you retain ownership of your User Content. You hereby grant and will grant to us and our affiliated companies a non-exclusive, worldwide, royalty-free, fully paid up, transferable, sublicensable (through multiple tiers), perpetual, irrevocable license to copy, publicly display, transmit, publicly perform, distribute, store, modify, create derivative works of, and otherwise use, analyze and exploit your User Content in connection with the operation of the Service or the promotion, advertising or marketing thereof, in any form, medium or technology now known or later developed. You agree that this license includes the right for us to make User Content available to other companies or individuals, including those who partner with us or our business partners, in connection with the Site.</p>

	<p>You acknowledge and agree that any questions, comments, suggestions, ideas, feedback or other information about the Site provided by you to us is non-confidential, and we shall be entitled to the unrestricted use and dissemination of the foregoing for any purpose, commercial or otherwise, without acknowledgment or compensation to you, and without placing us under any fiduciary or other obligation.</p>

	<p>We have no control over and is not responsible for any use or misuse (including any distribution) by any third party of User Content. IF YOU CHOOSE TO MAKE ANY OF YOUR PERSONALLY IDENTIFIABLE OR OTHER INFORMATION PUBLICLY AVAILABLE THROUGH THE SITE, YOU DO SO AT YOUR OWN RISK.</p>

	<p>You are solely responsible for your User Content.  You represent and warrant that you have all rights necessary to upload the User Content and to grant us the rights herein.  You shall ensure that User Content does not violate the privacy rights, publicity rights, intellectual property rights, or any other rights of any person.  User Content may not contain any content that (a) is illegal, obscene, or defamatory, (b) infringes intellectual property or personal rights, (c) promotes dangerous or illegal activities, or (d) is otherwise reasonably objectionable.</p>

	<p>You acknowledge and agree that we may preserve and/or disclose User Content if required to do so by law or in the good-faith belief that such preservation or disclosure is reasonably necessary to: (a) comply with legal process, applicable laws or government requests; (b) enforce this Agreement; (c) respond to claims that any Content violates the rights of third parties; or (d) protect the rights, property or personal safety of our Users or us or the public. You understand that the technical operation of the Site may involve (1) transmissions of User Content over various third-party, unsecured networks; and (2) changes to User Content to conform and adapt it to technical requirements of connecting networks or devices.</p>

	<p><i>Claims of Copyright Infringement</i></p>

	<p>Materials may be made available via the Site by third parties or other Users not within our control (such as User Content). We are under no obligation to, and do not, scan content used in connection with the Site for the inclusion of illegal or impermissible content. However, we respect the copyright interests of others. It is our policy not to permit materials known by us to infringe another party’s copyright to remain on the Site.</p>

	<p>If you believe any materials on the Site infringe a copyright, you should provide us with written notice that at a minimum contains:</p>

	<p>(i) A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;</p>

	<p>(ii) Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site;</p>

	<p>(iii) Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit us to locate the material;</p>

	<p>(iv) Information reasonably sufficient to permit us to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted;</p>

	<p>(v) A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and</p>
	<p>(vi) A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</p>

	<p>All such notices should be sent to our designated agent as follows:</p>
	<p>Novartis Pharmaceuticals Corporation<br />Sudipta Rao<br />Executive Director, Sr. Trademark Attorney<br />One Health Plaza<br />East Hanover, NJ 07936<br />(862) 778-8000</p>

	<p>Or emailed to our Copyright Agent at sudipta.rao@novartis.com (Subject line: “DMCA Takedown Request”).</p>

	<p><i>Liability for Site Content and Third Party Materials</i>: Under no circumstances will we be liable in any way for any Site Content, including for any errors or omissions in any Site Content, or for any loss or damage of any kind incurred as a result of the use of any Site Content posted, emailed or otherwise transmitted via the Site. You acknowledge that we do not necessarily pre-screen Site Content, but that we and our designees shall have the right (but not the obligation) in our sole discretion to refuse or remove any Site Content (including User Content) that are available via the Site. Without limiting the foregoing, we and our designees shall have the right to remove any Site Content that violates this Agreement or is deemed by us, in our sole and absolute discretion, to be otherwise objectionable. YOU AGREE THAT YOU MUST EVALUATE, AND BEAR ALL RISKS ASSOCIATED WITH, THE USE OF ANY SITE CONTENT, INCLUDING ANY RELIANCE ON THE ACCURACY, COMPLETENESS, OR USEFULNESS OF SUCH SITE CONTENT. YOUR USE OF ANY THIRD-PARTY MATERIALS IS ALSO AT YOUR OWN RISK AND SUBJECT TO ANY ADDITIONAL TERMS, CONDITIONS AND POLICIES APPLICABLE TO SUCH THIRD-PARTY MATERIALS (SUCH AS TERMS OF SERVICE OR PRIVACY POLICIES OF THE PROVIDERS OF SUCH THIRD-PARTY MATERIALS).</p>

	<h2>Indemnity</h2>

	<p>Except to the extent prohibited under applicable law, you agree to defend, indemnify and hold us and our affiliates, and our and their respective shareholders, directors, officers, employees, agents, representatives, licensors, suppliers and service providers harmless, from and against all claims, losses, costs and expenses (including attorneys’ fees) arising out of (a) your use of, or activities in connection with, the Site (including all User Content); (b) any violation or alleged violation of this Agreement by you; and (c) any violation by you of applicable law or any agreement or terms with a third party to which you are subject.</p>

	<h2>LIMITATION OF LIABILITY</h2>

	<p>TO THE EXTENT PERMITTED UNDER APPLICABLE LAW, WE WILL NOT BE LIABLE FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY OR PUNITIVE DAMAGES OF ANY KIND, UNDER ANY CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHER THEORY, INCLUDING DAMAGES FOR LOSS OF PROFITS, USE OR DATA, LOSS OF OTHER INTANGIBLES, LOSS OF SECURITY OF USER CONTENT (INCLUDING UNAUTHORIZED INTERCEPTION BY THIRD PARTIES OF ANY USER CONTENT), EVEN IF ADVISED IN ADVANCE OF THE POSSIBILITY OF SUCH DAMAGES OR LOSSES. WITHOUT LIMITING THE FOREGOING AND TO THE EXTENT PERMITTED UNDER APPLICABLE LAW, WE WILL NOT BE LIABLE FOR DAMAGES OF ANY KIND RESULTING FROM YOUR USE OF OR INABILITY TO USE THE SITE OR ANY SITE CONTENT. YOUR SOLE AND EXCLUSIVE REMEDY FOR DISSATISFACTION WITH THE SITE OR ANY SITE CONTENT IS TO STOP USING THE SITE AND SITE CONTENT. TO THE EXTENT PERMITTED UNDER APPLICABLE LAW, OUR MAXIMUM AGGREGATE LIABILITY FOR ALL DAMAGES, LOSSES AND CAUSES OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, SHALL BE THE GREATER OF THE TOTAL AMOUNT, IF ANY, PAID BY YOU TO US TO USE THE SITE OR $10.</p>

	<p>ALL LIMITATIONS OF LIABILITY AND DISCLAIMERS OF ANY KIND (INCLUDING IN THIS SECTION AND ELSEWHERE IN THIS AGREEMENT) ARE MADE ON BEHALF OF US OR OUR AFFILIATES AND THEIR RESPECTIVE SHAREHOLDERS, DIRECTORS, OFFICERS, EMPLOYEES, AFFILIATES, AGENTS, REPRESENTATIVES, LICENSORS, SUPPLIERS AND SERVICE PROVIDERS. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE SITE, OR WITH THIS AGREEMENT, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</p>

	<h2>Term and Termination of Access</h2>

	<p>This Agreement shall remain in full force and effect while you use the Site or are registered with the Site. You may terminate your registration at any time, for any reason, by sending an email requesting termination to support@theheartcommunity.com. We may suspend, limit or terminate your registration and/or access to the Site at any time, without warning and for any reason. You continue to remain bound by this Agreement even after your registration is terminated to the extent that you continue to access the Site in any capacity. We have no obligation to maintain, store or transfer to you any of the User Content that you have posted on or uploaded to the Site. If we remove any of your User Content or your account, all associated data from replies or comments to such User Content or to such removed account maybe permanently deleted from the community and the server(s). Sections entitled Acceptance of and Modifications to this Agreement, Intellectual Property Rights, Indemnity, Limitations of Liability and Governing Law and Venue shall survive any expiration or termination of this Agreement.</p>

	<h2>Governing Law and Venue</h2>

	<p>This Agreement shall be governed by the laws of the state of New Jersey and the applicable federal laws of the United States of America. All disputes arising under or in any way connected with the Site (including membership therein and other uses thereof), shall be litigated exclusively in the state and federal courts residing in New Jersey, and in no other court or jurisdiction. You hereby submit to the jurisdiction of the state and federal courts sitting in New Jersey.</p>

	<h2>Miscellaneous</h2>

	<p>This Agreement does not, and shall not be construed to, create any partnership, joint venture, employer-employee, agency or franchisor-franchisee relationship between you and us. If any provision of this Agreement is found to be unlawful, void or for any reason unenforceable, that provision will be deemed severable from this Agreement and will not affect the validity and enforceability of any remaining provision. You may not assign, transfer or sublicense any or all of your rights or obligations under this Agreement without our express prior written consent. We may assign, transfer or sublicense any or all of its rights or obligations under this Agreement without restriction. No waiver by either party of any breach or default hereunder will be deemed to be a waiver of any preceding or subsequent breach or default. Any heading, caption or section title contained herein is for convenience only, and in no way defines or explains any section or provision. All terms defined in the singular shall have the same meanings when used in the plural, where appropriate and unless otherwise specified. Any use of the term “including” or variations thereof in this Agreement shall be construed as if followed by the phrase “without limitation.” This Agreement, including any terms and conditions incorporated herein, is the entire agreement between you and us relating to the subject matter hereof, and supersedes any and all prior or contemporaneous written or oral agreements or understandings between you and us relating to such subject matter. Notices to you (including notices of changes to this Agreement) may be made via posting to the Site or by e-mail (including in each case via links), or by regular mail. Without limitation, a printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. We will not be responsible for any failure to fulfill any obligation due to any cause beyond its control.</p>

</div> <!-- .card-content -->