<div class="modal fade" id="exitModalDashboard" role="dialog">
	<div class="modal-dialog">
		
		<div class="modal-content">

			<div class="modal-header">
				<h3 class="header-title">Leaving heart</h3>
				<button type="button" class="close" data-dismiss="modal">
					<i class="material-icons">&#xE5CD;</i>
					<p>Close</p>
				</button>
			</div>

			<div class="modal-body">

				<h2>HFSA</h2>

				<p>You will now be redirected to view the event page at:</p>

				<p class="last-item"><a href="http://meeting.hfsa.org/" target="_blank">http://meeting.hfsa.org/</a></p>
				
				<p></p>
			</div>

		</div> <!-- .modal-content -->

	</div> <!-- .modal-dialog -->
</div> <!-- .modal -->