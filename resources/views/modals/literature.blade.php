@if(isset($literature))

@foreach ($literature as $literatureitem)

	<div class="modal fade" id="literatureModal-{{ $literatureitem->id }}" role="dialog">
		<div class="modal-dialog">
			
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="header-title">Leaving <span><i class="icon-heart"></i>heart</span></h3>
					<button type="button" class="close" data-dismiss="modal">
						<i class="material-icons">&#xE5CD;</i>
						<p>Close</p>
					</button>
				</div>

				<div class="modal-body">

					<h2>{{ $literatureitem->title }}</h2>

					<p class='external-link-disclaimer'>You will now be redirected to view the article at:</p>

					<p class="last-item" onclick="markResourceCompleted(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif,{{ $literatureitem->id }},'library-article');"><a href="{{ $literatureitem->link }}" target="_blank">{{ $literatureitem->link }}</a></p>

				</div>

			</div> <!-- .modal-content -->

		</div> <!-- .modal-dialog -->
	</div> <!-- .modal -->

@endforeach

@endif