@foreach ($learningresources as $learningresource)

<div class="modal fade module-modal" id="learningresourceModal-{{ $learningresource->id }}" role="dialog">
	<div class="modal-dialog">
	    
	  <div class="modal-content">
		
		<div class="modal-header">
			<h3 class="header-title">Module Viewer</h3>
			<button type="button" class="close module-modal-close" data-resource='{{ $learningresource->id }}' data-dismiss="modal">
				<i class="material-icons">&#xE5CD;</i>
				<p>Close</p>
			</button>
		</div>

	    <div class="modal-body modal-body-{{ $learningresource->id }}">


	    </div>

	  </div> <!-- .modal-content -->

	</div> <!-- .modal-dialog -->
</div> <!-- .modal -->

@endforeach