@foreach ($videos as $video)

	@if($video->type == "video")

	<div class="modal fade video-modal" id="videoModal-{{ $video->id }}" role="dialog">
		<div class="modal-dialog">
			
			<div class="modal-content">

				<div class="modal-header">
					<h3 class="header-title">Video Player</h3>
					<button type="button" class="close" data-dismiss="modal">
						<i class="material-icons">&#xE5CD;</i>
						<p>Close</p>
					</button>
				</div>

				<div class="modal-body">
					<div class="video-wrapper">
						
						<div class="modal-sidebar">
							<div class="video-sidebar-content">
							
								<h3>{{ $video->title }}</h3>

								<p>{{ $video->description }}</p>
							
							</div> <!-- .modal-sidebar-content -->
						</div> <!-- .modal-sidebar -->


						<div class="container" video-id="video-{{ $video->id }}">

								<iframe src="{{ $video->source }}" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen onclick="markResourceCompleted(@if(Auth::check()){{ Auth::user()->id }}@else{{0}}@endif, {{ $video->id }},'theater-video');"></iframe>

						</div> <!-- .container -->


					</div> <!-- .video-wrapper -->
				</div> <!-- .modal-body -->

			</div> <!-- .modal-content -->

		</div> <!-- .modal-dialog -->
	</div> <!-- .modal -->

	@endif

@endforeach