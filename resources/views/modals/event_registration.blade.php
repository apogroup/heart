<div class="modal fade" id="eventRegistrationThankYou" role="dialog">
	<div class="modal-dialog">
		
		<div class="modal-content">

			<div class="modal-header">
				<h3 class="header-title">Confirmation</h3>
				<button type="button" class="close" data-dismiss="modal">
					<i class="material-icons">&#xE5CD;</i>
					<p>Close</p>
				</button>
			</div>

			<div class="modal-body">

				<div class="registration-confirmation">

					<p>Thank you! You are now registered for:</p>

					<h3>Bringing the New Heart Failure Guidelines to Lfie: The 2016 ACC/AHA/HFSA Focused Update on New Pharmacological Therapy</h3>

					<p class="date">Wednesday, August 24, 2016 <span class="time">7:00 PM EDT</span></p>

					<p class="last-item">You will receive notifications via e-mail leading up to the start of the live event.</p>

				</div>

			</div>

		</div> <!-- .modal-content -->

	</div> <!-- .modal-dialog -->
</div> <!-- .modal -->