@foreach ($videos as $video)

	@if($video->type == 'module')

	<div class="modal fade module-modal video-module-modal" id="videomoduleModal-{{ $video->id }}" role="dialog">
		<div class="modal-dialog">
		    
		  <div class="modal-content">
			
			<div class="modal-header">
				<h3 class="header-title">Video Player</h3>
				<button type="button" class="close module-modal-close" data-resource='{{ $video->id }}' data-dismiss="modal">
					<i class="material-icons">&#xE5CD;</i>
					<p>Close</p>
				</button>
			</div>

			<div class="mobile-module-menu-btn"><i class="material-icons icon-more">&#xE5D4;</i><p class="mobile-module-menu-title"></p></div>

		    <div class="modal-body modal-body-{{ $video->id }}">


		    </div>

		  </div> <!-- .modal-content -->

		</div> <!-- .modal-dialog -->
	</div> <!-- .modal -->

	@endif

@endforeach