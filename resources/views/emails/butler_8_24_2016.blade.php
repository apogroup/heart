<html>

	<head>

		<title></title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	</head>

	<body style="margin: auto; background-color: #f4f4f4; background: #f4f4f4;">

		<table width="503" height="624" border="0" cellpadding="0" cellspacing="0" style="margin: 20px auto;">
			<tr>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_01.png')); ?>" width="8" height="8"></td>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_02.png')); ?>" width="487" height="8"></td>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_03.png')); ?>" width="8" height="8"></td>
			</tr>
			<tr>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_04.png')); ?>" width="8" height="230"></td>
				<td style="background-color: #ffffff; background: #ffffff; padding-top: 0px;">
				
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20" style="width: 20px;">&nbsp;</td>
							<td>

								<img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/heart.png')); ?>" width="90" height="30">
								<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 12px; font-weight: bold; margin-bottom: 30px;">The online educational community for <span style="color: #cb2f33;">clinicians treating heart failure</span></p>
								<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; font-weight: bold;">Hello [User Name],</p>
								<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; font-weight: bold; line-height: 20px;">This is a remind that you are registered for the following heart community event happening live [timeframe] from now at <a href="http://theheartcommunity.com" target="_blank" style="color: #cb2f33; text-decoration: none;">theHeartCommunity.com</a></p>

							</td>
							<td width="20" style="width: 20px;">&nbsp;</td>
						</tr>
					</table>

				</td>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_06.png')); ?>" width="8" height="230"></td>
			</tr>
			<tr>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_07.png')); ?>" width="8" height="8"></td>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_08.png')); ?>" width="487" height="8"></td>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_09.png')); ?>" width="8" height="8"></td>
			</tr>
			<tr>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_10.png')); ?>" width="8" height="163"></td>
				<td style="background-color: #fbfbfb; background: #fbfbfb;">
					
					<table width="487" border="0" cellpadding="0" cellspacing="0" style="width: 487px;">
						<tr>
							<td width="20" style="width: 20px;">&nbsp;</td>
							<td style="vertical-align: top;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/javed_butler.png')); ?>" width="98" height="137" style="margin-top: 5px; margin-right: 20px;"></td>
							<td style="vertical-align: top;">

								<h4 style="color: #cb2f33; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; line-height: 14px; font-weight: bold; margin-top: 5px; margin-bottom: 0px;">LIVE WEBCAST*:</h4>
								<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; margin-top: 10px; margin-bottom: 10px;">Bring the New Heart Failure Guidelines To Life: <br>The 2016 ACC/AHA/HFSA Focused Update on New <br>Pharmacological Therapy</p>

								<table width="338" style="width: 338px;">
									
									<tr>
										<td style="vertical-align: top;">
											<h4 style="color: #cb2f33; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; line-height: 14px; font-weight: bold; margin-bottom: 0px;">PRESENTED BY:</h4>
											<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; margin-top: 10px; margin-bottom: 0px;"><span style="font-weight: bold;">Javed Butler</span> <br><span style="font-size: 12px;">MD, MPH, MBA</span></p>
										</td>
										<td style="vertical-align: top;">
											<div style="margin-top: 7px; margin-left: 50px;">
												<img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/live.png')); ?>" width="54" height="21" style="float:left;">
												<p style="color: #cb2f33; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; font-weight: bold; text-align: right; margin-top: 0px; padding-top: 2px;">7:00 PM EDT</p>
											</div>
											<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; font-weight: bold; text-align: right; line-height: 12px;">Wednesday, August 24, 2016</p>
										</td>
									</tr>

								</table>

							</td>
							<td width="20" style="width: 20px;">&nbsp;</td>
						</tr>


					</table>

				</td>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_12.png')); ?>" width="8" height="163"></td>
			</tr>
			<tr>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_13.png')); ?>" width="8" height="8"></td>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_14.png')); ?>" width="487" height="8"></td>
				<td style="background-color: #fbfbfb; background: #fbfbfb;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_15.png')); ?>" width="8" height="8"></td>
			</tr>
			<tr>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_16.png')); ?>" width="8" height="197"></td>
				<td style="background-color: #ffffff; background: #ffffff;">

					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20" style="width: 20px;">&nbsp;</td>
							<td>

								<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; font-weight: bold; line-height: 20px;">In the meantime, visit <a href="http://theheartcommunity.com" target="_blank" style="color: #cb2f33; text-decoration: none;">theHeartCommunity.com</a> for all the latest resources, articles, and videos for clinicians treating heart failure.</p>

								<p style="color: #444444; font-family: Arial,Helvetica,Sans-Serif; font-size: 14px; font-weight: bold; line-height: 20px;">Thank you.</p>

								<img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/heart.png')); ?>" width="90" height="30" style="margin-top: 10px;">
								
							</td>
							<td width="20" style="width: 20px;">&nbsp;</td>
						</tr>
					</table>

				</td>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_18.png')); ?>" width="8" height="197"></td>
			</tr>
			<tr>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_19.png')); ?>" width="8" height="10"></td>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_20.png')); ?>" width="487" height="10"></td>
				<td style="background-color: #ffffff; background: #ffffff;"><img src="<?php echo $message->embed(asset('assets/email/img/butler_8_24_2016/frame_21.png')); ?>" width="8" height="10"></td>
			</tr>
		</table>

	</body>

</html>