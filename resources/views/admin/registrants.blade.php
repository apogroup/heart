@extends('layouts.default')

@section('content')

	<div>

		<div class="card">
			<div class="card-content">

				<?php $count = 0 ?>
				
				<table>
					<tr>
						<th style="padding: 0 40px 0 0">#</th>
						<th style="padding: 0 40px 0 0">@sortablelink ('name')</th>
						<th style="padding: 0 40px 0 0">@sortablelink ('email')</th>
						<th style="padding: 0 40px 0 0">@sortablelink ('specialty')</th>
						<th style="padding: 0 40px 0 0">@sortablelink ('created_at', 'Registration Date')</th>
					</tr>
	
					@foreach ($registrants as $registrant)
					<?php $count++ ?>

					<tr>
						<td style="padding: 0 40px 0 0">{{$count}}</td>
						<td style="padding: 0 40px 0 0">{{$registrant->name}}</td>
						<td style="padding: 0 40px 0 0">{{$registrant->email}}</td>
						<td style="padding: 0 40px 0 0">{{$registrant->specialty}}</td>
						<td style="padding: 0 40px 0 0">{{$registrant->created_at}}</td>
					</tr>

					@endforeach
				</table>

			</div>
		</div>


	</div> 

@endsection