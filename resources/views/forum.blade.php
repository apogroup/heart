@extends('layouts.default')

@section('content')

	<div class="forum">

		<div class="card card-header forum-header">
			<div class="card-content">

				<h2 class="heart-logo"><i class="icon-heart"></i>heart Forum</h2>
				<h2 class="menu-hamburger"><i class="material-icons icon-menu">&#xE5D2;</i></h2>

				<h4>Clinicians come together to share knowledge, experiences, and ideas about heart failure.</h4>
			
			</div>
		</div> <!-- .forum-header -->

		<div class="card card-header forum-workcast">
			<div class="card-content">

				<div style="margin-bottom: 20px;"><p>This live event requires Flash player version 9.0 or greater. Please download latest version from <a href="https://get.adobe.com/flashplayer/" target="_blank">here</a>.</p></div>

				<div id="wcint_playerwrapper">
					<!-- WorkCast Video Player - This div tag to go in to the body section of your page -->
					<div id="wcint_playercontainer"></div>
				</div>

				<div class="workcast-question-form">
						
					<div class="workcast-question-form-prompt"></div>
					<div class="workcast-question-form-inline"></div>

				</div>

				<!-- WorkCast Ask a Question - This div tag to go in to the body section of your page -->
				<div id="wcint_askaquestioncontainer"></div>

			</div>
		</div> <!-- .forum-expert-workcast -->

	</div> <!-- .forum -->

	<!-- WorkCast Mingle - This script tag to go in to the body section of your page next to the </body> tag -->
	<script type="text/javascript" language="javascript">
	    (function(m, i, n, g, l, e, d) {

	        m['WorkCastIntegrationObject'] = l;
	        m[l] = m[l] || function() {
	            (m[l].q = m[l].q || []).push(arguments)
	        }, m[l].l = 1 * new Date();
	        e = i.createElement(n), d = i.getElementsByTagName(n)[0];
	        e.async = 1;
	        e.src = g;
	        d.parentNode.insertBefore(e, d);

	    })(window, document, 'script', '//d2uwbgdpur2zq9.cloudfront.net/js/workcast_int_100.min.js', 'wi');
	    wi('version', '1.0');
	    wi('integrate', 'tVUaDj1AiUg=');
	    wi('pak', '4820990616892959');
	    wi('cpak', '1076392649326375');
	    wi('email', '{{ Auth::user()->email }}');

	</script>
	<!-- End WorkCast Mingle -->

@endsection